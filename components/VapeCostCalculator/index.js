import React from 'react';

import CartSizeOne from 'assets/SVG/CartSizeOne';
import CartSizeTwo from 'assets/SVG/CartSizeTwo';
import CartSizeThree from 'assets/SVG/CartSizeThree';

import {
  Container,
  Heading,
  IconSection,
  MainHeading,
} from 'css/components/VapeCostCalculator';

const VapeCostCalculator = () => {
  return (
    <Container>
      <Heading>
        <h3>VAPE COST CALCULATOR</h3>
      </Heading>
      <MainHeading>
        <h1>LET’s START WITH YOUR CART SIZE</h1>
        <p>Please start by choosing a size below</p>
      </MainHeading>
      <IconSection>
        <div>
          <CartSizeOne />
          <p>.35g</p>
        </div>
        <div>
          <CartSizeTwo />
          <p>.5g</p>
        </div>
        <div>
          <CartSizeThree />
          <p>1g</p>
        </div>
      </IconSection>
      <p>My cart size isn't here </p>
    </Container>
  );
};

export default VapeCostCalculator;
