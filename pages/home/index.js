import React, { useState, useEffect } from 'react';
import { useMotionValue, useSpring, useAnimation, motion } from 'framer';
import Header from 'components/Header';
import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import smoothscroll from 'smoothscroll-polyfill';
import RotatingMarquee from 'components/RotatingMarquee';
import QuintEssentialOil from 'components/QuintEssentialOil';
import ProductCategories from 'components/ProductCategories';
import FarmBill from 'components/FarmBill';
import Promise from 'components/Promise';
import Drop from 'components/Drop';
import LegalBg from 'assets/Images/LegalBg.png';
import styled from 'styled-components';
import Footer from 'components/Footer';
import Evolution from 'components/Evolution';
import Chromatogram from 'components/Chromatogram';
import WorldMap from 'components/WorldMap';
import PromiseBg from 'assets/Images/PromiseBg.jpeg';
import RotateIcon from 'assets/SVG/Rotate';
import { Helmet } from 'react-helmet';

// Track last scroll for Header collapse
let lastScrollTop = 0;

let defaultWidth;

// Smooth scroll for iOS devices
smoothscroll.polyfill();

// Get offset values for elments from top of the page
const offset = (el) => {
  if (!el) return el;
  var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
};

// Check if iPad
const isiPad = () => {
  return (
    ['iPad Simulator', 'iPad'].includes(navigator.platform) ||
    (navigator.userAgent.includes('Mac') && 'ontouchend' in document)
  );
};

// const scrollToTop = () => {
//   window.scrollTo(0, 0);
// };

// if ('onpagehide' in window) {
//   window.addEventListener('pagehide', scrollToTop);
// } else {
//   window.addEventListener('unload', scrollToTop);
// }

const Home = () => {
  const [collision, setCollision] = useState({});
  const headerCollapse = useMotionValue(0);
  const profileChanged = useMotionValue(0);
  const desktopY = useSpring(useMotionValue(0), {
    velocity: 0,
    mass: 0.1,
  });
  const mobileY = useMotionValue(0);
  const directionControls = useAnimation();
  const dropControls = useAnimation();
  const history = useHistory();
  const [rotateScreen, setRotateScreen] = useState(false);
  const [video, setVideo] = useState(false);
  const category = useSelector((state) => state?.marquee);
  const location = useLocation();

  const scrollEvents = () => {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    let dir = lastScrollTop - scrollTop;
    if (dir !== 0) initiateScrollEvents(dir);
    lastScrollTop = scrollTop <= 0 ? 0 : scrollTop;
    if (y.get() !== -document.documentElement.scrollTop)
      y.set(-document.documentElement.scrollTop);
  };

  useEffect(() => {
    defaultWidth = window.innerWidth;
    orientationEvents();
  }, []);

  useEffect(() => {
    window.addEventListener('scroll', scrollEvents);
    window.addEventListener('orientationchange', orientationEvents);

    return () => {
      window.removeEventListener('scroll', scrollEvents);
      window.removeEventListener('orientationchange', orientationEvents);
    };
  });

  const initiateScrollEvents = (scrollDir) => {
    if (category !== 'citrus' && profileChanged.get() === 0) {
      profileChanged.set(1);
    }
    if (scrollDir > 0 && headerCollapse?.get() !== 0) {
      headerCollapse.set(0);
      dropControls.start({ rotate: -180, transition: { duration: 0.15 } });

      directionControls.start({ y: 0 });
    } else if (scrollDir < 0 && headerCollapse?.get() !== -68) {
      headerCollapse.set(-68);
      dropControls.start({ rotate: 0, transition: { duration: 0.15 } });
      directionControls.start({ y: -68 });
    }
  };

  useEffect(() => {
    if (
      location.search &&
      location.search.split('to=')[1].split('&')[0] === 'shop'
    )
      window.scrollTo({
        top: collision?.science?.scrollHeight,
        behavior: 'smooth',
      });
  }, [collision?.science, location.search]);

  const initiateCollision = () => {
    let spiritDiv = document.getElementById('spiritDiv');
    let nugSection = document.getElementById('nugSection');
    let processSection = document.getElementById('process_section');
    let labSection = document.getElementById('labSection');
    let chromatogramSection = document.getElementById('chromatogram_section');
    let waitisoverSection = document.getElementById('waitisover_section');
    let science = document.getElementById('science-culture-section');
    let map = document.getElementById('worldMapSection');
    let farmer = document.getElementById('sunIcon');
    let manufacture = document.getElementById('manufacturingPlant');
    let refining = document.getElementById('refineryBuilding');
    let thc = document.getElementById('lowThcContainer');

    setCollision({
      ...collision,
      spiritExit: {
        ...collision?.spiritExit,
        scrollHeight: offset(spiritDiv)?.top,
        clientHeight: spiritDiv?.clientHeight,
      },
      nug: {
        ...collision?.nug,
        scrollHeight: offset(nugSection)?.top,
        clientHeight: nugSection?.clientHeight,
      },
      process: {
        ...collision?.process,
        scrollHeight: offset(processSection)?.top,
        clientHeight: processSection?.clientHeight,
      },
      wait: {
        ...collision?.wait,
        scrollHeight: offset(waitisoverSection)?.top,
        clientHeight: waitisoverSection?.clientHeight,
      },
      lab: {
        ...collision?.lab,
        scrollHeight: offset(labSection)?.top,
        clientHeight: labSection?.clientHeight,
      },
      chart: {
        ...collision?.chart,
        scrollHeight: offset(chromatogramSection)?.top,
        clientHeight: chromatogramSection?.clientHeight,
      },
      science: {
        ...collision?.science,
        scrollHeight: offset(science)?.top,
        clientHeight: science?.clientHeight,
      },
      map: {
        ...collision?.map,
        scrollHeight: offset(map)?.top,
        clientHeight: map?.clientHeight,
      },
      drop: {
        ...collision?.drop,
        farmerEl: farmer,
        refiningEl: refining,
        manuEl: manufacture,
        farmerHeight: offset(farmer)?.top - offset(processSection)?.top,
        manufactureHeight:
          offset(manufacture)?.top - offset(processSection)?.top,
        refiningHeight: offset(refining)?.top - offset(processSection)?.top,
        thcHeight: offset(thc)?.top - offset(labSection)?.top,
      },
    });
  };

  useEffect(() => {
    if (!collision?.wait || !collision?.science) {
      initiateCollision();
    }
  });

  const y = isiPad() ? mobileY : window.innerWidth <= 1023 ? mobileY : desktopY;

  return (
    <>
      {process.browser && (
        <>
          <Helmet>
            <title>
              Welcome To California's Terpene Belt - Cannabis Essential Oil
            </title>
            <meta
              name='google-site-verification'
              content='yYjZBj16Z1zDqrzewE0LY7JgqbfkmObn-1A9s2upy9U'
            />
            <meta
              name='description'
              content='California Cannabis Essential Oil now shipping globally. Available in Citrus, Exotic, Fruit, Gas, Pine, Sour, and Sweet flavor profiles.'
            />
          </Helmet>
          {rotateScreen && (
            <RotateScreen>
              <div>
                <RotateIcon />
                <p>Please rotate back the screen to portrait mode</p>
              </div>
            </RotateScreen>
          )}
          {!rotateScreen && (
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1, transition: { duration: 0.6 } }}>
              <Header
                headerCollapse={headerCollapse}
                controls={directionControls}
                y={y}
                scrollHeight={collision?.science?.scrollHeight}
                history={history}
              />
              {/* <RotatingMarquee
                scrollHeight={collision?.spiritExit?.scrollHeight}
                scienceScroll={collision?.science?.scrollHeight}
                videoRotation={{
                  video: video,
                  setVideo: setVideo,
                  setRotateScreen: setRotateScreen,
                }}
                y={y}
              /> */}
              {/* <Drop collision={collision} y={y} controls={dropControls} /> */}

              <QuintEssentialOil
                key={collision?.nug}
                spirit={collision?.spiritExit}
                nug={collision?.nug}
                process={collision?.process}
                science={collision?.science}
                drop={collision?.drop}
                y={y}
              />

              <Evolution lab={collision?.lab} y={y} />
              <Chromatogram chart={collision?.chart} y={y} />
              <ProductCategories />
              <WorldMap map={collision?.map} y={y} />

              <BillAndPromiseWrapper>
                <Opacity></Opacity>
                <FarmBill />
                <Promise />
              </BillAndPromiseWrapper>

              <Footer
                y={y}
                headerCollapse={headerCollapse}
                controls={directionControls}
              />
            </motion.div>
          )}
        </>
      )}
    </>
  );
};

export default Home;

const RotateScreen = styled.div`
  background-color: #000000;
  height: 100vh;
  width: 100vw;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
  > div {
    display: flex;
    flex-direction: column;
    align-items: center;

    > p {
      color: #fff;
      margin-top: 20px;
      opacity: 0.9;
      font-family: 'robotoregular';
    }
  }
`;

const BillAndPromiseWrapper = styled.div`
  background-image: url(${LegalBg}), url(${PromiseBg});
  background-repeat: no-repeat;
  background-position: top;
  background-size: contain, cover;
  position: relative;
`;
const Opacity = styled.div`
  background-color: #ffffffa6;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;
