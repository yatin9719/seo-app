import * as React from 'react';
import { motion } from 'framer';

const Icon = (props) => (
  <svg
    width={229 * 3.2}
    height={61 * 3.2}
    viewBox='0 0 229 61'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}>
    <path
      d='M1.51 58.192V1.628h226.222v57.673'
      stroke='#58A08F'
      strokeWidth={2}
      strokeLinecap='round'
      strokeLinejoin='bevel'
      strokeDasharray='3 3'
    />
  </svg>
);

export default Icon;
