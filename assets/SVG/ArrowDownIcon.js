import * as React from 'react';

const Icon = (props) => (
  <svg width={5 * 2} height={9 * 2} viewBox='0 0 5 9' fill='none' {...props}>
    <path d='M.89 4.368L4.934.696V8.04L.89 4.368z' fill='#C4C4C4' />
  </svg>
);

export default Icon;
