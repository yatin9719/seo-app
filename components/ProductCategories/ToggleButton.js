import React, { useState, useEffect } from 'react';
import { motion } from 'framer';
import { SwitchContainer } from 'css/components/ProductCategories';

const ToggleButton = ({ isOn, onClick, disabled, ...props }) => {
  const [width, setWidth] = useState(window.innerWidth);

  useEffect(() => {
    setWidth(width);
  }, [window.innerWidth]);
  return (
    <SwitchContainer>
      <label className='switch'>
        <input type='checkbox' checked={!isOn} onChange={onClick} />
        <span className='slider round' />
        <motion.span
          className='toggle'
          initial={{ x: 0 }}
          animate={{ x: !isOn ? 'calc(100% - 10px)' : 0 }}
          transition={{
            type: 'spring',
            stiffness: 100,
            bounce: 1,
          }}
        />
        <h3
          className='products-append'
          style={isOn ? { color: '#ffffff' } : {}}>
          <span>CANNABIS PRODUCTS</span>
        </h3>
        <h3 className='no-append' style={isOn ? { color: '#ffffff' } : {}}>
          <span>CANNABIS</span>
        </h3>
        <h3
          className='products-append'
          style={!isOn ? { color: '#ffffff' } : {}}>
          <span>NON-CANNABIS PRODUCTS</span>
        </h3>
        <h3 className='no-append' style={!isOn ? { color: '#ffffff' } : {}}>
          <span>NON-CANNABIS</span>
        </h3>
      </label>
    </SwitchContainer>
  );
};

export default ToggleButton;
