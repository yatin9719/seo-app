import React, { useEffect } from 'react';
import { BoxContainer } from 'css/components/ScienceVsCulture';

const getFontSize = (length) => {
  if (length <= 15) return 19;
  else {
    let size = length * 1;
    if (size >= 17) size = 14;
    return size;
  }
};

const DataBox = ({
  color,
  data,
  dragConstraint,
  controls,
  custom,
  initial,
  startSection,
  animate,
  variants,
  active,
  ...props
}) => {
  useEffect(() => {
    if (startSection?.get() === 1) animate.start('animateQuick');
  }, [animate]);

  return (
    <BoxContainer
      custom={custom}
      initial='initial'
      animate={animate}
      variants={variants}
      color={color}
      href={data?.pdf !== '' ? data?.pdf : null}
      target='_next'
      {...props}>
      <h3 style={{ fontSize: `${getFontSize(data?.title?.length)}px` }}>
        {data?.title}
      </h3>
      <p>{data?.description}</p>
    </BoxContainer>
  );
};

export default DataBox;
