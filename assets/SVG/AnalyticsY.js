import * as React from 'react';
import { motion } from 'framer';

const variants = {
  initial: { scaleY: 1 },
  animate: { scaleY: [1.4, 1, 1.4] },
};

const Icon = ({ height, y, controls, ...props }) => {
  return (
    <motion.svg
      width={30}
      height={height}
      initial='initial'
      animate={controls}
      variants={variants}
      transition={{ duration: 1, repeat: Infinity }}
      viewBox='0 0 2 8'
      fill='none'
      style={{ bottom: 11, right: props?.right }}
      {...props}>
      <rect x={0.5} width={1.5} height={7.5} rx={0.5} fill='#F89631' />
    </motion.svg>
  );
};

export default Icon;
