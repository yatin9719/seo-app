import React from 'react';
import { Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';
import { Controller } from 'react-hook-form';
import useStyles from 'components/FormField/style';
import { getTheme } from 'components/FormField/theme';

const SelectField = ({ theme, options, ...props }) => {
  const classes = useStyles(getTheme(theme));
  return (
    <FormControl variant='outlined' className={classes.root}>
      <InputLabel htmlFor={props?.id}>{props?.label}</InputLabel>

      <Controller
        render={({ field }) => (
          <Select
            {...field}
            inputProps={{
              id: props?.id,
            }}>
            {options?.map((item, index) => (
              <MenuItem key={`option-${index}`} value={item?.value}>
                {item?.label}
              </MenuItem>
            ))}
          </Select>
        )}
        name={props?.name}
        control={props?.control}
      />
    </FormControl>
  );
};

export default SelectField;
