import React, { useState } from 'react';
import { EvolutionBoxContainers } from 'css/components/Evolution';
import { motion } from 'framer';

const EvolutionBox = (props) => {
  const [clicked, setClicked] = useState(false);

  const {
    options,
    icon,
    heading,
    className,
    initial,
    animate,
    exit,
    transition,
    detail,
    onClick,
  } = props;

  return (
    <EvolutionBoxContainers
      initial={initial}
      animate={animate}
      exit={exit}
      transition={transition}
      className={className}
      onClick={() => setClicked(!clicked)}
      {...options}>
      {icon}
      <div className='flip-card'>
        <div
          className='flip-card-inner'
          style={clicked ? { transform: 'rotateY(180deg)' } : {}}>
          <div className='flip-card-front'>
            <h4>{heading}</h4>
            {props.children}
          </div>
          <div className='flip-card-back'>
            <p>{detail}</p>
          </div>
        </div>
      </div>
    </EvolutionBoxContainers>
  );
};

export default EvolutionBox;
