import React, { useState, useContext } from 'react';
import EvolutionBox from 'components/Evolution /EvolutionBox';
import ThcCannabis from 'assets/SVG/ThcCannabis';
import Cannabis from 'assets/SVG/Cannabis';
import IndustrialHemp from 'assets/SVG/IndustrialHemp';
import StarImage from 'assets/Images/star.png';
import { Frame, AnimatePresence, motion } from 'framer';
import { checkContext } from 'pages/test';

import {
  Container,
  Heading,
  WrapperOne,
  WrapperTwo,
  Star,
} from 'css/components/Evolution';

const Evolution = React.memo(({ lab, ...props }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = () => setIsOpen(!isOpen);

  const context = useContext(checkContext);
  const y = context?.scrollbar;

  const labSticky =
    lab?.start && !lab?.endSection ? y - lab?.scrollHeight : null;

  return (
    <Container id='labSection'>
      <Frame
        style={{ backgroundColor: null, width: '100%', height: null }}
        y={labSticky}>
        <Heading>
          <h3>
            the evolution <small>OF</small> cannabis
          </h3>
          <p>
            What ingredients is your Cannabis manufacturing supply chain based
            on?
          </p>
        </Heading>
      </Frame>
      {lab?.start && (
        <Frame
          style={{ backgroundColor: null, width: '100%', height: null }}
          y={labSticky}
          top='120px'>
          <WrapperOne>
            <EvolutionBox
              icon={<ThcCannabis />}
              heading={!isOpen && 'Synthetic'}
              detail={
                !isOpen &&
                'Laboratory derived to produce artificial plant compounds'
              }
              onClick={toggleOpen}
              initial={{
                opacity: 0,
                translateX: -300,
              }}
              animate={{
                opacity: 1,
                translateX: 0,
              }}
              exit={{
                opacity: 0,
                translateX: -300,
              }}
              transition={{ delay: 0.5, duration: 0.5 }}
              layout
            />

            <EvolutionBox
              icon={<Cannabis />}
              heading='CANNABIS'
              detail='From the direct source.
                100% true to the plant.'
              initial={{
                opacity: 0,
                translateY: -150,
              }}
              animate={{
                opacity: 1,
                translateY: 0,
              }}
              exit={{
                opacity: 0,
                translateY: -150,
              }}
            />
            <EvolutionBox
              icon={<ThcCannabis />}
              heading='BOTANICAL'
              detail='Copied from non-source
              plants in attempt to recreate
              the true plant taste.'
              initial={{
                opacity: 0,
                translateX: 150,
              }}
              animate={{
                opacity: 1,
                translateX: 0,
              }}
              exit={{
                opacity: 0,
                translateX: 150,
              }}
              transition={{ delay: 0.5, duration: 0.5 }}
            />
            {lab?.['33%'] && (
              <div className='dottedLineX'>
                <motion.div
                  layout
                  initial={{ height: '0%' }}
                  animate={{ height: '100%' }}
                  transition={{ duration: 2 }}
                  style={{
                    overflow: 'hidden',
                  }}></motion.div>
              </div>
            )}
          </WrapperOne>
          <WrapperTwo className='secondWrapper'>
            {lab?.['33%'] ? (
              <div className='innerSection'>
                <AnimatePresence>
                  <EvolutionBox
                    key='1'
                    className='industrial'
                    icon={<IndustrialHemp />}
                    heading='INDUSTRIAL HEMP'
                    detail='Industrial Hemp Cultivars are bread for non-flower uses and do not produce diverse or significant terpene profiles'
                    initial={{
                      opacity: 0,
                      translateX: -150,
                    }}
                    animate={{
                      opacity: 1,
                      translateX: 0,
                    }}
                    exit={{
                      opacity: 0,
                      translateX: -150,
                    }}
                    transition={{ delay: 0.5, duration: 0.5 }}
                  />
                </AnimatePresence>
                <EvolutionBox
                  icon={<ThcCannabis />}
                  heading='LOW-THC CANNABIS'
                  detail='(Cannabis Sativa) Complex flower-lead and terpene-rich cultivars which have the THC removed.'
                  initial={{
                    opacity: 0,
                    translateY: -150,
                  }}
                  animate={{
                    opacity: 1,
                    translateY: 0,
                  }}
                  exit={{
                    opacity: 0,
                    translateY: -150,
                  }}
                  className='lowTHC'>
                  <Star>
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                    <img src={StarImage} alt='sparkling' />
                  </Star>
                </EvolutionBox>
                <EvolutionBox
                  icon={<ThcCannabis />}
                  heading='HIGH-THC
          CANNABIS'
                  detail='(Cannabis Sativa) High THC flower-lead and terpene-rich cultivars.
                  Produced and sold within the state market only.Trade and production restrictions mean brands can not scale a consistent formula.'
                  initial={{
                    opacity: 0,
                    translateX: 150,
                  }}
                  animate={{
                    opacity: 1,
                    translateX: 0,
                  }}
                  transition={{ delay: 0.5, duration: 0.5 }}
                />
              </div>
            ) : (
              <div style={{ height: '263px' }}></div>
            )}
            {lab?.['33%'] && (
              <>
                <div className='dottedLineY'>
                  <motion.div
                    initial={{
                      height: '0px',
                      width: '-10%',
                      opacity: 0,
                    }}
                    animate={{
                      height: ['11px', '11px', '150px'],
                      width: ['0%', '53%', '53%'],
                      opacity: [0, 1, 1],
                      times: [0.1, 0.15, 1],
                    }}
                    transition={{ delay: 1.2, duration: 2 }}></motion.div>
                </div>
              </>
            )}
          </WrapperTwo>
        </Frame>
      )}
    </Container>
  );
});

export default Evolution;
