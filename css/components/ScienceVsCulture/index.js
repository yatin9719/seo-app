import styled from 'styled-components';
import { motion } from 'framer';
import rightArrowIcon from 'assets/Images/right-arrow.png';
import leftArrowIcon from 'assets/Images/left-arrow.png';

const Container = styled.div`
  position: relative;
  min-height: 300vh;
  /* padding: 40px 0; */
  ${(props) =>
    props?.active
      ? `background-image: linear-gradient(
    -45deg,
    #e977026e,
    #64a9976e,
    #ef527c6e,
    #a47e5b6e,
    #94a7536e,
    #d096266e,
    #a85ba36e,
    #ffffff00
  );
  background-size: 1000% 1000%;
  animation: gradient 30s ease-in-out infinite;
  `
      : `animation: "";`}

  .science-frame {
    height: 100vh;
    /* padding: 30px 0; */
  }

  @media (min-width: 968px) {
    /* padding: 50px 0; */
    /* min-height: 300vh; */

    .science-frame {
      /* padding: 30px 0; */
    }
  }

  @keyframes gradient {
    0% {
      background-position: 0% 0%;
    }
    50% {
      background-position: 0% 100%;
    }
    100% {
      background-position: 0% 0%;
    }
    /* 100% {
      background-position: 0% 0%;
    } */
  }

  @media (max-width: 962px) {
    padding-bottom: 40px;
  }
  .moreProfile {
    overflow: auto;
    flex-wrap: nowrap;
    margin: 0;
    justify-content: ${(props) => (props?.moreProfile ? 'start' : 'center')};
    padding: 3vh 15px;

    @media (max-height: 667px) {
      padding: 2vh 15px;
    }

    > div {
      margin: 0;
      padding: 0 2.9vw;
      > svg {
        height: 44px;
        width: 40px;
      }
      > p {
        font-size: 10px;
      }
    }
  }
`;
const Heading = styled.div`
  text-align: center;
  /* padding-top: 1vh; */
  /* padding-top: 100px; */
  /* position: relative; */
  span.plusIcon {
    line-height: 10px;
    font-size: 13px;
    position: relative;
    top: -11px;
  }
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    margin: 0 auto;

    .smallText {
      font-size: 17.5px;
    }
  }
  h3 {
    font-size: 17.5px;
    font-family: 'robotobold';
    text-transform: uppercase;
    margin: 0 auto;
  }
  @media (max-width: 767px) {
    /* padding-top: 10px; */

    @media (max-height: 667px) {
      /* padding-top: 10px; */
    }

    h2 {
      line-height: 16pt;
      font-size: 16pt;
    }
    h3 {
      font-size: 12pt;
      max-width: 285px;
      font-family: 'roboto_condensedregular';
      text-transform: initial;
      display: none;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -7px;
    }
  }
  @media (max-width: 320px) {
    padding-top: 61px;
    h2 {
      line-height: 16pt;
      font-size: 11pt;
      max-width: 299px;
      padding-top: 25px;
    }
    h3 {
      font-size: 9pt;
      max-width: 285px;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -7px;
    }
  }
`;

const HiddenContainer = styled(motion.div)`
  overflow: hidden;
  max-height: 84.5px;

  @media (max-width: 768px) {
    max-height: 108.5px;
  }
  @media (max-width: 768px) and (max-height: 667px) {
    max-height: 84.5px;
  }
`;

const SliderContainer = styled.div`
  max-width: 1266px;
  margin: 0 auto;
  padding: 0 25px;
  min-height: 423px;

  .slick-list {
    min-height: 423px;
    @media (max-width: 768px) {
      min-height: 400px;
      @media (max-height: 667px) {
        min-height: 378px;
      }
    }
  }

  .slick-dots {
    @media (max-width: 768px) and (max-height: 667px) {
      bottom: -19px;
    }
  }
  .slick-dots > li > button:before {
    color: #58a08f;
    font-size: 20px;
  }

  .slick-next {
    background-image: url(${rightArrowIcon});
    right: -28px;
    height: 75px;
    width: 30px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    ::before {
      display: none;
    }
  }
  .slick-disabled {
    background-image: url(${rightArrowIcon});
    right: -28px;
    height: 75px;
    width: 30px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    opacity: 0.4;
    ::before {
      display: none;
    }
  }
  .slick-prev {
    left: -28px;
    background-image: url(${leftArrowIcon});
    height: 75px;
    width: 30px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    ::before {
      display: none;
    }
  }
  @media (max-width: 667px) {
    padding: 0 10px;
    .slick-next,
    .slick-prev {
      display: none !important;
    }
  }
  @media (max-width: 768px) {
    min-height: 400px;
    @media (max-height: 667px) {
      min-height: 378px;
    }
  }
`;

const IconSection = styled(motion.div)`
  display: flex;
  justify-content: space-around;
  align-items: center;
  text-align: center;
  text-transform: uppercase;
  max-width: 992px;
  margin: 2vh auto;
  flex-wrap: wrap;
  width: 100%;
  position: relative;
  z-index: 9;
  > div {
    > svg {
      cursor: pointer;
      height: 60px;

      @media (min-height: 750px) {
        height: 80px;
      }
    }
    > p {
      cursor: pointer;
    }

    :hover {
      opacity: 0.8 !important;
    }
  }
  @media (min-height: 750px) {
    margin: 4vh auto;
  }
  @media (max-width: 768px) {
    margin: 80px auto 20px;
    justify-content: center;
    > div {
      margin: 15px 10px;
      > svg {
        height: 70px;
        cursor: pointer;
      }
    }
  }
`;

const BoxContainer = styled(motion.a)`
  background: #ffffff;
  text-decoration: none;
  border: 3px solid ${(props) => (props?.color ? props?.color : '#94a753')};
  box-sizing: border-box;
  box-shadow: 6px 6px 0px
    ${(props) => (props?.color ? props?.color + '91' : '#94a753')};
  border-radius: 7.5px;
  width: 319px;
  cursor: pointer;
  height: 65px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${(props) => (props?.color ? props?.color : '#94a753')};
  font-size: 18.5px;
  margin: 0 8px 26px;
  transition: color 0.5s ease-in-out, box-shadow 0.5s ease-in-out,
    border 0.5s ease-in-out;
  transform: translateY(10px);
  flex-direction: column;
  overflow: hidden;
  :hover {
    opacity: 0.9;
  }

  :active {
    opacity: 0.8;
  }

  > p {
    color: #6b6b6b;
    font-family: 'roboto_condensedregular';
    display: block;
  }
  > h3 {
    text-transform: uppercase;
    color: ${(props) => (props?.color ? props?.color : '#94a753')};
    font-size: 1em;
    text-align: center;
  }
  @media (max-width: 768px) {
    font-size: 17.5px;
    margin: 0 8px 20px;
  }
  @media (max-width: 667px) {
    width: 100%;
    margin: 0 auto 15px;
    max-width: 319px;
  }
  @media (max-height: 667px) {
    height: 60px;
  }
`;
const BoxWrapper = styled(motion.div)`
  display: flex !important;
  justify-content: center;
  max-width: 1005px;
  outline: none;
  margin: 0 auto;
  width: 100%;
  /* min-height: 198px; */

  /* margin-top: 80px; */

  @media (min-width: 768px) {
    flex-wrap: wrap;
  }

  > p {
    margin: 20px 0;
    font-size: 2rem;
    text-align: center;

    @media (max-width: 768px) {
      font-size: 1rem;
    }
  }

  @media (max-width: 968px) {
    margin-top: 0px;
  }
  @media (max-width: 667px) {
    display: grid !important;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: max-content;
    /* flex-direction: column; */
    /* min-height: 372px; */
    /* justify-content: flex-start; */
    > div {
      padding: 0 9px;
      h3 {
        font-size: 11px !important;
      }
      p {
        font-size: 14px;
        text-align: center;
      }
    }
    @media (max-height: 750px) {
      min-height: 240px;
    }
  }
`;

const PineWrapper = styled.div`
  padding: 35px 0 0 0;

  @media (max-width: 768px) and (max-height: 667px) {
    padding: 25px 0 0 0;
  }
`;

const PineProfiles = styled.p`
  text-decoration: underline;
  text-transform: capitalize;
  text-align: center;
  font-size: 14px;
  font-family: robotoregular;
  margin-bottom: 40px;
  color: ${(props) => (props?.color ? props?.color : '#94a753')};
`;
const RequestSampleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  max-width: 750px;
  margin: 0 auto;
  > a {
    max-width: 341px;
    width: 100%;
    margin: 0 5px;
    text-decoration: none;
    justify-content: center;
    cursor: pointer;
  }
  @media (max-width: 768px) {
    flex-direction: column;
    > a {
      max-width: 300px;
      margin: 0 auto;
      margin-bottom: 13px;
      width: 67%;
      padding: 0 20px;
      :last-child {
        margin-bottom: 0;
      }
    }
  }
`;
const RequestSample = styled.a`
  height: 60px;
  background: ${(props) => (props?.color ? props?.color : '#94a753')};
  text-align: center;
  width: fit-content;
  /* margin: 0 auto; */
  padding: 0 37px;
  display: flex;
  font-size: 20px;
  align-items: center;
  color: #fff;
  border-radius: 7.5px;
  transition: background 0.5s ease-in-out;

  :active {
    opacity: 0.6;
  }

  :hover {
    opacity: 0.8;
  }

  @media (max-width: 768px) {
    font-size: 15px;
    margin: 0 auto;
    max-width: 300px;
  }
`;
const Wrapper = styled.div`
  position: relative;
  width: 756px;
  display: flex;
  align-items: center;
  svg {
    margin: 0 auto;
  }
  img.chatRightBoxIcon {
    position: absolute;
    left: 288px;
    z-index: 1;
    top: 35px;
  }
`;
const JustinWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  @media (max-width: 768px) {
    display: none;
  }
  .chatBox {
    max-width: 292px;
    font-size: 15px;
    text-align: center;
    padding: 11px 20px;
    font-family: 'robotoregular';
    /* border: 2px solid; */
    border: 2px solid #4a4a4a;
    background: #fff;
    border-radius: 6px;
    position: absolute;
    top: 0;
  }
`;
const SeeMoreProfile = styled.div`
  font-family: 'roboto_condensedregular';
  padding-bottom: 15px;
  text-align: center;

  @media (min-width: 768px) {
    display: none;
  }
`;
const MobileViewSlider = styled(motion.div)`
  /* @media (min-width: 968px) {
    display: none;
  } */
`;

const DesktopWrapper = styled.div`
  @media (max-width: 968px) {
    display: none;
  }
`;

export {
  Container,
  Heading,
  IconSection,
  BoxContainer,
  BoxWrapper,
  RequestSample,
  JustinWrapper,
  DesktopWrapper,
  Wrapper,
  PineProfiles,
  RequestSampleWrapper,
  SeeMoreProfile,
  MobileViewSlider,
  SliderContainer,
  HiddenContainer,
  PineWrapper,
};
