import * as React from 'react';

const Icon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    width={115 * 3.25}
    height={70.13 * 3.05}
    viewBox='0 0 115 70.13'
    {...props}>
    <g id='Layer_2' data-name='Layer 2' transform='translate(1 1)'>
      <g id='Layer_1' data-name='Layer 1' transform='translate(0 0)'>
        <path
          id='Path_9217'
          data-name='Path 9217'
          d='M114,69.13V1H1'
          transform='translate(-1 -1)'
          fill='none'
          stroke='#58a08f'
          strokeLinecap='round'
          strokeLinejoin='bevel'
          strokeWidth={2}
          strokeDasharray='3 3'
        />
      </g>
    </g>
  </svg>
);

export default Icon;
