import { Modal } from 'react-responsive-modal';
import 'react-responsive-modal/styles.css';
import styled from 'styled-components';

const CustomModal = ({ onClose, open, stage, ...props }) => {
  return (
    <Modal open={open} onClose={onClose} closeOnOverlayClick={false} center>
      <Container>
        <div>
          {stage === 'success' && (
            <>
              <h3>THANK YOU.</h3>
              <p>We’ll reach out with more details shortly.</p>
            </>
          )}

          {stage === 'error' && (
            <>
              <h3>HOLD ON.</h3>
              <p>
                AN ERROR HAS OCCURRED. PLEASE CHECK THE EMAIL ADDRESS IS VALID
                BEFORE RE-SUBMITTING THE FORM.
              </p>
            </>
          )}
        </div>
      </Container>
    </Modal>
  );
};

export default CustomModal;

const Container = styled.div`
  width: calc(90vw - 40px);
  max-width: 750px;
  min-height: 420px;
  display: flex;
  align-items: center;
  justify-content: center;
  p {
    font-family: 'robotoregular';
    text-align: center;
    padding: 11px;
    font-size: 25px;
    max-width: 547px;
    margin: 0 auto;
    line-height: 40px;
  }
  h3 {
    font-size: 60px;
    text-align: center;
    font-family: 'hansonbold';
  }

  @media (max-width: 968px) {
    p {
      font-size: 18px;
      line-height: 25px;
    }
    h3 {
      font-size: 30px;
    }
  }
  @media (max-width: 767px) {
    min-height: 250px;
    padding: 15px 0;
  }
`;
