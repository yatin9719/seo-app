import * as React from 'react';

const Icon = (props) => (
  <svg width={27} height={27} xmlns='http://www.w3.org/2000/svg' {...props}>
    <title>{'background'}</title>
    <path fill='none' d='M-1-1h582v402H-1z' />
    <g>
      <title>{'Layer 1'}</title>
      <path
        stroke='null'
        fill='#3d3d3d'
        style={{
          fill: '#3d3d3d',
        }}
        d='M12.385.16a12.262 12.225 0 1012.262 12.215A12.262 12.225 0 0012.385.16zm0 23.453a11.272 11.238 0 1111.272-11.238 11.282 11.248 0 01-11.272 11.238z'
        className='prefix__cls-1'
      />
      <path
        fill='#3d3d3d'
        style={{
          fill: '#3d3d3d',
        }}
        d='M18.42 6.35a.5.5 0 00-.71 0l-5.33 5.33-5.33-5.33a.49.49 0 10-.7.7l5.33 5.33-5.33 5.33a.5.5 0 000 .71.47.47 0 00.35.15.49.49 0 00.35-.15l5.33-5.33 5.33 5.33a.51.51 0 00.36.15.5.5 0 00.35-.15.5.5 0 000-.71l-5.33-5.33 5.33-5.33a.48.48 0 000-.7z'
        className='prefix__cls-1'
      />
    </g>
  </svg>
);

export default Icon;
