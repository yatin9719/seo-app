import React, { useEffect, useMemo, useState, useRef } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import parse from 'autosuggest-highlight/parse';
import throttle from 'lodash/throttle';
import useStyles from 'components/AutoCompleteField/style';
import { getTheme } from 'components/AutoCompleteField/theme';

const loadScript = (src, position, id) => {
  if (!position) {
    return;
  }

  const script = document.createElement('script');
  script.setAttribute('async', '');
  script.setAttribute('id', id);
  script.src = src;
  position.appendChild(script);
};

const autocompleteService = { current: null };

export const GoogleMaps = ({ theme, ...props }) => {
  const [value, setValue] = useState(null);
  const [inputValue, setInputValue] = useState('');
  const [options, setOptions] = useState([]);
  const loaded = useRef(false);
  const classes = useStyles(getTheme(theme));
  const [sessionToken, setSessionToken] = useState(null);

  useEffect(() => {
    setSessionToken(getToken());
  }, []);

  if (typeof window !== 'undefined' && !loaded.current) {
    if (!document.querySelector('#google-maps')) {
      loadScript(
        `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_API_KEY}&libraries=places`,
        document.querySelector('head'),
        'google-maps'
      );
    }

    loaded.current = true;
  }

  const getToken = () => {
    if (window.google) {
      let token = new window.google.maps.places.AutocompleteSessionToken();
      return token;
    } else {
      return null;
    }
  };

  const fetch = useMemo(
    () =>
      throttle((request, callback) => {
        autocompleteService.current.getPlacePredictions(request, callback);
      }, 200),
    []
  );

  useEffect(() => {
    let active = true;
    if (!autocompleteService.current && window.google) {
      autocompleteService.current = new window.google.maps.places.AutocompleteService();
    }
    if (!autocompleteService.current) {
      return undefined;
    }

    if (inputValue === '') {
      setOptions(value ? [value] : []);
      return undefined;
    }

    fetch({ input: inputValue, sessionToken: sessionToken }, (results) => {
      if (active) {
        let newOptions = [
          {
            description: inputValue,
            structured_formatting: {
              main_text: inputValue,
              main_text_matched_substrings: [
                { length: inputValue.length, offset: 0 },
              ],
            },
          },
        ];

        if (value) {
          newOptions = [value];
        }

        if (results) {
          newOptions = [...newOptions, ...results];
        }

        setOptions(newOptions);
      }
    });

    return () => {
      active = false;
    };
  }, [value, inputValue, fetch]);

  return (
    <Autocomplete
      id='google-map-demo'
      className={classes.root}
      getOptionLabel={(option) =>
        typeof option === 'string' ? option : option.description
      }
      filterOptions={(x) => x}
      options={options}
      autoComplete
      includeInputInList
      filterSelectedOptions
      value={value}
      onChange={(event, newValue) => {
        setOptions(newValue ? [newValue, ...options] : options);
        setValue(newValue);
        setSessionToken(getToken());
      }}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      renderInput={(params) => (
        <TextField
          {...params}
          label='Add a location'
          variant='outlined'
          fullWidth
          {...props}
        />
      )}
      renderOption={(option) => {
        const matches =
          option?.default ||
          option?.structured_formatting?.main_text_matched_substrings;
        const parts =
          option?.default ||
          parse(
            option?.structured_formatting?.main_text,
            matches?.map((match) => [
              match?.offset,
              match?.offset + match?.length,
            ])
          );

        return (
          <Grid container alignItems='center'>
            <Grid item>
              <LocationOnIcon color={'green'} className={classes.icon} />
            </Grid>
            <Grid item xs>
              {parts?.map((part, index) => (
                <span
                  key={index}
                  style={{ fontWeight: part.highlight ? 700 : 400 }}>
                  {part.text}
                </span>
              ))}

              <Typography variant='body2' color='textSecondary'>
                {option.structured_formatting.secondary_text}
              </Typography>
            </Grid>
          </Grid>
        );
      }}
    />
  );
};

export default GoogleMaps;
