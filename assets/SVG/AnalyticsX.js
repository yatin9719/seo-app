import * as React from 'react';
import { motion } from 'framer';

const Icon = (props) => (
  <motion.svg
    width={27}
    height={9}
    viewBox='0 0 8 1'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    style={{ bottom: 8, right: 150 }}
    {...props}>
    <rect
      y={1}
      width={0.5}
      height={5}
      rx={0.5}
      transform='rotate(-90 0 1)'
      fill='#F89631'
    />
  </motion.svg>
);

export default Icon;
