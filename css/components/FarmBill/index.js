import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';

const Container = styled.div`
  background-image: url(${LegalBg});
  background-repeat: no-repeat;
  background-position: top;
  /* background-size: contain; */
  position: relative;
  /* overflow-x: clip; */
`;

const Wrapper = styled.div``;
const Heading = styled.div`
  text-align: center;
  padding-top: 77px;
  position: relative;
  small {
    font-size: 12px;
    position: relative;
    top: -12px;
  }
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    max-width: 79%;
    line-height: 30px;
    margin: 0 auto;
    span {
      color: #58a08f;
    }
  }
  h3 {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    margin-top: 10px;
    font-weight: unset;
  }
  @media (max-width: 767px) {
    h2 {
      font-size: 16pt;
      line-height: 16pt;
      width: 237px;
    }
    h3 {
      font-size: 12pt;
      max-width: 83%;
      margin: 10px auto auto;
      font-weight: unset;
    }
    small {
      font-size: 10px;
      line-height: 10px;
      position: relative;
      top: -8px;
    }
  }
  @media (max-width: 320px) {
    h2 {
      font-size: 11pt;
      line-height: 16pt;
    }
    h3 {
      font-size: 9pt;
      max-width: 83%;
      font-weight: unset;
      margin: auto;
    }
    small {
      font-size: 10px;
      line-height: 10px;
      position: relative;
      top: -8px;
    }
  }
`;
const FarmAct = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 1196px;
  margin: 0 auto;
  padding: 0 50px;
  margin-top: 130px;
  position: relative;
  > div {
    flex: 1;
    max-width: 326px;
    padding: 15px;
    display: flex;
    flex-direction: column;
    align-items: center;
    > h4 {
      font-family: 'robotoregular';
      font-size: 17.5px;
      line-height: 30px;
      font-weight: unset;
      margin-top: 45px;
    }
  }
  @media (max-width: 767px) {
    flex-direction: column;
    align-items: center;
    margin-top: 50px;
    padding: 0 5px;
    > div {
      max-width: 302px;
      > h4 {
        margin-top: 18px;
        font-size: 12pt;
        line-height: 28px;
      }
    }
  }
`;
const IconSection = styled.div`
  min-height: 114px;
  display: flex;
  align-items: center;
`;
export { Container, Wrapper, Heading, FarmAct, IconSection };
