import React, { useState, useEffect } from 'react';
import { useViewportScroll } from 'framer';
import { HeadContainer } from 'css/components/Header';
import { useLocation } from 'react-router-dom';
import CloseIcon from 'assets/SVG/CloseIcon';
import ExpandIcon from 'assets/SVG/ExpandIcon';
import MainLogo from 'assets/SVG/MainLogo';

const Header = ({ scrollDir, headerCollapse, ...props }) => {
  const [openMenu, setOpenMenu] = useState(false);
  const handleClick = () => setOpenMenu(!openMenu);
  const location = useLocation();
  const { pathname } = location;
  const { controls } = props;
  const { scrollY } = useViewportScroll();
  const { scrollHeight } = props;

  useEffect(() => {
    const unsubscribe = scrollY.onChange(() => {
      if (headerCollapse?.get() === -68 && openMenu) setOpenMenu(false);
    });
    return () => unsubscribe();
  });

  const goToShop = () => {
    window.scrollTo({ top: scrollHeight + 5, behavior: 'smooth' });
    if (openMenu) setOpenMenu(false);
  };

  return (
    <HeadContainer
      initial={{ y: 0 }}
      variant={pathname !== '/'}
      animate={controls}
      transition={{ delay: 0.05, duration: 0.8 }}>
      <nav className='navbar'>
        <div className='navbar-container'>
          <div className='menu-icon' onClick={handleClick}>
            {openMenu ? <CloseIcon /> : <ExpandIcon />}
          </div>
          <div>
            <ul className={openMenu ? 'nav-menu active' : 'nav-menu'}>
              <li className='nav-item'>
                <a
                  href='/contact'
                  className={`nav-links ${
                    pathname === '/contact' ? 'active' : ''
                  }`}>
                  CONTACT
                </a>
              </li>
              <li
                className='nav-item right-float'
                onClick={() => {
                  if (pathname === '/') goToShop();
                }}>
                <a
                  className='nav-links shop'
                  href={pathname === '/' ? null : '/samples'}>
                  SAMPLES
                </a>
              </li>
            </ul>
          </div>
          <div
            className='navbar-logo'
            onClick={() =>
              window.location.pathname === '/'
                ? window.scrollTo({ top: 0, behavior: 'smooth' })
                : (window.location.href = '/')
            }>
            <MainLogo times={window.innerWidth <= 968 ? 1.5 : 2} />
          </div>
        </div>
      </nav>
    </HeadContainer>
  );
};

export default Header;
