import React from 'react';
import Certified from 'assets/SVG/CertifiedIcon';
import {
  Container,
  Heading,
  IconSection,
  Button,
} from 'css/components/Promise';

const Promise = () => {
  return (
    <Container>
      <Heading>
        <h2>WE ARE A Certified Cannabis Supply-chain</h2>
        <h3>Built to scale across industries and around the globe</h3>
      </Heading>
      <IconSection>
        <Certified />
      </IconSection>
      <Button href='/contact' className='button is-animating'>
        Don't miss the future, get in touch now
      </Button>
    </Container>
  );
};

export default Promise;
