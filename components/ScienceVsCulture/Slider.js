import React, { useEffect, useState, useRef } from 'react';
import Slider from 'react-slick';
import { useMotionValue } from 'framer';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import {
  BoxWrapper,
  SliderContainer,
  HiddenContainer,
} from 'css/components/ScienceVsCulture';
import DataBox from 'components/ScienceVsCulture/DataBox';

const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  adaptiveHeight: true,
};

const showVariants = {
  initial: {
    y: 100,
    transition: { duration: 0.1, type: 'tween', stiffness: 200 },
  },
  animate: (custom) => {
    if (custom === 0)
      return {
        transition: { delay: 0.75, type: 'tween', stiffness: 200 },
        y: 0,
      };
    return {
      transition: { delay: 0.75 + 0.2 * custom, type: 'tween', stiffness: 200 },
      y: 0,
    };
  },
  animateQuick: {
    transition: { delay: 0.1, type: 'tween', stiffness: 200 },
    y: 0,
  },
};

const FlavorSlider = ({
  data,
  controls,
  profiles,
  active,
  startSection,
  initial,
  animate,
  variants,
}) => {
  const [displayData, setDisplayData] = useState([]);
  const slider = useRef(null);
  const isAnimating = useMotionValue(0);

  useEffect(() => {
    if (data) {
      let show = 15;
      if (window.innerWidth < 768) {
        show = 10;
        if (window.innerHeight < 750) show = 10;
      }
      let times = Math.ceil(data.length / show);
      let allData = [];
      for (let i = 1; i <= times; i++) {
        let start = (i - 1) * show;
        let arr = data.slice(start, start + show);
        allData.push(arr);
      }
      setDisplayData(allData);
    }
  }, [data, window.innerWidth, window.innerHeight]);

  useEffect(() => {
    // setTimeout(() => {
    //   slider?.current?.slickGoTo(0, false);
    // }, 200);
  }, [active]);

  // const events = {
  //   afterChange: () => isAnimating.set(0),
  //   beforeChange: () => isAnimating.set(1),
  // };

  useEffect(() => {
    animate.start('initial');
  }, []);

  return (
    <SliderContainer color={profiles?.[active]?.color}>
      <Slider ref={slider} {...settings}>
        {displayData.map((slides, i) => (
          <BoxWrapper key={`$box-${i}`}>
            {slides.map((item, index) => (
              <HiddenContainer key={`${item?.heading}-${index}`}>
                <DataBox
                  initial='initial'
                  custom={index}
                  animate={animate}
                  variants={showVariants}
                  active={active}
                  startSection={startSection}
                  color={profiles?.[item?.profile]?.color}
                  data={item}
                  controls={controls}
                />
              </HiddenContainer>
            ))}
          </BoxWrapper>
        ))}
      </Slider>
    </SliderContainer>
  );
};

export default FlavorSlider;
