import React, { useState, useRef } from 'react';
import ReactGA from 'react-ga';
import { Frame } from 'framer';
import WorldMapSVG from 'assets/SVG/WorldMap';
import UsMap from 'assets/SVG/UsMap';
import UsMapState from 'assets/SVG/USMapState';
import { Container, Heading, MapWrapper } from 'css/components/WorldMap';
import Modal from 'components/Modal';

const frameStyle = {
  backgroundColor: null,
  height: null,
  width: '100%',
};

const WorldMap = ({ y, map }) => {
  const [openModal, setOpenModal] = useState(false);
  const [active, setActive] = useState(undefined);
  const [email, setEmail] = useState('');
  const inputField = useRef(null);
  const containerRef = useRef(null);

  const closeModal = () => {
    inputField?.current?.blur();
    setEmail('');
    setOpenModal(!openModal);
  };

  return (
    <Container id='worldMapSection' ref={containerRef}>
      <Frame
        id='essentialSection'
        style={{ ...frameStyle, height: '100vh', position: 'sticky' }}
        top='0px'>
        <div>
          <Heading>
            <div>
              <h2>
                Can I buy Cannabis<span className='plusIcon'>+</span>
              </h2>
              <h2>
                Essential Oil in my{' '}
                {window.innerWidth <= 768 ? 'state' : 'location'}?
              </h2>
            </div>

            <h3>Click your location to learn more</h3>
          </Heading>
          <MapWrapper>
            <WorldMapSVG
              onClick={(e) => {
                if (e?.target?.id && !e?.target?.id?.includes('polygon')) {
                  setActive(e?.target?.id);
                  !openModal && setOpenModal(true);
                  ReactGA.modalview('world_map_model_open');
                }
              }}
            />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <UsMap
                onClick={(e) => {
                  if (e?.target?.id && !e?.target?.id?.includes('prefix')) {
                    setActive(e?.target?.id);
                    !openModal && setOpenModal(true);
                    ReactGA.modalview('world_map_model_open');
                  }
                }}
              />
              <UsMapState
                onClick={(e) => {
                  if (e?.target?.id && !e?.target?.id?.includes('polygon')) {
                    setActive(e?.target?.id);
                    !openModal && setOpenModal(true);
                    ReactGA.modalview('world_map_model_open');
                  }
                }}
              />
            </div>
            <Modal
              open={openModal}
              active={active}
              email={email}
              setEmail={setEmail}
              onClose={closeModal}
              scrollHeight={
                containerRef?.current?.getBoundingClientRect()?.top +
                document.documentElement.scrollTop
              }
              inputRef={inputField}
              setActive={setActive}
            />
          </MapWrapper>
        </div>
      </Frame>
    </Container>
  );
};

export default WorldMap;
