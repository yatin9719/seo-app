import React from 'react';
import { TextField } from '@material-ui/core';
import useStyles from 'components/TextAreaField/style';
import { getTheme } from 'components/TextAreaField/theme';

const FormField = ({ theme, rows, rowsMax, ...props }) => {
  const classes = useStyles(getTheme(theme));
  return (
    <TextField
      className={classes.root}
      variant='outlined'
      multiline
      rows={rows || 2}
      rowsMax={rowsMax || Infinity}
      {...props}
    />
  );
};

export default FormField;
