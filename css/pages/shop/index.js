import styled from 'styled-components';
import { motion } from 'framer';

const Container = styled(motion.div)`
  width: 100%;
`;

const BodyContainer = styled.div`
  margin-top: 60px;
  padding: 60px 40px;
`;

const BodyWrapper = styled.div`
  max-width: 900px;
  min-height: 500px;
  padding: 30px;
  margin: 0 auto;
  text-align: center;

  > h1 {
    text-transform: uppercase;
    font-size: 26px;
  }

  > p {
    font-size: 14px;
    font-family: 'robotoregular';
  }

  @media (max-width: 964px) {
    padding: 10px 0;
  }
`;

const FormWrapper = styled.div`
  margin-top: 50px;
  max-width: 968px;
`;

const Row = styled.div`
  display: flex;

  @media (max-width: 768px) {
    display: block;
  }
`;

const Column = styled.div`
  font-size: 12px;
  font-family: 'robotoregular300';
  color: #1e1e1e;
  width: 100%;
  padding: 0 5px;
  margin-bottom: 10px;
  .SelectInput {
    > div {
      background-color: #efefef;
      border: none;
      padding-left: 14px;
      box-shadow: none;
      ::placeholder {
        color: #1e1e1e;
      }
    }
    .indicatorSeparator {
      display: none;
    }
  }
  > input {
    -webkit-appearance: none;
    -moz-appearance: none;
    width: 100%;
    height: 40px;
    border-radius: 5px;
    background-color: #efefef;
    outline: none;
    border: none;
    padding: 0 24px 0;
    font-family: 'robotoregular300';
    ::placeholder {
      color: #1e1e1e;
    }
  }

  > textarea {
    width: 100%;
    border-radius: 5px;
    background-color: #efefef;
    outline: none;
    border: none;
    padding: 12px 24px 0;
    font-family: 'robotoregular300';
    ::placeholder {
      color: #1e1e1e;
    }
  }

  > p {
    color: #d72700;
    font-weight: bold;
    text-transform: capitalize;
  }
`;

const MultiSelect = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(min(50%, 95px), 1fr));
  margin-bottom: 25px;
`;

const MultiSelectItems = styled.div`
  font-family: 'robotoregular300';
  display: grid;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  grid-template-rows: 70px 20px;
  svg {
    margin: 0 auto;
  }
`;
const SubmitAction = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  .captcha {
    transform: scale(0.8);
  }
  .submitBtn {
    -webkit-appearance: none;
    -moz-appearance: none;
    font-size: clamp(16px, 2vh, 20px);
    background: #58a08f;
    border-radius: 7.5px;
    width: fit-content;
    padding: 19px 45px;
    color: #fff;
    text-transform: uppercase;
    border: none;
    font-family: 'hansonbold';
    cursor: pointer;
    flex: 1;
    max-width: 262px;
    min-width: 262px;
    :active {
      opacity: 0.8;
    }
    :hover {
      opacity: 0.8;
    }
    .rc-anchor-normal .rc-anchor-content {
      width: 154px;
    }
  }

  .disabled {
    background: #cfcfcf;
    cursor: not-allowed;
  }
  .newsCheckbox {
    background: #58a08f;
    border-radius: 7.5px;
    width: fit-content;
    padding: 17px 17px 17px 15px;
    color: #fff;
    font-family: 'robotoregular300';
    flex: 1;
    max-width: 262px;
    min-width: 262px;

    .container {
      display: block;
      position: relative;
      padding-left: 35px;
      /* cursor: pointer; */
      font-size: 20px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0;
      width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
      position: absolute;
      top: 0;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #fff;
      border-radius: 2.5px;
    }

    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
      background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container input:checked ~ .checkmark {
      background-color: #ffffff;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
      content: '';
      position: absolute;
      display: none;
    }

    /* Show the checkmark when checked */
    .container input:checked ~ .checkmark:after {
      display: block;
    }

    /* Style the checkmark/indicator */
    .container .checkmark:after {
      left: 9px;
      top: 5px;
      width: 5px;
      height: 10px;
      border: solid #58a08f;
      border-width: 0 3px 3px 0;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
    }
  }
`;

export {
  Container,
  BodyWrapper,
  BodyContainer,
  FormWrapper,
  Row,
  Column,
  MultiSelect,
  MultiSelectItems,
  SubmitAction,
};
