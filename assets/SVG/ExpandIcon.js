import * as React from 'react';

const Icon = (props) => (
  <svg width={27} height={27} xmlns='http://www.w3.org/2000/svg' {...props}>
    <title>{'background'}</title>
    <path fill='none' d='M-1-1h582v402H-1z' />
    <g>
      <title>{'Layer 1'}</title>
      <path
        stroke='null'
        fill='#3d3d3d'
        d='M12.385.212a12.173 12.173 0 1012.173 12.163A12.173 12.173 0 0012.385.212zm0 23.354a11.19 11.19 0 1111.19-11.19 11.2 11.2 0 01-11.19 11.19z'
        className='prefix__cls-1'
        style={{
          fill: '#3d3d3d',
        }}
      />
      <path
        fill='#3d3d3d'
        d='M19.19 9.62l-6.81 6.81-6.81-6.81a.5.5 0 00-.7 0 .5.5 0 000 .71L12 17.49a.49.49 0 00.35.15.51.51 0 00.36-.15l7.16-7.16a.5.5 0 000-.71.51.51 0 00-.68 0z'
        className='prefix__cls-1'
        style={{
          fill: '#3d3d3d',
        }}
      />
    </g>
  </svg>
);

export default Icon;
