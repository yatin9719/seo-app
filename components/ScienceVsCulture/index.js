import React, { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useAnimation, useMotionValue, Frame, useViewportScroll } from 'framer';
import { useLocation } from 'react-router-dom';

import CitrusIcon from 'assets/SVG/CitrusIcon';
import FruitIcon from 'assets/SVG/FruitIcon';
import GasIcon from 'assets/SVG/GasIcon';
import PineIcon from 'assets/SVG/PineIcon';
import SweetIcon from 'assets/SVG/SweetIcon';
import ExoticIcon from 'assets/SVG/ExoticIcon';
import SourIcon from 'assets/SVG/SourIcon';
import {
  Container,
  Heading,
  RequestSample,
  PineProfiles,
  RequestSampleWrapper,
  SeeMoreProfile,
  MobileViewSlider,
  PineWrapper,
  DesktopWrapper,
  BoxWrapper,
  HiddenContainer,
} from 'css/components/ScienceVsCulture';
import { SET_CATEGORY } from 'redux/actions/marquee';
import { SET_PRODUCTS } from 'redux/actions/products';
import { client } from 'utils';
import FlavorSlider from 'components/ScienceVsCulture/Slider';
import ProfileSection from 'components/ScienceVsCulture/Profiles';
import { inView } from 'utils';
import AnimatedLogo from 'assets/Gifs/TBF_loading_spinner4_small.gif';

const profiles = {
  citrus: {
    color: '#E97702',
    bgColor: '#a08f5833',
    data: {},
    Icon: CitrusIcon,
  },
  exotic: {
    color: '#64A997',
    bgColor: '#a08f5833',
    data: {},
    Icon: ExoticIcon,
  },
  fruit: { color: '#EF527D', bgColor: '#a0586333', data: {}, Icon: FruitIcon },
  gas: { color: '#A0744B', bgColor: '#a0865833', data: {}, Icon: GasIcon },
  pine: { color: '#7D8934', bgColor: '#58a08f33', data: {}, Icon: PineIcon },
  sour: { color: '#D09626', bgColor: '#a08f5833', data: {}, Icon: SourIcon },
  sweet: { color: '#A85BA3', bgColor: '#8a58a033', data: {}, Icon: SweetIcon },
};

const iconVariants = {
  initial: { opacity: 0 },
  category: { opacity: 0 },
  animate: {
    opacity: 1,
  },
};

const profileVariants = {
  initial: { opacity: 0 },
  category: (obj) => {
    return {
      opacity: obj?.opacity || 1,
    };
  },
  animate: (obj) => {
    return {
      transition: { delay: 0.15 * obj?.delay, duration: 0.15 },
      opacity: obj?.opacity || 1,
    };
  },
};

const productVariants = {
  initial: { opacity: 0, transition: { duration: 0.1 } },
  category: { opacity: 0 },
  animate: {
    opacity: 1,
    transition: { delay: 3, duration: 6 },
  },
};

const ScienceVsCulture = ({ science, frameStyle }) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const { products, section } = useSelector((state) => state);
  const [combinedData, setCombinedData] = useState([]);
  const category = useSelector((state) => state?.marquee);
  const [active, setActive] = useState(category);
  const dragContainer = useRef(null);
  const controls = useAnimation();
  const [allData, setAllData] = useState(undefined);
  const { scrollHeight, clientHeight } = science || 0;
  const [viewMore, setViewMore] = useState(false);
  const [startSection, setStartSection] = useState(false);
  const crossed = useMotionValue(0);
  const [moreProfile, setMoreProfile] = useState(false);
  const iconControls = useAnimation();
  const profileControls = useAnimation();
  const productControls = useAnimation();
  const { scrollY } = useViewportScroll();
  const startSectionCheck = useMotionValue(0);
  const startSectionVal = useMotionValue(0);
  const preStartSectionCheck = useMotionValue(0);
  const [activeProducts, setActiveProducts] = useState(Object.keys(profiles));

  useEffect(() => {
    let isMounted = true;
    if (!products.length) {
      let temp = {};
      client.collection.fetchAllWithProducts().then((collections) => {
        collections?.forEach((collection) => {
          let title = collection.title.toLowerCase();
          let valid_products = [];
          collection?.products?.forEach((product) => {
            let store = false;
            let compliancePdf = '';
            let pdf = '';
            product?.tags?.forEach((tag) => {
              if (tag?.value === 'TBF Store') {
                store = true;
              }
              if (
                tag?.value.toLowerCase().includes('compliance') &&
                tag?.value.toLowerCase().includes('.pdf')
              )
                compliancePdf = tag?.value;
              if (
                tag?.value.toLowerCase().includes('.pdf') &&
                !tag?.value.toLowerCase().includes('compliance')
              )
                pdf = tag?.value;
            });
            if (store) {
              let arr = compliancePdf?.split('.');
              let split = arr?.[arr?.length - 2]?.split('_');
              let formatted = {
                title: `${product?.title}`,
                description: `Similar to ${product?.description}`,
                pdf: pdf,
              };
              if (compliancePdf) {
                formatted.complianceTitle = `${split?.[
                  split?.length - 1
                ]?.replace('pdf', '')}`;
                formatted.compliancePdf = compliancePdf;
              }
              valid_products.push({ ...formatted, profile: title });
            }
          });
          valid_products.sort((first, second) => {
            return (
              parseInt(first?.title?.split('#')?.[1]) -
              parseInt(second?.title?.split('#')?.[1])
            );
          });
          temp = { ...temp, [title]: [...valid_products] };
        });
        if (isMounted) {
          setAllData(temp);
        }
        dispatch({ type: SET_PRODUCTS, payload: temp });
      });
    } else {
      if (isMounted) setAllData(products);
    }
    return () => {
      isMounted = false;
    };
  }, []);

  const scrollEvents = (drop, section) => {
    let offsetY = scrollY.get();
    let preSection = offsetY >= 1000;
    let startSectionMinus = offsetY >= scrollHeight - clientHeight * (25 / 100);
    let startSectionStep = offsetY >= scrollHeight;
    if (preSection && preStartSectionCheck.get() === 0) {
      iconControls.start('initial');
      productControls.start('initial');
      profileControls.start('initial');
      productControls.stop();
      preStartSectionCheck.set(1);
    }
    if (startSectionStep && startSectionCheck.get() === 0) {
      iconControls.start('animate');
      productControls.start('animate');
      profileControls.start('animate');
      startSectionCheck.set(1);
    } else if (startSectionMinus && startSectionVal.get() === 0) {
      setStartSection(true);
      iconControls.start('animate');
      productControls.start('animate');
      profileControls.start('animate');
      startSectionVal.set(1);
    } else if (!startSectionMinus && startSectionVal.get() === 1) {
      setStartSection(false);
      iconControls.start('initial');
      productControls.start('initial');
      profileControls.start('initial');
      startSectionVal.set(0);
    }

    if (
      drop?.getBoundingClientRect()?.top >=
        section?.getBoundingClientRect()?.top &&
      crossed.get() === 0 &&
      scrollHeight
    ) {
      crossed.set(1);
    } else if (
      drop?.getBoundingClientRect()?.top <
        section?.getBoundingClientRect()?.top &&
      crossed.get() !== 0
    ) {
      crossed.set(0);
    }
  };

  useEffect(() => {
    let drop = document.getElementById('dropIcon');
    let section = document.getElementById('science-culture-section');

    iconControls.start('initial');
    productControls.start('initial');
    profileControls.start('initial');

    const unsubscribe = scrollY.onChange(() => {
      if (
        inView(scrollHeight - window.innerHeight, clientHeight, scrollY.get())
      )
        setTimeout(() => {
          requestAnimationFrame(() => scrollEvents(drop, section));
        }, 10);
    });
    return () => unsubscribe();
  }, []);

  useEffect(() => {
    if (allData) {
      let unused_profiles = Object.keys(profiles)?.filter(
        (item) => !Object.keys(allData)?.includes(item)
      );

      if (!unused_profiles.includes(category)) {
        // filterData(category);

        setActiveProducts([...unused_profiles, category]);
        setCombinedData(
          getCombinedData().filter((item) => item.profile === category)
        );
        profileControls.start('category');
      }
      if (!section) {
        if (unused_profiles.includes(category)) {
          {
            setActiveProducts([...Object.keys(profiles)]);
          }
        } else {
          setActiveProducts([...unused_profiles, category]);
          setCombinedData(
            getCombinedData().filter((item) => item.profile === category)
          );
        }
      }
    }
  }, [category]);

  const getCombinedData = () => {
    let arr = [];
    Object.values(allData).forEach((item) => {
      arr = [...arr, ...item];
    });
    return arr;
  };

  useEffect(() => {
    filterData(category);
    if (allData) {
      setCombinedData(getCombinedData());
    }
  }, [allData]);

  const filterData = (key) => {
    dispatch({ type: SET_CATEGORY, payload: key });
    setActive(key);
  };

  useEffect(() => {
    if (allData && profiles) {
      productControls.start('animateQuick');
      profileControls.start('category');
    }
  }, [activeProducts]);

  useEffect(() => {
    if (
      location.search &&
      location.search.split('to=')[1].split('&')[0] === 'shop'
    ) {
      setTimeout(() => {
        iconControls.start('animate');
        productControls.start('animate');
        profileControls.start('animate');
        startSectionCheck.set(1);
      }, 2500);
    }
  }, [location.search]);

  const unselectData = (key) => {
    if (
      activeProducts.includes(key) &&
      activeProducts.length >= Object.keys(profiles).length
    ) {
      setActiveProducts(activeProducts.filter((item) => item === key));
      setCombinedData(getCombinedData().filter((item) => item.profile === key));
    } else if (!activeProducts.includes(key)) {
      setActiveProducts(Object.keys(profiles).filter((item) => item === key));
      setCombinedData(getCombinedData().filter((item) => item.profile === key));
    } else if (activeProducts.includes(key)) {
      setActiveProducts(Object.keys(profiles));
      setCombinedData(getCombinedData());
    }
  };

  return (
    <>
      <Container
        color={profiles?.[active]?.bgColor}
        active={startSection}
        ref={dragContainer}
        moreProfile={moreProfile}
        id='science-culture-section'>
        <Frame
          className='science-frame'
          style={{
            ...frameStyle,
            position: 'sticky',
            zIndex: 2,
          }}
          top='0px'>
          {startSection && (
            <div
              style={{
                position: 'absolute',
                // top: '-10vh',
                width: '100%',
                height: '100%',
                overflow: 'hidden',
              }}></div>
          )}
          <div
            style={{
              position: 'relative',
              height: '100%',
              minHeight: '655px',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}>
            {!allData && (
              <div
                style={{
                  display: 'flex',
                  position: 'absolute',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  width: '100%',
                  zIndex: 2,
                  backgroundColor: 'rgb(0 0 0 / 20%)',
                }}>
                <img height={50} src={AnimatedLogo} alt='TBF Loader' />
              </div>
            )}
            {allData && (
              <>
                <Heading>
                  <h2>
                    Shop our Cannabis<span className='plusIcon'>+</span>{' '}
                    Essential Oils
                  </h2>
                  <h3>
                    Select a Flavor Profile to See The Available Essential Oil
                    Products
                  </h3>
                </Heading>
                <MobileViewSlider>
                  <ProfileSection
                    animate={iconControls}
                    variants={iconVariants}
                    allData={allData}
                    filterData={unselectData}
                    profiles={profiles}
                    profileControls={profileControls}
                    profileVariants={profileVariants}
                    activeProducts={activeProducts}
                  />

                  {Object.keys(profiles)?.map((profile) => {
                    let arr = [];
                    if (allData?.profile) arr.push(profile);
                    if (arr.length > 5) {
                      setMoreProfile(true);
                      return (
                        <SeeMoreProfile>
                          Swipe to see more profiles →
                        </SeeMoreProfile>
                      );
                    } else return null;
                  })}

                  <FlavorSlider
                    data={combinedData}
                    controls={controls}
                    profiles={profiles}
                    active={active}
                    startSection={startSectionVal}
                    initial={startSection ? 'animate' : 'initial'}
                    animate={productControls}
                    variants={productVariants}
                  />
                </MobileViewSlider>

                <PineWrapper>
                  {viewMore && (
                    <PineProfiles
                      onClick={() => {
                        setViewMore(false);
                      }}
                      color={profiles?.[active]?.color}>
                      {`More ${active} Varieties`}
                    </PineProfiles>
                  )}
                  <RequestSampleWrapper>
                    <RequestSample href='/samples' color='#58a08f'>
                      REQUEST SAMPLES
                    </RequestSample>
                  </RequestSampleWrapper>
                </PineWrapper>
              </>
            )}
          </div>
        </Frame>
      </Container>
    </>
  );
};

export default ScienceVsCulture;
