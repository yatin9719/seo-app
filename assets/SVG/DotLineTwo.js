import * as React from 'react';

const Icon = ({ width, height }) => (
  <svg width={width} height={height} viewBox='0 0 3 26' fill='none'>
    <path
      stroke='#58A08F'
      strokeWidth={2}
      strokeMiterlimit={16}
      strokeLinecap='round'
      strokeLinejoin='round'
      strokeDasharray='3 3'
      d='M1.5 1.478v23.34'
    />
  </svg>
);

export default Icon;
