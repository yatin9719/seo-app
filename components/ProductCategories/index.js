import React, { useEffect, useState } from 'react';
import { useAnimation, motion, useMotionValue, Frame } from 'framer';
import Vape from 'assets/SVG/Vape';
import Edibles from 'assets/SVG/Edibles';
import Concentrates from 'assets/SVG/Concentrates';
import InfusedFlower from 'assets/SVG/InfusedFlower';
import InfusedPaper from 'assets/SVG/InfusedPaper';
import Topicals from 'assets/SVG/Topicals';
import Tinctures from 'assets/SVG/Tinctures';
import Beverages from 'assets/SVG/Beverages';
import SpiritIcon from 'assets/SVG/SpiritIcon';
import BeerIcon from 'assets/SVG/BeerIcon';
import WineIcon from 'assets/SVG/WineIcon';
import GastronomyIcon from 'assets/SVG/Gastronomy';
import TobaccoInfusionIcon from 'assets/SVG/TobaccoIcon';
import NicotineIcon from 'assets/SVG/NicotineIcon';
import PerfumesIcon from 'assets/SVG/PerfumeIcon';
import SoapIcon from 'assets/SVG/SoapIcon';
import {
  Container,
  Heading,
  IconSection,
} from 'css/components/ProductCategories';
import { ButtonWrapper } from 'components/ProductCategories/styles';
import ToggleButton from 'components/ProductCategories/ToggleButton';

const frameStyle = {
  backgroundColor: null,
  height: null,
  width: '100%',
};

const ProductCategories = () => {
  const controls = useAnimation();
  const nonControls = useAnimation();
  const [isOn, setIsOn] = useState(false);
  const firstMaskAnimating = useMotionValue(0);
  const secondMaskAnimating = useMotionValue(0);

  const variants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1 },
  };

  useEffect(() => {
    if (!isOn) {
      controls.start('visible');
      nonControls.start('hidden');
      firstMaskAnimating.set(1);
      secondMaskAnimating.set(1);
    } else {
      controls.start('hidden');
      nonControls.start('visible');
      firstMaskAnimating.set(1);
      secondMaskAnimating.set(1);
    }
  }, [isOn, controls, nonControls]);

  const toggleSwitch = () => {
    if (firstMaskAnimating.get() === 0 && secondMaskAnimating.get() === 0)
      setIsOn(!isOn);
  };

  const cannabisMask = (
    <motion.rect
      initial={{ y: 0 }}
      animate={controls}
      onAnimationComplete={() => firstMaskAnimating.set(0)}
      transition={{ duration: 0.7 }}
      variants={{ hidden: { y: 90 }, visible: { y: 0 } }}
      width='150'
      height='150'
      style={{ fill: 'white' }}
    />
  );

  const nonCannabisMask = (
    <motion.rect
      initial={{ y: 0 }}
      animate={nonControls}
      onAnimationComplete={() => secondMaskAnimating.set(0)}
      transition={{ duration: 0.7 }}
      variants={{
        hidden: { y: 90 },
        visible: { y: 0 },
      }}
      width='150'
      height='150'
      style={{ fill: 'white' }}
    />
  );

  return (
    <Container>
      <Frame
        id='essentialSection'
        style={{ ...frameStyle, height: '100vh', position: 'sticky' }}
        top='0px'>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            height: '100%',
          }}>
          <Heading>
            <div>
              <h2>Essential Oil Formulated</h2>
              <h2>Product Categories</h2>
            </div>
          </Heading>
          <ButtonWrapper>
            <ToggleButton isOn={!isOn} onClick={toggleSwitch} />
          </ButtonWrapper>
          <IconSection pointer={!isOn}>
            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Vape'>
                  <Vape mask={cannabisMask} />
                </a>
                <div>
                  <SpiritIcon mask={nonCannabisMask} />
                </div>
              </div>

              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Vape'>Vape</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Beverages
              </motion.p>
            </div>
            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Edibles'>
                  <Edibles mask={cannabisMask} />
                </a>
                <div>
                  <BeerIcon mask={nonCannabisMask} />
                </div>
              </div>

              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Edibles'>Edibles</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Cosmetics
              </motion.p>
            </div>
            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Concentrates'>
                  <Concentrates mask={cannabisMask} />
                </a>
                <div>
                  <WineIcon mask={nonCannabisMask} />
                </div>
              </div>
              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Concentrates'>Concentrates</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Pharmaceuticals
              </motion.p>
            </div>

            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Infused_Flower'>
                  <InfusedFlower mask={cannabisMask} />
                </a>
                <div>
                  <GastronomyIcon mask={nonCannabisMask} />
                </div>
              </div>

              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Infused_Flower'>Infused Flower</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Food
              </motion.p>
            </div>

            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Infused_Paper'>
                  <InfusedPaper mask={cannabisMask} />
                </a>
                <div>
                  <TobaccoInfusionIcon mask={nonCannabisMask} />
                </div>
              </div>

              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Infused_Paper'>Infused Paper</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Tobacco Infusion
              </motion.p>
            </div>
            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Topicals'>
                  <Topicals mask={cannabisMask} />
                </a>
                <div>
                  <NicotineIcon mask={nonCannabisMask} />
                </div>
              </div>
              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Topicals'>Topicals</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Nicotine Vapes
              </motion.p>
            </div>
            <div style={{ cursor: isOn ? 'auto' : 'pointer' }}>
              <div>
                <a href='/samples?selection=Tinctures'>
                  <Tinctures mask={cannabisMask} />
                </a>
                <div>
                  <PerfumesIcon mask={nonCannabisMask} />
                </div>
              </div>
              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Tinctures'>Tinctures</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Fragrance
              </motion.p>
            </div>
            <div>
              <div>
                <a href='/samples?selection=Beverages'>
                  <Beverages mask={cannabisMask} />
                </a>
                <div>
                  <SoapIcon mask={nonCannabisMask} />
                </div>
              </div>
              <motion.p
                initial={{ opacity: 1 }}
                animate={controls}
                variants={variants}>
                <a href='/samples?selection=Beverages'>Beverages</a>
              </motion.p>
              <motion.p
                initial={{ opacity: 0 }}
                animate={nonControls}
                variants={variants}>
                Soap
              </motion.p>
            </div>
          </IconSection>
        </div>
      </Frame>
    </Container>
  );
};

export default ProductCategories;
