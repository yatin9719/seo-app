import styled from 'styled-components';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: rgb(255, 255, 255, 0.5);
  position: fixed;
  top: 0;
  z-index: 9999;
`;

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const LoaderWrapper = styled.div``;

export { Container, Wrapper, LoaderWrapper };
