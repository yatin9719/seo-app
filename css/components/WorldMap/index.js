import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';
const Container = styled.div`
  position: relative;
  background: #f4f4f4;
  min-height: 300vh;
  background: url(${LegalBg}),
    linear-gradient(180deg, #58a08f33 0%, #ffffff33 100%), #c4c4c433;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: top;

  > div > div {
    padding: 0 0 0 0;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow-x: hidden;

    @media (max-width: 767px) {
      justify-content: start;
      padding: 0 20px;
    }
  }
`;
const Heading = styled.div`
  text-align: center;
  padding: 50px 0;
  /* padding-top: 100px; */
  /* padding-top: 77px; */
  /* position: relative; */
  .plusIcon {
    line-height: 10px;
    font-size: 13px;
    position: relative;
    top: -11px;
  }
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    /* max-width: 682px; */
    margin: 0 auto;
    .smallText {
      font-size: 17.5px;
    }
  }
  h3 {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    margin: 0 auto;
    font-weight: unset;
    margin-top: 10px;
  }
  @media (max-width: 767px) {
    padding-top: 55px;
    h2 {
      line-height: 16pt;
      font-size: 16pt;
      max-width: 212px;
      margin: 0 auto;
    }
    h3 {
      font-size: 12pt;
      font-weight: unset;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -7px;
    }
  }
  @media (max-width: 320px) {
    padding-top: 61px;
    h2 {
      line-height: 16pt;
      font-size: 11pt;
      max-width: 256px;
      margin: 0 auto;
    }
    h3 {
      font-size: 9pt;
      font-weight: unset;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -7px;
    }
  }
`;

const MapWrapper = styled.div`
  svg {
    height: calc(100vh - 216px);
    margin: 0 auto;
    width: 100%;
    padding-bottom: 15px;
  }
  .UsMap {
    display: none;
  }
  .UsStateSVG {
    display: none;
    padding: 0 40px;
  }
  .webWorldMap {
    display: block;

    path:hover {
      cursor: pointer;
      fill: #4e8a7c;
    }
  }
  .react-responsive-modal-closeButton {
    opacity: 0.7;
    outline: none;
  }
  @media (max-width: 1024px) {
    svg {
      max-height: 500px;
      height: auto;
    }
  }
  @media (max-width: 767px) {
    height: auto;
    display: flex;
    align-items: center;
    transform: scale(1.25);
    justify-content: center;
    margin-top: 10vh;
    .webWorldMap {
      display: none;
    }
    .UsStateSVG {
      display: block;
      padding: 0 40px;
      path:active {
        fill: #4e8a7c;
      }
    }
    .UsMap {
      display: block;
      /* margin: -10vh 0 0 0; */
      path:active {
        fill: #4e8a7c;
      }
    }
    @media (max-height: 680px) {
      margin-top: 3vh;
    }
  }
  @media (max-width: 767px) and (-webkit-device-pixel-ratio: 3) {
    margin-top: 10vh;
  }
`;

export { Container, Heading, MapWrapper };
