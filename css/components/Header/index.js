import styled from 'styled-components';
import { motion } from 'framer';

const HeadContainer = styled(motion.div)`
  width: 100%;
  height: 68px;

  position: fixed;
  opacity: ${(props) => (props.stickyShow || props.toggle ? '0' : '1')};
  top: 0;
  z-index: 9;
  backdrop-filter: saturate(180%) blur(12px);
  background-color: ${(props) =>
    props?.variant ? '#d3d3d373' : 'rgba(255, 255, 255, 0.72)'};
  .navbar {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    padding: 0 10px;
  }

  .shop {
    padding: 4px 15px !important;
    border: 2.5px solid #1d715d;
    border-radius: 7.5px;
    color: #1d715d !important;

    :hover {
      opacity: 0.8;
    }
    :active {
      opacity: 0.6;
    }
  }

  .right-float {
    margin-left: auto;
  }

  .navbar-container {
    display: flex;
    justify-content: space-between;
    /* height: 80px; */
  }
  .footer {
    display: none;
  }
  .footer.active {
    display: block;
    position: absolute;
    bottom: 0;
    right: 0;
    left: 0;
    background: #58a08f;
    /* h5,
    p {
      font-size: 14px;
    } */
  }
  .navbar-logo {
    color: #fff;
    justify-self: start;
    cursor: pointer;
    text-decoration: none;
    display: flex;
    align-items: center;
    position: relative;
    left: 8px;
    /* width: 250px; */
    /* > img {
      width: 100%;
      position: absolute;
      right: 0;
      left: 0;
      top: 22px;
      height: auto;
    } */
    /* img {
      width: 100%;
      height: auto;
    } */
    svg {
      width: 200px;
      height: 68px;
      :nth-child(2) {
        display: none;
      }
    }
    /* .mobileLogoIcon {
      display: none;
    } */
    .transparentDesktopLogo {
      margin: 0 auto;
    }
  }

  .navbar-icon {
    margin-right: 0.5rem;
  }

  .nav-menu {
    list-style: none;
    position: absolute;
    left: 0;
    right: 0;
    height: 100%;
    top: 0;
    display: flex;
  }

  .nav-item {
    padding: 0 20px;
    display: flex;
    align-items: center;

    /* height: 80px;
    border-bottom: 2px solid transparent; */
  }

  .nav-item:hover {
    /* border-bottom: 2px solid #f00946; */
  }

  .nav-links {
    color: #000;
    display: flex;
    align-items: center;
    text-decoration: none;
    padding: 0.5rem 1rem;
    height: auto;
    font-size: 16px;
    text-transform: uppercase;
    font-family: 'roboto_condensedregular';
    justify-content: center;
    cursor: pointer;

    :hover {
      opacity: 0.8;
    }
  }

  .nav-links.active {
    color: #94a753;
    font-family: 'robotobold';
    display: flex;
    align-items: center;
    text-decoration: none;
    padding: 0.5rem 1rem;
    height: auto;
    font-size: 16px;
    text-transform: uppercase;
    justify-content: center;
    cursor: pointer;
  }

  .menu-icon {
    display: none;
  }
  /* .mobileLogoIcon {
    display: none;
  }
  .transparentDesktopLogo {
    display: block;
  } */
  @media screen and (max-width: 960px) {
    /* .transparentDesktopLogo {
      display: none;
    }
    .mobileLogoIcon {
      display: block;
    } */
    height: 50px;
    /* margin-top: -50px; */
    .NavbarItems {
      position: relative;
    }
    .blankLi {
      display: none;
    }
    .nav-menu {
      display: flex;
      flex-direction: column;
      width: 100%;
      height: fit-content;
      position: absolute;
      /* top: 50px; */
      top: -241px;
      opacity: 1;
      transition: all 0.5s ease;
      padding: 0;
      align-items: center;
    }
    .nav-menu.active {
      background: ${(props) => (props?.variant ? '#d3d3d3f2' : '#ffffffd9')};
      top: 0;
      height: auto;
      opacity: 1;
      transition: all 0.6s ease;
      z-index: 1;
      justify-content: flex-start;
      padding: 15px 15px 25px 15px;
      align-items: center;
      padding-top: 60px;
      .readMoreAccordion {
        display: block;
      }
      .readMoreBtn {
        display: none;
      }
    }
    .nav-links {
      padding: 10px;
      width: 100%;
      display: table;
      text-align: center;
    }

    /* .nav-links:hover {
      transform: scale(1.1);
      transition: all 0.3s ease;
    } */
    .nav-item {
      width: fit-content;
      height: 60px;
    }
    .nav-item:hover {
      border: none;
    }

    .right-float {
      margin: 0;
    }

    .navbar-logo {
      top: 0;
      left: 0;
      right: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      max-width: 150px;
      position: relative;
      z-index: 2;
      svg {
        height: 48px;
      }
      /* img {
        display: none;
      } */
      /* .transparentDesktopLogo {
        display: none;
      } */
      /* .mobileLogoIcon {
        display: block;
      } */
    }

    .menu-icon {
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      transform: translate(49%, 36%);
      cursor: pointer;
      z-index: 2;
    }
  }
`;

const Logo = styled.div`
  text-align: center;
  img {
    width: 86px;
  }
`;
const Wrapper = styled.div`
  position: absolute;
  left: 17px;
  display: none;
  @media screen and (max-width: 960px) {
    display: block;
    z-index: 2;
    top: 12px;
  }
`;
export { HeadContainer, Logo, Wrapper };
