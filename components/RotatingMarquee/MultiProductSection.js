import React from 'react';
import { MultiProductContainers } from 'css/components/RotatingMarquee';

const MultiProductSection = ({ indexKey, ...props }) => {
  const { options, icon, heading, description } = props;

  return (
    <MultiProductContainers key={indexKey} {...options} {...props}>
      <div>
        {icon}
        <h4>{heading.toUpperCase()}</h4>
        <p>{description}</p>
      </div>
    </MultiProductContainers>
  );
};

export default MultiProductSection;
