import React from 'react';
import { TextField } from '@material-ui/core';
import useStyles from 'components/FormField/style';
import { getTheme } from 'components/FormField/theme';

const FormField = ({ theme, ...props }) => {
  const classes = useStyles(getTheme(theme));
  return <TextField className={classes.root} variant='outlined' {...props} />;
};

export default FormField;
