import React, { useState, useRef, useEffect } from 'react';
import Hls from 'hls.js';
import styled from 'styled-components';
import { SplitText } from 'components/SplitText';
import Loader from 'components/Loader';
import MuteIcon from 'assets/SVG/MuteIcon';
import { useSelector, useDispatch } from 'react-redux';
import PlayIcon from 'assets/Images/play-icon.png';
import MultiProductSection from 'components/RotatingMarquee/MultiProductSection';
import {
  SectionContainer,
  HeroBg,
  HeroWrapper,
  BannerText1,
  BannerText2,
  BannerText3,
  BannerTextWrapper,
  MuteText,
  Skip,
  BannerWrapper,
  MultiProductMarquee,
  PlayAgain,
  WebView,
  MobileView,
  IconSection,
  VideoPlayButton,
} from 'css/components/RotatingMarquee';
import { SET_VIDEO_PLAY } from 'redux/actions/video';
import { SET_CATEGORY } from 'redux/actions/marquee';
import { useMotionValue, useAnimation, motion } from 'framer';
import { enableScroll, disableScroll, isAndroid } from 'utils';
import profiles from 'components/RotatingMarquee/profiles';
import { SET_SECTION } from 'redux/actions/section';

const variants = {
  visible: (i) => ({
    y: 0,
    transition: {
      delay: i * 0.2,
    },
  }),
  invisible: (i) => ({
    y: 100,
    transition: {
      delay: i * 0.1,
    },
  }),
};

const hideVariants = {
  visible: { opacity: 1 },
  invisible: { opacity: 0 },
};

const videoURL = `${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_API_PORT}/api/video`;
const mobileVideoURL = `${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_API_PORT}/api/video/index.m3u8`;

let videoRun = false;
let videoAnimating = false;
let firstLoad = false;

const Marquee = React.memo(({ scrollbar, videoRotation, ...props }) => {
  const [mute, setMute] = useState(true);
  const [section, toggleSection] = useState(true);
  const video = useRef(null);
  const mobileVideo = useRef(null);
  const toggle = useMotionValue(0);
  const playPause = useMotionValue(0);
  const textAnimation = useAnimation();
  const [videoReady, setVideoReady] = useState(false);
  const [showVideo, setShowVideo] = useState(false);
  const [hideLoader, setHideLoader] = useState(false);
  const [mobileLoader, setMobileLoader] = useState(true);
  const { video: videoPlay, products } = useSelector((state) => state);

  const dispatch = useDispatch();
  const { y } = props;

  const options = {
    textOptions: {
      initial: { y: '100%' },
      animate: mute ? 'visible' : 'invisible',
      variants: variants,
    },
    bannerOptions: {
      initial: { opacity: 1 },
      animate: { opacity: 1 },
      exit: { opacity: 0 },
    },
  };

  const mobileEnd = () => {
    let angle = window?.screen?.orientation?.angle || window?.orientation;
    videoRotation?.setVideo(false);
    if (angle !== 0) {
      videoRotation?.setRotateScreen(true);
    }
    if (mobileVideo?.current?.webkitExitFullscreen)
      mobileVideo?.current?.webkitExitFullscreen();
    if (mobileVideo?.current?.mozExitFullscreen)
      mobileVideo?.current?.mozExitFullscreen();
    if (mobileVideo?.current?.exitFullscreen)
      mobileVideo?.current?.exitFullscreen();
    if (mobileVideo?.current) {
      mobileVideo.current.currentTime = 1;
      mobileVideo?.current?.pause();
    }
    setShowVideo(false);
  };

  useEffect(() => {
    dispatch({ type: SET_VIDEO_PLAY, payload: !mute });
  }, [dispatch, mute]);

  useEffect(() => {
    dispatch({ type: SET_SECTION, payload: section });
  }, [dispatch, section]);

  const fullscreenChange = (e) => {
    if (
      document.fullScreenElement ||
      document.webkitIsFullScreen ||
      document.mozFullScreen ||
      document.msFullscreenElement
    ) {
      if (!mobileLoader) setMobileLoader(true);
    } else {
      mobileEnd();
      enableScroll();
    }
  };

  const pictureInPicture = () => {
    if (!mobileVideo?.current?.webkitDisplayingFullscreen) {
      mobileEnd();
      enableScroll();
    }
  };

  useEffect(() => {
    if (mobileVideo && mobileVideo?.current) {
      mobileVideo.current.addEventListener(
        'leavepictureinpicture',
        pictureInPicture
      );

      mobileVideo.current.addEventListener(
        'fullscreenchange',
        fullscreenChange
      );
      mobileVideo.current.addEventListener(
        'webkitendfullscreen',
        fullscreenChange
      );
      mobileVideo.current.addEventListener(
        'webkitfullscreenchange',
        fullscreenChange
      );
      mobileVideo.current.addEventListener(
        'mozfullscreenchange',
        fullscreenChange
      );
    }
    return () => {
      if (mobileVideo?.current) {
        mobileVideo.current.removeEventListener(
          'leavepictureinpicture',
          pictureInPicture
        );
        mobileVideo.current.removeEventListener(
          'webkitendfullscreen',
          fullscreenChange
        );
        mobileVideo.current.removeEventListener(
          'fullscreenchange',
          fullscreenChange
        );
        mobileVideo.current.removeEventListener(
          'webkitfullscreenchange',
          fullscreenChange
        );
        mobileVideo.current.removeEventListener(
          'mozfullscreenchange',
          fullscreenChange
        );
      }
    };
  });

  useEffect(() => {
    if (video?.current) {
      video.current.currentTime = 0;
    }
  }, [video]);

  useEffect(() => {
    if (window.innerWidth >= 968) {
      if (!section && video?.current) {
        video.current.currentTime = 0;
        video?.current?.pause();
        toggle.set(1);
      }
      if (section) {
        toggle.set(0);
        video?.current?.play();
      }
    }
  }, [section]);

  useEffect(() => {
    let unsubscribe;
    let clear;
    if (window.innerWidth >= 968) {
      unsubscribe = y.onChange(() => {
        if (
          y.get() <= -window.innerHeight &&
          !video?.current?.paused &&
          toggle.get() === 0 &&
          playPause.get() === 0
        ) {
          if (video?.current) video?.current?.pause();
          textAnimation.stop();
          textAnimation.start('visible');
          playPause.set(1);
        } else if (
          y.get() > -window.innerHeight &&
          video?.current?.paused &&
          toggle.get() === 0 &&
          playPause.get() === 1
        ) {
          textAnimation.stop();
          if (video?.current) video?.current?.play();
          textAnimation.start('visible');
          playPause.set(0);
          if (videoAnimating)
            clear = setTimeout(() => {
              textAnimation.start('invisible');
              videoAnimating = false;
            }, [5000]);
          videoAnimating = true;
        }
      });
    }
    return () => {
      if (unsubscribe) unsubscribe();
      if (clear) clearTimeout(clear);
    };
  }, [y, window.innerWidth]);

  const selectCategory = (category) => {
    dispatch({ type: SET_CATEGORY, payload: category });
    window.scrollTo({ top: props?.scienceScroll, behavior: 'smooth' });
  };

  useEffect(() => {
    setTimeout(() => {
      if (!videoReady && !hideLoader && videoAnimating) {
        setHideLoader(true);
        videoAnimating = false;
      }
    }, [!firstLoad ? 20000 : 5000]);
    videoAnimating = true;
  }, [hideLoader]);

  useEffect(() => {
    const screenOrientation = window?.screen?.orientation;
    if (mobileVideo?.current) {
      if (showVideo) {
        videoRotation?.setVideo(true);
        setMobileLoader(false);
        if (isAndroid) {
          if (
            typeof mobileVideo?.current?.webkitEnterFullscreen != 'undefined'
          ) {
            mobileVideo?.current?.webkitEnterFullscreen();
          } else if (
            typeof mobileVideo?.current?.webkitRequestFullscreen != 'undefined'
          ) {
            mobileVideo?.current?.webkitRequestFullscreen();
          } else if (
            typeof mobileVideo?.current?.mozRequestFullScreen != 'undefined'
          ) {
            mobileVideo?.current?.mozRequestFullScreen();
          }
          if (screenOrientation && screenOrientation.lock) {
            screenOrientation?.lock('landscape');
          }
        }

        videoRun = true;
      } else if (!showVideo && videoRun) {
        setTimeout(() => {
          window.scrollTo({
            top: props?.scrollHeight,
            behavior: 'smooth',
          });
        }, [1000]);
      }
    }
  }, [showVideo]);

  useEffect(() => {
    if (mobileVideo?.current) {
      enableScroll();
      if (mobileVideo?.current?.canPlayType('application/vnd.apple.mpegurl')) {
        mobileVideo.current.src = mobileVideoURL;
      } else if (Hls.isSupported()) {
        let hls = new Hls();
        hls.loadSource(mobileVideoURL);
        hls.attachMedia(mobileVideo.current);
        hls.on(Hls.Events.MANIFEST_PARSED, function () {
          setMobileLoader(true);
        });
      }
    }
  }, [mobileVideo?.current]);

  return (
    <SectionContainer>
      <BannerWrapper toggle={section}>
        <HeroWrapper>
          {window.innerWidth >= 968 && (
            <HeroBg
              id='videoDiv'
              src={videoURL}
              type='video/mp4'
              preload='auto'
              autoPlay='autoplay'
              ref={video}
              muted={mute}
              controls={!mute}
              disablePictureInPicture
              controlsList='nodownload noremoteplayback'
              onCanPlay={() => {
                enableScroll();
                firstLoad = true;
                setVideoReady(true);
                setTimeout(() => {
                  textAnimation.start('invisible');
                }, [5000]);
              }}
              onEnded={() => {
                if (video?.current?.webkitExitFullscreen)
                  video?.current?.webkitExitFullscreen();
                if (video?.current?.mozExitFullscreen)
                  video?.current?.mozExitFullscreen();
                if (video?.current?.exitFullscreen)
                  video?.current?.exitFullscreen();
                toggleSection(!section);
                video.current.currentTime = 1;
                video?.current?.pause();
              }}
            />
          )}
        </HeroWrapper>

        {!videoReady && !hideLoader && (
          <LoaderWrapper>
            <Loader />
          </LoaderWrapper>
        )}
        {hideLoader && !videoReady && !firstLoad && (
          <BannerTextWrapper>
            <div className='error'>
              <div>Video couldn't be loaded. Please reload the page.</div>{' '}
              <div
                className='retry-link'
                onClick={() => {
                  setHideLoader(false);
                  video?.current?.load();
                }}>
                Retry loading video
              </div>
            </div>
          </BannerTextWrapper>
        )}
        {videoReady && !videoPlay && (
          <BannerTextWrapper>
            <motion.div
              initial='visible'
              animate={textAnimation}
              variants={hideVariants}>
              <BannerText1 key='1' {...options?.bannerOptions}>
                <SplitText {...options?.textOptions}>
                  FRESH NEVER FROZEN
                </SplitText>
                <SplitText className='smallText' {...options?.textOptions}>
                  TM
                </SplitText>
              </BannerText1>
              <BannerText2
                key='2'
                {...options?.bannerOptions}
                transition={{ duration: 5 }}>
                <SplitText {...options?.textOptions}>CANNABIS</SplitText>
                <SplitText {...options?.textOptions} className='smallText1'>
                  +
                </SplitText>
                <SplitText {...options?.textOptions}>ESSENTIAL OILS</SplitText>
              </BannerText2>
              <BannerText3 key='3' {...options?.bannerOptions}>
                <SplitText {...options?.textOptions}>
                  SHIPPING GLOBALLY
                </SplitText>
              </BannerText3>
            </motion.div>

            {mute ? (
              <div
                style={{ cursor: 'pointer', marginTop: '25px' }}
                onClick={() => {
                  setMute(!mute);
                }}>
                <MuteIcon style={{ margin: '0 auto', width: '100%' }} />
                <MuteText style={{ textAlign: 'center' }}>UNMUTE</MuteText>
              </div>
            ) : null}
          </BannerTextWrapper>
        )}

        {!videoPlay && videoReady && (
          <Skip
            onClick={() => {
              window.scrollTo({ top: props?.scrollHeight, behavior: 'smooth' });
            }}>
            <h4>Skip</h4>
          </Skip>
        )}
      </BannerWrapper>

      <MultiProductMarquee toggle={section}>
        <WebView>
          {profiles?.map((profile, index) => (
            <MultiProductSection
              icon={<profile.Icon color='#ffffff' fill='#ffffff' />}
              heading={profile?.name}
              indexKey={`multiproduct-${index}`}
              onClick={() => selectCategory(profile?.category)}
              options={{
                background: profile?.gradient,
              }}
            />
          ))}
          <PlayAgain>
            <BannerTextWrapper>
              <div>
                <BannerText1>
                  FRESH NEVER FROZEN
                  <span className='smallText'>TM</span>
                </BannerText1>
                <BannerText2>
                  CANNABIS
                  <span className='smallText1'>+</span>
                  ESSENTIAL OILS
                </BannerText2>
                <BannerText3>SHIPPING GLOBALLY</BannerText3>
              </div>
              <h4
                id='watchVideos'
                onClick={() => {
                  toggleSection(!section);
                  setMute(false);
                }}>
                Watch the
                <span>
                  <img src={PlayIcon} alt='playIcon' />
                </span>
                video again
              </h4>
            </BannerTextWrapper>
          </PlayAgain>
        </WebView>
        <MobileView>
          <div>
            <BannerTextWrapper>
              <BannerText1 className='BannerText1Mobile'>
                FRESH NEVER FROZEN
                <span className='smallText'>TM</span>
              </BannerText1>
              <BannerText2 className='BannerText2Mobile'>
                CANNABIS<span className='smallText1'>+</span> ESSENTIAL OILS
              </BannerText2>
            </BannerTextWrapper>
            <BannerTextWrapper>
              <BannerText3
                key='3'
                initial={{ opacity: 1 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}>
                <SplitText
                  initial={{ y: '100%' }}
                  animate='visible'
                  variants={{
                    visible: (i) => ({
                      y: 0,
                      transition: {
                        delay: i * 0.1,
                      },
                    }),
                  }}>
                  NOW SHIPPING GLOBALLY
                </SplitText>
              </BannerText3>
            </BannerTextWrapper>
          </div>
          <VideoPlayButton>
            <h4
              id='watchVideos'
              onClick={() => {
                mobileVideo?.current?.play();
                disableScroll();
                setShowVideo(true);
              }}>
              Watch the
              <span>
                <img src={PlayIcon} alt='playIcon' />
              </span>
              intro video
            </h4>
          </VideoPlayButton>
          {!videoReady && !mobileLoader && (
            <LoaderWrapper>
              <Loader />
            </LoaderWrapper>
          )}
          {window.innerWidth <= 968 && (
            <div
              style={{
                position: 'fixed',
                top: 0,
                height: '100vh',
                width: '100vw',
                zIndex: 10,
                display: showVideo ? 'block' : 'none',
              }}>
              <HeroBg
                controls
                id='mobile-video'
                ref={mobileVideo}
                controlsList='nodownload noremoteplayback'
                loop={false}
                preload='none'
                onCanPlay={() => {
                  if (!mobileLoader) setMobileLoader(true);
                }}
                onEnded={mobileEnd}
              />
            </div>
          )}

          <IconSection>
            {profiles?.map((profile, index) => (
              <div onClick={() => selectCategory(profile?.category)}>
                <profile.Icon
                  color={profile?.color}
                  key={`multiproduct-mobile-${index}`}
                />
                <p style={{ color: profile?.color }}>{profile?.name}</p>
              </div>
            ))}
          </IconSection>
        </MobileView>
      </MultiProductMarquee>
    </SectionContainer>
  );
});

export default Marquee;

const LoaderWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;
