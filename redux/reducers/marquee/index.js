import * as a from 'redux/actions/marquee';

const initial = '';

const marquee = (state = initial, action = null) => {
  switch (action.type) {
    case a.INITIALIZE_CATEGORY:
      return initial;
    case a.SET_CATEGORY:
      return action.payload;
    default:
      return state;
  }
};

export default marquee;
