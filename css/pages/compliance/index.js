import styled from 'styled-components';
import { motion } from 'framer';

const Container = styled(motion.div)`
  width: 100%;
`;

const BodyContainer = styled.div`
  padding: 60px 40px;
  min-height: calc(100vh - 80px);

  @media (max-width: 964px) {
    padding: 10px 20px;
  }
`;

const BodyWrapper = styled.div`
  max-width: 1080px;
  padding: 30px;
  margin: 60px auto 0;
  text-align: center;

  @media (max-width: 964px) {
    padding: 30px 0;
  }

  > h1 {
    text-align: center;
    text-transform: uppercase;
    margin-bottom: 20px;
    font-size: 24px;
  }

  > p {
    font-family: 'robotoregular';
    text-align: center;
    line-height: 1.75rem;
    font-size: 16px;
    letter-spacing: 0.05rem;

    > span {
      color: #000000;
      font-family: 'robotobold';
    }

    > a {
      position: relative;
      color: #4a4a4a;
      border-bottom: 2px solid rgb(75, 75, 75, 0.2);
      text-decoration: none;
      ::after {
        content: '';
        position: absolute;
        bottom: -2px;
        left: 0px;
        width: 0%;
        border-bottom: 2px solid #4a4a4a;
        transition: width 0.5s ease;
      }
      :hover::after {
        width: 100%;
      }
    }
  }
  > span {
    font-family: 'robotobold';
    text-align: center;
    letter-spacing: 0.05rem;
  }

  > a:visited {
    cursor: pointer;
    color: #000000;
  }
`;

const GridWrapper = styled.div`
  margin: 25px 0;
  display: grid;
  justify-content: center;
  grid-template-columns: repeat(auto-fit, minmax(223px, 400px));

  > h3 {
    text-align: center;
    padding: 30px 0;
  }
`;

const GridContainer = styled.div``;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px 0;
  > form {
    width: 100%;
    max-width: 623px;
    text-align: center;
    .MuiInputLabel-outlined {
      transform: none;
      padding: 22px 30px;
      text-align: center;
      color: inherit;
      margin: 0 auto;
      width: 100%;
    }
    .MuiSvgIcon-root {
      width: 20px;
      opacity: 0.5;
    }
    .MuiOutlinedInput-root input {
      text-align: center;
      font-family: 'robotobold';
      text-transform: uppercase;
      padding: 10px 22px;
    }
    .MuiFormControl-root {
      height: 60px;
      margin-bottom: 15px;
    }
    .MuiInputAdornment-root {
      position: absolute;
      right: 8px;
    }
  }

  .submitBtn {
    -webkit-appearance: none;
    -moz-appearance: none;
    font-size: clamp(16px, 2vh, 20px);
    background: #58a08f;
    border-radius: 7.5px;
    width: 190px;
    padding: 19px 45px;
    color: #fff;
    text-transform: uppercase;
    border: none;
    font-family: 'hansonbold';
    cursor: pointer;

    :disabled {
      opacity: 0.5;
      :hover {
        cursor: not-allowed;
        opacity: 0.5;
      }
    }

    :active {
      opacity: 0.8;
    }
    :hover {
      opacity: 0.8;
    }
    .rc-anchor-normal .rc-anchor-content {
      width: 154px;
    }
  }
`;

const LinkContainer = styled.div`
  width: 100%;
  > div {
    text-align: center;
    margin-bottom: 20px;

    > a {
      font-size: 1.2rem;
      text-decoration: none;
      text-transform: uppercase;

      :hover {
        opacity: 0.8;
      }
      :active {
        opacity: 0.6;
      }
    }
  }
`;

const Grid = styled.div`
  padding: 20px;
  text-align: center;
  max-width: 100%;
  text-transform: uppercase;
  background-color: ${(props) => (props.bgColor ? props?.bgColor : '#000')};
  color: #fff;
  min-height: 700px;

  > svg {
    height: 130px;
    width: 100px;
    margin: 20px 0;
  }

  > h1 {
    margin-bottom: 70px;
    font-size: 1.6vw;
  }

  a {
    font-size: 1.2vw;
    margin-top: 2.5em;
    margin-bottom: 1em;
    text-decoration: none;
    color: white;
  }

  > h3 {
    /* font-size: 26px; */
    position: relative;
    margin-top: 2.5em;
    margin-bottom: 1em;

    > a {
      border-bottom: 2px solid rgba(72, 72, 72, 0.1);
      cursor: pointer;
      position: relative;
      text-decoration: none;
      color: white;

      ::after {
        content: '';
        position: absolute;
        bottom: -2px;
        left: 0px;
        width: 0%;
        border-bottom: 2px solid #ffffff;
        transition: width 0.5s ease;
      }
      :hover::after {
        width: 100%;
      }
    }
  }
`;
const NoProfile = styled.div`
  text-align: center;
  font-family: 'robotobold';
  text-transform: uppercase;
  h3 {
    font-size: 28px;
    padding: 1px 0;
  }
  span {
    font-size: 17.5px;
    color: #58a08f;
    margin: 24px 0;
    display: block;
    cursor: pointer;
    text-decoration: underline;

    :hover {
      opacity: 0.8;
    }
  }
  @media (max-width: 767px) {
    h3 {
      font-size: 16px;
    }
    span {
      font-size: 14px;
    }
  }
`;
export {
  Container,
  BodyWrapper,
  BodyContainer,
  LinkContainer,
  Grid,
  GridWrapper,
  GridContainer,
  SearchWrapper,
  NoProfile,
};
