import * as a from 'redux/actions/products';

const initial = [];

const products = (state = initial, action = null) => {
  switch (action.type) {
    case a.INITIALIZE_PRODUCTS:
      return initial;
    case a.SET_PRODUCTS:
      return action.payload;
    default:
      return state;
  }
};

export default products;
