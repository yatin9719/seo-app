import React from 'react';
import { EvolutionBoxContainers } from 'css/components/Evolution';

const EvolutionBox = (props) => {
  const {
    options,
    icon,
    heading,
    className,
    initial,
    animate,
    exit,
    transition,
    detail,
    keyVal,
    style,
    id,
    onClick,
    open,
    onHover,
  } = props;

  return (
    <EvolutionBoxContainers
      id={id}
      key={keyVal}
      initial={initial}
      animate={animate}
      exit={exit}
      style={style}
      transition={transition}
      className={className}
      onMouseEnter={onHover}
      {...options}>
      {icon}
      <div className='flip-card' onClick={onClick}>
        <div
          className='flip-card-inner'
          style={open ? { transform: 'rotateY(180deg)' } : {}}>
          <div className='flip-card-front'>
            <h4 style={{ maxWidth: '200px' }}>{heading}</h4>
            {props.children}
          </div>
          <div className='flip-card-back'>
            <p>{detail}</p>
          </div>
        </div>
      </div>
    </EvolutionBoxContainers>
  );
};

export default EvolutionBox;
