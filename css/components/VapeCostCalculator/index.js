import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';
const Container = styled.div`
  position: relative;
  z-index: 999;
  background: #f4f4f4;
  min-height: 100vh;
  /* background: url(${LegalBg}),
    linear-gradient(180deg, #58a08f33 0%, #ffffff33 100%), #c4c4c433; */
  background: white;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: top;
  /* overflow-x: clip; */
  > p {
    text-align: center;
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    text-decoration: underline;
  }
`;
const Heading = styled.div`
  text-align: center;
  padding: 50px 0;
  /* padding-top: 100px; */
  /* padding-top: 77px; */
  /* position: relative; */
  h3 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    max-width: 94%;
    margin: 0 auto;
    .smallText {
      font-size: 17.5px;
    }
  }
  p {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    width: 56%;
    margin: 0 auto;
    margin-top: 10px;
  }
  @media (max-width: 767px) {
    /* padding-top: 65px; */
    h3 {
      line-height: 26px;
      font-size: 22.5px;
      max-width: 90%;
      margin: 0 auto;
    }
    p {
      font-size: 17.5px;
      width: 51%;
    }
  }
  @media (max-width: 320px) {
    padding-top: 61px;
  }
`;
const IconSection = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  text-align: center;
  text-transform: uppercase;
  max-width: 992px;
  margin: 64px auto;
  flex-wrap: wrap;
  > div {
    height: 190px;
    width: 248px;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex-direction: column;
    position: relative;
    > p {
      color: #58a08f;
      font-size: 65px;
      text-transform: capitalize;
    }
  }
`;

const MainHeading = styled.div`
  h1 {
    font-size: 62.5px;
    max-width: 900px;
    margin: 0 auto;
    line-height: 61px;
    text-align: center;
  }
  p {
    font-size: 17.5px;
    text-align: center;
    font-family: 'robotoregular';
  }
`;

export { Container, Heading, IconSection, MainHeading };
