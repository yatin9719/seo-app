const GREEN = '#58a08f';

const defaultTheme = {
  borderColor: GREEN,
  labelColorFocus: GREEN,
  width: '100%',
  padding: '10px 10px',
  backgroundColor: '#efefef',
  labelColor: '#000',
  labelFontSize: '14px',
  labelFontSizeFocus: '15px',
  labelTransform: 'translate(14px, 13px) scale(1)',
  labelTransformFocus: 'translate(0px, -15px) scale(0.75)',
};

const getTheme = (theme) => {
  switch (theme) {
    case 'compact':
      return {
        ...defaultTheme,
        fontFamily: 'robotoregular',
        width: null,
      };
    default:
      return defaultTheme;
  }
};

export { getTheme };
