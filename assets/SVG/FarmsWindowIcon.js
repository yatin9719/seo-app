import * as React from 'react';
import { motion } from 'framer';

const Icon = ({ className }) => (
  <motion.svg
    width={'clamp(19.25px, 6vw, 27.5px)'}
    height={'clamp(22.75px, 6vw, 32.5px)'}
    viewBox='0 0 11 13'
    fill='none'
    className={className}>
    <path
      d='M8.8 11.56H4.3v-.3h1.4v-.9H4.3v-.2h4.5v1.4zM7.4 4.96H4.3v-2.3l3.1 1.9v.4zM7.4 9.26H4.3v-.9h2.5v-.8H4.3v-1.7h3.1v3.4zM3.4 8.36H.9v3.1h2.5v-3.1zM3.4 5.86H.9v1.7h2.5v-1.7z'
      fill='#ADC98D'
    />
    <path
      d='M.1 1.56L1.9.46c.1-.1.3-.1.4 0L8 3.86c.1.1.2.2.2.4v5h1c.2 0 .4.2.4.4v1.8h.9v.9H.1v-.1'
      fill='#ADC98D'
    />
    <path d='M3.4 2.16v2.8H.9v-2.8l1.2-.8 1.3.8z' fill='#ADC98D' />
    <path
      d='M.1 12.26v.1h10.4v-.9h-.9v-1.8c0-.2-.2-.4-.4-.4h-1v-5c0-.2-.1-.3-.2-.4L2.3.46c-.1-.1-.3-.1-.4 0L.1 1.56v10.7zm4.2-2.1h4.5v1.4H4.3v-.3h1.4v-.9H4.3v-.2zm0-4.3h3.1v3.4H4.3v-.8h2.6v-.9H4.3v-1.7zm0-3.2l3.1 1.9v.5H4.3v-2.4zM.9 8.36h2.5v3.1H.9v-3.1zm0-2.5h2.5v1.7H.9v-1.7zm0-3.7l1.3-.8 1.3.8v2.8H1v-2.8H.9z'
      fill='#010101'
    />
  </motion.svg>
);

export default Icon;
