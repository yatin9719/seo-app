import styled from 'styled-components';
import spiritBackground from 'assets/Images/spirit-background.png';
import spiritBackgroundOne from 'assets/Images/spirit-background2.png';
import spiritBackgroundTwo from 'assets/Images/spirit-background3.png';
import { motion } from 'framer';

const Container = styled(motion.div)`
  background-image: url(${spiritBackground}), url(${spiritBackgroundOne}),
    url(${spiritBackgroundTwo});

  background-size: cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-color: transparent;
  position: relative;
  @media (max-width: 767px) {
    background-size: auto;
    background-image: url(${spiritBackground}), url(${spiritBackground}),
      url(${spiritBackgroundTwo});
  }
`;

const Heading = styled.div`
  text-align: center;
  .headingWrapper {
    margin-top: 12px;
    display: flex;
    justify-content: center;
  }
  span.plusIcon {
    line-height: 10px;
    font-size: 14px;
    position: relative;
    top: -17px;
  }
  .greenText {
    color: #58a08f;
  }
  h4 {
    font-size: 40px;
    text-transform: uppercase;
    .greenText {
      color: #58a08f;
    }
  }
  .leadingProducer {
    text-transform: uppercase;
    font-family: 'roboto_condensedbold';
    font-size: 14px;
    letter-spacing: 3px;
  }
  .greenText {
    color: #58a08f;
  }
  .scarcity-process {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    width: 70%;
    margin: 0 auto;
    max-width: 581px;

    .smallText {
      font-size: 12px;
      position: relative;
      top: -15px;
    }
  }
  h3 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    width: 70%;
    margin: 0 auto;
    max-width: 581px;

    .smallText {
      font-size: 12px;
      position: relative;
      top: -15px;
    }
  }
  small {
    font-size: 17.5px;
    text-transform: uppercase;
  }
  .aristotle-taught {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    margin: 10px auto;
    text-transform: initial;
    font-weight: unset;
    display: block;
    span {
      font-weight: bold;
    }
  }
  .smallHeading {
    font-size: 15px;
    letter-spacing: 3px;
    font-family: 'roboto_condensedbold';
  }
  .mobileView {
    display: none;
  }
  .webView {
    display: block;
  }
  .ofSpanWeb {
    display: inline-block;
  }
  .ofSpanMobile {
    display: none;
  }
  .brandLogo {
    display: none;
    margin: 3vh auto;
  }
  @media (max-height: 667px) {
    .brandLogo {
      margin: 2vh auto;
    }
  }
  @media (max-width: 767px) {
    .headingWrapper {
      flex-direction: column;
      margin-top: 0px;
    }
    .mobileView {
      display: block;
    }
    .brandLogo {
      display: block;
    }
    .ofSpanWeb {
      display: none;
    }
    .ofSpanMobile {
      display: block;
      font-size: 9.5px;
      margin: 4px auto 10px auto;
      font-family: 'roboto_condensedbold';
    }
    .webView {
      display: none;
    }
    .smallHeading {
      font-size: 13px;
      margin-bottom: 0;
    }
    .scarcity-process {
      width: 259px;
      font-size: 16pt;
      line-height: 16pt;
      margin-top: 5px;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 13px;
      position: relative;
      top: -13px;
    }
    h4 {
      font-size: 22pt;
      line-height: 22pt;
      width: 325px;
      margin: 0 auto;
      font-family: 'roboto_condensedbold';
    }
    .aristotle-taught {
      font-size: 12pt;
      width: 306px;
    }
  }

  @media (max-width: 320px) {
    padding-top: 61px;
    h3 {
      width: 100%;
      font-size: 11pt;
      line-height: 14pt;
    }

    .aristotle-taught {
      margin-top: 0;
      font-size: 10pt;
      width: 286px;
    }
  }
`;
const HexagonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 6vh;
  margin-bottom: 15vh;
  position: relative;
  > div {
    display: flex;
    :last-child {
      position: absolute;
      top: 76%;
    }
    > div {
      padding: 4px;
      width: clamp(150px, 24vh, 205px);
      height: auto;
      img {
        width: 100%;
      }
    }
  }

  @media (max-height: 667px) and (max-width: 768px) {
    margin-top: 3vh;
  }

  @media only screen and (max-width: 768px) {
    > div {
      > div {
        width: clamp(150px, 24vh, 205px);
      }
    }
  }
  @media only screen and (max-width: 600px) {
    > div {
      > div {
        width: clamp(150px, 22vw, 205px);
      }
    }
  }
  @media only screen and (max-width: 375px) and (max-height: 667px) {
    > div {
      > div {
        width: clamp(131px, 22vw, 205px);
      }
    }
  }
  @media only screen and (max-width: 320px) {
    > div {
      > div {
        width: 112px;
      }
    }
  }
`;
const Box = styled.div`
  position: relative;
  .flip-card {
    background-color: transparent;
    height: 100%;
    perspective: 1000px;
  }

  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
  }

  .flip-card-front,
  .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }

  .flip-card-front {
    z-index: 1;
    > h5 {
      position: absolute;
      left: 0;
      top: 0;
      bottom: 0;
      right: 0;
      height: 100%;
      font-size: clamp(17px, 3.5vh, 28px);
      text-transform: uppercase;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }

  .flip-card-back {
    color: white;
    transform: rotateY(180deg);
    height: 219px;

    > svg {
      position: absolute;
      /* right: -34px; */
      left: 34px;
      top: 44px;
    }
    > img {
      width: 77%;
      height: auto;
      position: relative;
      top: 38px;
    }
    @media (min-height: 640px) and (max-height: 1024px) {
      /* > img {
        top: 28px;
      } */
    }

    .greenImageBack {
      width: 100%;
      position: absolute;
      top: 0;
      right: 0;
    }
    @media (max-width: 550px) {
      > img {
        top: 28px;
      }
    }
    @media (max-width: 320px) {
      > img {
        top: 20px;
      }
    }
  }
  .white-text {
    color: #fff;
  }
  > h5 {
    position: absolute;
    color: #58a08f;
    z-index: 1;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: clamp(15px, 2.5vh, 26px);
    text-transform: uppercase;
  }
  .greenImage {
    animation: greenHexagon 0.8s alternate infinite;
  }
  @keyframes greenHexagon {
    0% {
      filter: brightness(1.2);
    }
    100% {
      filter: brightness(1);
    }
  }
`;
const Star = styled(motion.div)`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  img {
    animation: twinkleOne 0.75s alternate infinite;
    animation-delay: 0.88462s;
    :nth-child(even) {
      animation-delay: 1.88462s;
    }
    :nth-child(odd) {
      animation-delay: 2s;
    }
    :first-child {
      position: absolute;
      left: 0;
      opacity: 0;
      top: 51%;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 1.6;
    }
    :nth-child(2) {
      position: absolute;
      left: 0;
      opacity: 0;
      bottom: 48px;
      width: 20px;
      animation-delay: 0.5;
    }
    :nth-child(3) {
      position: absolute;
      left: 31px;
      opacity: 0;
      bottom: 40px;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 0.8;
    }
    :nth-child(4) {
      position: absolute;
      left: 37px;
      opacity: 0;
      bottom: -12px;
      width: 10px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 1;
    }
    :nth-child(5) {
      position: absolute;
      left: 45px;
      opacity: 0;
      bottom: 12px;
      width: 14px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 1.2;
    }
    :nth-child(6) {
      position: absolute;
      left: 71px;
      opacity: 0;
      bottom: 12px;
      width: 14px;
      animation-delay: 1.4;
    }
    :nth-child(7) {
      position: absolute;
      left: 83px;
      opacity: 0;
      bottom: -15px;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 1.3;
    }
    :nth-child(8) {
      position: absolute;
      right: 38px;
      opacity: 0;
      bottom: 1px;
      width: 14px;
      animation-delay: 1.2;
    }
    :nth-child(9) {
      position: absolute;
      right: 10px;
      opacity: 0;
      bottom: 26px;
      width: 19px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 1.8;
    }
    :nth-child(10) {
      position: absolute;
      right: 42px;
      opacity: 0;
      bottom: 40px;
      width: 9px;
      animation-delay: 1.2s;
    }
    :nth-child(11) {
      position: absolute;
      right: 17px;
      top: 56px;
      opacity: 0;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
      animation-delay: 1.4s;
    }
    :nth-child(12) {
      position: absolute;
      right: 41px;
      top: 15px;
      opacity: 0;
      width: 14px;
      animation: twinkleTwo 0.7s alternate infinite;
      animation-delay: 1.6s;
    }
    :nth-child(13) {
      position: absolute;
      left: 71px;
      top: 29px;
      opacity: 0;
      width: 8px;
      animation-delay: 2s;
    }
  }

  @keyframes twinkleOne {
    0% {
      opacity: 0;
    }

    100% {
      opacity: 1;
      transform: scale(1.2);
    }
  }
  @keyframes twinkleTwo {
    0% {
      opacity: 0.5;
    }
    100% {
      opacity: 1;
      transform: scale(1.5);
    }
  }
`;

const NugSection = styled.div``;
const NugWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 979px;
  margin: 0 auto;
  margin-top: 5vh;
  position: relative;
  flex-direction: column;
  .verticleText {
    writing-mode: vertical-rl;
    position: absolute;
    right: -10px;
    top: 0;
    font-size: 18px;
    text-transform: uppercase;
    white-space: nowrap;
    color: #50665e;
    font-family: 'roboto_condensedregular';
    font-weight: 700;
  }
  @media (max-width: 950px) {
    margin-top: 50px;
    .verticleText {
      font-size: 14px;
      text-transform: uppercase;
      color: #50665e;
      margin-bottom: 18px;
      position: unset;
      writing-mode: initial;
    }
  }
  @media (max-width: 600px) {
    margin-top: 00px;
  }
`;

const NugIconWrapper = styled.div`
  position: relative;

  svg.nugBackground {
    transform: scale(2);
    position: absolute;
    top: ${window.innerWidth > 1100 ? '190px' : '140px'};
    left: 0px;
    right: 10px;
    z-index: -1;
    margin: auto;
    opacity: 0.4;
    clip-path: circle(0%);
    animation: circle 4s infinite ease-in-out;
    /* animation-delay: 1s; */
    transition: opacity 2s ease-in-out, height 5s ease-in-out,
      transform 1s ease-in-out;

    @media (max-width: 667px) {
      transform: scale(1.2);
    }
  }

  @keyframes circle {
    0% {
      clip-path: circle(0%);
    }
    50% {
      clip-path: circle(75%);
    }
    100% {
      clip-path: circle(0%);
    }
  }
  .nug {
    position: relative;
    z-index: 1;
    width: 490px;
    height: 390px;

    @media (max-width: 630px) {
      width: 410px;
      height: 310px;
    }

    @media (max-height: 667px) {
      width: 100%;
    }
  }

  .glowBackground {
    width: 1px;
    height: 1px;
    border: none;
    background-color: #f9c014;
    border-radius: 50%;
    animation: glowingIntense 3s infinite alternate;
    transition: opacity 2s ease-in-out;
    box-shadow: 0 0px 100px -100px #f9c014;
    position: absolute;
    top: 235px;
    left: 0px;
    right: 0px;
    margin: auto;
    z-index: -1;
    @media (max-height: 667px) {
      width: 35px;
      height: 35px;
      top: 163px;
    }
  }

  @keyframes glowingIntense {
    0% {
      box-shadow: 0 0px 100px -80px #f9c014;
    }
    40% {
      box-shadow: 0 0px 140px 100px #f9c014;
    }
    60% {
      box-shadow: 0 0px 140px 100px #f9c014;
    }
    100% {
      box-shadow: 0 0px 100px -40px #f9c014;
    }
  }

  @media (max-width: 950px) {
    margin: 1vh 0;
  }
`;
const WaterStats = styled(motion.div)`
  width: 100px;
  color: #50665f;
  text-align: center;

  h4 {
    font-size: clamp(32px, 5vh, 45px);
    line-height: 21px;
    > span {
      font-size: 14.5px;
    }
  }
  p {
    font-size: 15px;
    font-family: 'roboto_condensedregular';
  }

  @media (max-width: 767px) {
    > h4 {
      font-size: 16pt;
    }
    > p {
      font-size: 10pt;
    }
  }
`;
const PlantStats = styled(motion.div)`
  width: 100px;
  color: #738c83;
  text-align: center;
  @media only screen and (max-width: 768px) {
    width: 138px;
  }
  @media only screen and (max-width: 575px) {
    width: 100px;
  }

  h4 {
    font-size: clamp(32px, 5vh, 45px);
    line-height: 21px;
    > span {
      font-size: 14.5px;
    }
  }
  p {
    font-size: 15px;
    font-family: 'roboto_condensedregular';
  }

  @media (max-width: 767px) {
    > h4 {
      font-size: 16pt;
    }
    > p {
      font-size: 10pt;
    }
  }
`;
const Cannabinoids = styled(motion.div)`
  width: 100px;
  color: #8f82c4;
  text-align: center;
  @media only screen and (max-width: 768px) {
    width: 138px;
  }
  @media only screen and (max-width: 520px) {
    width: 100px;
  }

  h4 {
    font-size: clamp(32px, 5vh, 45px);
    line-height: 21px;
    > span {
      font-size: 14.5px;
    }
  }
  p {
    font-size: 15px;
    font-family: 'roboto_condensedregular';
  }
  @media (max-width: 950px) {
  }
  @media (max-width: 767px) {
    > h4 {
      font-size: 16pt;
    }
    > p {
      font-size: 10pt;
    }
  }
`;
const EssentialOil = styled(motion.div)`
  margin-top: 20px;
  width: 120px;
  text-align: center;
  color: #ffb600;
  box-shadow: 0 0 10px -10px #ffffff;
  .bold {
    font-family: 'robotobold';
  }

  > div {
    > h4 {
      font-size: clamp(35px, 8vh, 75px);
      line-height: 25px;
      text-shadow: 3px 2px 4px rgb(84 84 84);
      > span {
        font-size: 30px;
      }
    }
    > p {
      font-size: 17.5px;
      font-family: 'roboto_condensedregular';
      color: #47453e;
      margin-top: 7px;
    }
  }
  @media (max-width: 950px) {
    margin: 0 auto;
    margin-top: 8px;
    width: auto;
    > div {
      > h4 {
        line-height: 40px;
        font-size: 30pt;
      }
      p {
        margin-top: 0;
        font-size: 12pt;
      }
    }
  }
  @media (max-height: 667px) {
    margin-top: 6px;
  }
  @media (max-width: 375px) and (max-height: 667px) {
    > div {
      > h4 {
        line-height: 40px;
        font-size: 26pt;
      }
      p {
        font-size: 11pt;
        line-height: 11pt;
      }
    }
  }
`;

const StatsWrapper = styled.div``;
const WebView = styled.div`
  position: absolute;
  right: 5%;
  top: -3px;

  width: 197px;
  display: flex;
  flex-direction: column;
  height: 455px;
  justify-content: flex-start;
  align-items: center;
  > div {
    margin-bottom: 4.6vh;
    padding-bottom: 4vh;
    width: 100%;
    position: relative;

    :after {
      width: 68%;
      height: 1px;
      display: block;
      content: '';
      background: #46464680;
      position: absolute;
      bottom: 0;
      margin: 0 auto;
      right: 0;
      left: 0;
    }
    :last-child:after {
      display: none;
    }
    :last-child {
      margin-bottom: 0;
      border-bottom: none;
      padding-bottom: 0;
    }
  }
  @media (max-width: 950px) {
    display: none;
  }
`;
const MobileView = styled.div`
  display: none;
  @media (max-width: 950px) {
    display: flex;
    justify-content: center;
  }
`;

const QuintEssentialContainer = styled.div`
  position: relative;
  height: 280vh;
  z-index: 1;
  min-height: 1550px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  overflow: hidden;

  /* @media (min-width: 768px) { */
  padding-bottom: 40px;
  /* } */
`;

export {
  Container,
  Heading,
  HexagonWrapper,
  Box,
  Star,
  NugSection,
  NugWrapper,
  NugIconWrapper,
  WaterStats,
  PlantStats,
  Cannabinoids,
  EssentialOil,
  StatsWrapper,
  WebView,
  MobileView,
  QuintEssentialContainer,
  Wrapper,
};
