import * as React from 'react';
import { motion } from 'framer';

const Icon = ({ pathLength, opacity, ...props }) => (
  <svg
    width={3.5 * 2.9}
    height={window.innerHeight <= 667 ? 113 * 2 : 113 * 2.4}
    viewBox='0 0 1 116'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}>
    <defs>
      <mask id='mask-4'>
        <path
          d='M.5.982v113.756'
          stroke='#ffffff'
          strokeMiterlimit={16}
          strokeLinecap='round'
          strokeLinejoin='round'
          strokeDasharray='3 3'
        />
      </mask>
    </defs>
    <motion.path
      style={{ pathLength: pathLength, opacity: opacity }}
      d='M.5.982v113.756'
      stroke='#58A08F'
      strokeMiterlimit={16}
      strokeLinecap='round'
      strokeLinejoin='round'
      mask='url(#mask-4)'
    />
  </svg>
);

export default Icon;
