const GREEN = '#58a08f';

const defaultTheme = {
  borderColor: GREEN,
  labelColorFocus: GREEN,
  width: '100%',
  padding: '10px 10px',
  backgroundColor: '#efefef',
  labelColor: '#000',
  labelFontSize: '14px',
  labelFontSizeFocus: '15px',
  labelTransform: 'translate(14px, 13px) scale(1)',
  labelTransformFocus: 'translate(0px, -15px) scale(0.75)',
};

const getTheme = (theme) => {
  switch (theme) {
    case 'compact':
      return {
        ...defaultTheme,
        fontFamily: 'robotoregular',
        width: '100%',
        labelTransformFocus: null,
        labelFontSizeFocus: null,
        labelColorFocus: null,
        labelDisplayFocus: 'none',
        labelAlign: 'center',
        labelFontSize: '14px',
        outline: 'none',
      };
    default:
      return defaultTheme;
  }
};

export { getTheme };
