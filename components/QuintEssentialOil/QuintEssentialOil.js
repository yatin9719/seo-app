import React, { useEffect } from 'react';
import HexagonImage from 'assets/Images/hexagon.png';
import HexagonGreenImage from 'assets/Images/green-hexagon.png';
import StarImage from 'assets/Images/star.png';
import GreenHexagonBack from 'assets/Images/hexaBackIcon.png';
import {
  Frame,
  motion,
  useViewportScroll,
  useAnimation,
  useMotionValue,
} from 'framer';
import {
  Heading,
  HexagonWrapper,
  Box,
  Star,
  QuintEssentialContainer,
  Wrapper,
} from 'css/components/QuintEssentialOil';
import { inView } from 'utils';
import BrandLogo from 'assets/SVG/BrandLogoSmall';
const starContainerVariant = {
  initial: { opacity: 0 },
  animate: { opacity: 1, transition: { staggerChildren: 1 } },
};

const starVariant = {
  initial: { opacity: 0 },
  animate: { opacity: 1 },
};

const QuintEssentialOil = ({
  frameStyle,
  spiritSticky,
  collision,
  scrollbar,
  ...props
}) => {
  const { scrollHeight, clientHeight } = collision || 0;
  const { scrollY } = useViewportScroll();
  const flipControls = useAnimation();

  const starContainerControls = useAnimation();
  const starControls = useAnimation();
  const flipAnimation = useMotionValue(0);

  const scrollEvents = () => {
    let offsetY = document.documentElement.scrollTop;
    let midStep = offsetY >= scrollHeight + (clientHeight * 25) / 100;

    if (midStep && flipAnimation.get() === 0) {
      flipControls.start({ transform: 'rotateY(0deg)' });
      starContainerControls.start('animate');
      starControls.start('animate');
      flipAnimation.set(1);
    } else if (!midStep && flipAnimation.get() !== 0) {
      flipControls.start({ transform: 'rotateY(180deg)' });
      starContainerControls.start('initial');
      starControls.start('initial');
      flipAnimation.set(0);
    }
  };

  useEffect(() => {
    const unsubscribe = scrollY.onChange(() => {
      if (inView(scrollHeight, clientHeight, scrollY.get()))
        setTimeout(() => {
          requestAnimationFrame(scrollEvents);
        }, 10);
    });
    return () => unsubscribe();
  });

  return (
    <QuintEssentialContainer id='spiritDiv'>
      <Frame
        id='essentialSection'
        style={{ ...frameStyle, height: '100vh', position: 'sticky' }}
        top='0px'>
        <Wrapper>
          <Heading>
            <h2 className='leadingProducer'>
              THE LEADING producer <span className='ofSpanWeb'>of</span>
              <span className='ofSpanMobile'>of</span>
            </h2>

            <h2 className='headingWrapper'>
              <h4 className='greenText'>
                Cannabis
                <span className={`plusIcon  `}>+</span>&nbsp;
              </h4>
              <h4>
                <span>(quint)</span>
                <span className='greenText'>essential oil</span>
              </h4>
            </h2>
            <BrandLogo />
            <h3 className='aristotle-taught'>
              Aristotle taught that <span>(quint)essence</span> is the 5th
              Element, the spirit of every living thing
            </h3>
          </Heading>
          <HexagonWrapper>
            <div>
              <Box>
                <img src={HexagonImage} alt='HexagonImage' />
                <h5>Water</h5>
              </Box>
              <Box>
                <img src={HexagonImage} alt='HexagonImage' />
                <h5>Earth</h5>
              </Box>
            </div>
            <div>
              <Box>
                <img src={HexagonImage} alt='HexagonImage' />
                <h5>Air</h5>
              </Box>
              <Box id='spirit'>
                <div className='flip-card'>
                  <motion.div
                    className='flip-card-inner'
                    initial={{ transform: 'rotateY(180deg)' }}
                    transition={{ duration: 0.1 }}
                    animate={flipControls}>
                    <div className='flip-card-front'>
                      <img
                        src={HexagonGreenImage}
                        className='greenImage'
                        alt='HexagonGreenImage'
                      />
                      <h5 className='white-text'>Spirit</h5>
                    </div>
                    <div className='flip-card-back'>
                      <img
                        src={HexagonGreenImage}
                        className='greenImageBack'
                        alt='HexagonGreenImage'
                      />
                      <img
                        src={GreenHexagonBack}
                        className='greenImage'
                        alt='HexagonGreenImage'
                      />
                    </div>
                  </motion.div>
                </div>

                <Star
                  initial='initial'
                  animate={starContainerControls}
                  variants={starContainerVariant}>
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                  <motion.img
                    src={StarImage}
                    alt='sparkling'
                    initial='initial'
                    animate={starControls}
                    variants={starVariant}
                  />
                </Star>
              </Box>
              <Box>
                <img src={HexagonImage} alt='HexagonImage' />
                <h5>Fire</h5>
              </Box>
            </div>
          </HexagonWrapper>
        </Wrapper>
      </Frame>
    </QuintEssentialContainer>
  );
};

export default QuintEssentialOil;
