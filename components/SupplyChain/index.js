import React, { useEffect } from 'react';
import FarmHouseIcon from 'assets/SVG/FarmsHouseIcon';
import SunIcon from 'assets/SVG/SunIcon';
import ManIcon from 'assets/SVG/ManIcon';
import FarmTruckIcon from 'assets/SVG/FarmTruckIcon';
import ManufacturingPlantIcon from 'assets/SVG/ManufacturingPlantIcon';
import ManufacturingPlantIconOne from 'assets/SVG/ManufacturingPlantIconOne';
import ManufacturingManIcon from 'assets/SVG/ManufacturingManIcon';
import RefineryManIcon from 'assets/SVG/RefineryManIcon';
import BuildingIcon from 'assets/SVG/BuildingIcon';
import BuildingIconOne from 'assets/SVG/BuildingIconOne';

import BarnIcon from 'assets/SVG/BarnIcon';
import BarnWheelIcon from 'assets/SVG/BarnWheel';
import AnalyticsY from 'assets/SVG/AnalyticsY';
import TruckIcon from 'assets/SVG/TruckIcon';
import {
  SupplyChainContainer,
  SupplyChainWrapper,
  ProcessSectionImage,
  ProcessSectionText,
  Heading,
  MobileView,
  SunGlow,
  SunSection,
  SupplyChainWrapperMobile,
} from 'css/components/SupplyChain';
import {
  Frame,
  motion,
  AnimatePresence,
  useTransform,
  useViewportScroll,
  useAnimation,
  useMotionValue,
} from 'framer';
import { getPercentage, inView } from 'utils';

const borderStyle = {
  borderBottomStyle: 'double',
  borderWidth: '6px',
  borderColor: '#000',
};

const fadeInLeft = {
  initial: { opacity: 0, x: -300 },
  fadeIn: {
    opacity: [0, 0, 1],
    x: [-300, -150, 0],
    transition: { duration: 0.4, type: 'tween', stiffness: 200, bounce: 0 },
    steps: [0, 0.5, 1],
  },
  fadeOut: {
    opacity: [1, 0, 0],
    x: [0, -150, -300],
    transition: { duration: 0.4, type: 'tween', stiffness: 200, bounce: 0 },
    steps: [0, 0.5, 1],
  },
};
const fadeInRight = {
  initial: { opacity: 0, x: 300 },
  fadeIn: {
    opacity: [0, 0, 1],
    x: [300, 150, 0],
    transition: { duration: 0.4, type: 'tween', stiffness: 200, bounce: 0 },
    steps: [0, 0.5, 1],
  },
  fadeOut: {
    opacity: [1, 0, 0],
    x: [0, 150, 300],
    transition: { duration: 0.4, type: 'tween', stiffness: 200, bounce: 0 },
    steps: [0, 0.5, 1],
  },
};

const SupplyChain = ({ scrollbar, ...props }) => {
  const { scrollHeight, clientHeight } = props?.collision || 0;
  const { scrollY } = useViewportScroll();
  const { y } = props;
  const dropCollisionAnimation = useMotionValue(0);
  const dropFallAnimation = useMotionValue(0);
  const farmerAnimationControls = useAnimation();
  const farmerTruckAnimationControls = useAnimation();
  const startAnimation = useMotionValue(0);
  const manuDropAnimation = useMotionValue(0);
  const refiningDropAnimation = useMotionValue(0);
  const sunAnimation = useAnimation();
  const manuAnimationControls = useAnimation();
  const refiningAnalyticsControls = useAnimation();
  const farmerExitAnimation = useMotionValue(0);
  const farmerMobileControls = useAnimation();
  const farmerControls = useAnimation();
  const farmerBoxControls = useAnimation();
  const farmerAnimation = useMotionValue(0);
  const manuMobileControls = useAnimation();
  const manuControls = useAnimation();
  const manuBoxControls = useAnimation();
  const manuTruckControls = useAnimation();
  const manuAnimation = useMotionValue(0);
  const refineryMobileControls = useAnimation();
  const refineryControls = useAnimation();
  const refineryBoxControls = useAnimation();
  const refineryAnimation = useMotionValue(0);
  const refineryRetractAnimation = useMotionValue(0);
  const transformY = useTransform(y, (item) => item / -1);

  const farmerMobileX = useTransform(
    transformY,
    [
      scrollHeight - clientHeight * (35 / 100),
      scrollHeight - clientHeight * (6 / 100),
      scrollHeight - 1,
      scrollHeight + 80,
      scrollHeight + clientHeight * (53 / 100),
      scrollHeight + clientHeight * (56.5 / 100),
    ],
    [
      'calc(-100% - 0px)',
      'calc(0% - 59px)',
      'calc(0% - 59px)',
      'calc(0% - 0px)',
      'calc(0% - 0px)',
      'calc(-100% - 0px)',
    ],
    [
      {
        clamp: false,
        ease: [1, 0.42, 0.55, 0.7, 1],
      },
    ]
  );

  const farmerMobileFade = useTransform(
    scrollY,
    [
      scrollHeight - clientHeight * (12 / 100),
      scrollHeight - clientHeight * (6 / 100),
    ],
    [0, 1]
  );

  const manuMobileX = useTransform(
    scrollY,
    [
      scrollHeight + clientHeight * (20 / 100) - 50,
      scrollHeight + clientHeight * (20 / 100) + 50,
      scrollHeight + clientHeight * (59 / 100),
      scrollHeight + clientHeight * (63 / 100),
    ],
    [
      'calc(-100% - 0px)',
      'calc(0% - 0px)',
      'calc(0% - 0px)',
      'calc(-100% - 0px)',
    ]
  );
  const manuMobileFade = useTransform(
    scrollY,
    [
      scrollHeight + clientHeight * (20 / 100) - 50,
      scrollHeight + clientHeight * (20 / 100) + 50,
    ],
    [0, 1]
  );

  const refiningMobileX = useTransform(
    scrollY,
    [
      scrollHeight + clientHeight * (39 / 100) - 50,
      scrollHeight + clientHeight * (39 / 100),
      scrollHeight + clientHeight * (59 / 100),
      scrollHeight + clientHeight * (63 / 100),
      scrollHeight + clientHeight * (80 / 100),
      scrollHeight + clientHeight * (84 / 100),
    ],
    [
      'calc(-100% - 0px)',
      'calc(0% - 0px)',
      'calc(0% - 0px)',
      'calc(0% - 60px)',
      'calc(0% - 60px)',
      'calc(-100% - 0px)',
    ]
  );
  const refiningMobileFade = useTransform(
    scrollY,
    [
      scrollHeight + clientHeight * (39 / 100) - 50,
      scrollHeight + clientHeight * (39 / 100),
    ],
    [0, 1]
  );

  const opacity = useTransform(
    scrollY,
    [
      scrollHeight - getPercentage(3000, 200),
      scrollHeight - getPercentage(3000, 100),
    ],
    [0, 1]
  );

  const scrollEvents = () => {
    let offsetY = document.documentElement.scrollTop;
    if (window.innerWidth > 1023) {
      let farmerStep = offsetY >= scrollHeight - clientHeight * (17 / 100);
      let startStep = offsetY >= scrollHeight;
      let manuStep = offsetY >= scrollHeight + clientHeight * (12 / 100);
      let manuDrop = offsetY >= scrollHeight + clientHeight * (33.5 / 100);
      let refineryStep = offsetY >= scrollHeight + clientHeight * (27 / 100);
      let refineryDrop = offsetY >= scrollHeight + clientHeight * (66 / 100);

      if (farmerStep && farmerAnimation.get() === 0) {
        farmerControls.start('fadeIn');
        farmerBoxControls.start('fadeIn');
        farmerAnimation.set(1);
      } else if (!farmerStep && farmerAnimation.get() !== 0) {
        farmerControls.start('fadeOut');
        farmerBoxControls.start('fadeOut');
        farmerAnimation.set(0);
      } else if (startStep && startAnimation.get() === 0) {
        farmerAnimationControls.start('animate');
        farmerTruckAnimationControls.start(['move', 'flip']);
        sunAnimation.start({ opacity: 1 });
        startAnimation.set(1);
      } else if (!startStep && startAnimation.get() !== 0) {
        farmerAnimationControls.start('initial');
        farmerTruckAnimationControls.start(['initial']);
        sunAnimation.start({ opacity: 0 });
        startAnimation.set(0);
      } else if (manuStep && manuAnimation.get() === 0) {
        manuControls.start('fadeIn');
        manuBoxControls.start('fadeIn');
        manuAnimation.set(1);
      } else if (!manuStep && manuAnimation.get() !== 0) {
        manuControls.start('fadeOut');
        manuBoxControls.start('fadeOut');
        manuAnimation.set(0);
      } else if (manuDrop && manuDropAnimation.get() === 0) {
        manuAnimationControls.start({ opacity: 1 });
        manuTruckControls.start(['move', 'flip']);
        manuDropAnimation.set(1);
      } else if (!manuDrop && manuDropAnimation.get() !== 0) {
        manuAnimationControls.start({ opacity: 0 });
        manuTruckControls.start('initial');
        manuDropAnimation.set(0);
      } else if (refineryStep && refineryAnimation.get() === 0) {
        refineryControls.start('fadeIn');
        refineryBoxControls.start('fadeIn');
        refineryAnimation.set(1);
      } else if (!refineryStep && refineryAnimation.get() !== 0) {
        refineryControls.start('fadeOut');
        refineryBoxControls.start('fadeOut');
        refineryAnimation.set(0);
      } else if (refineryDrop && refiningDropAnimation.get() === 0) {
        refiningAnalyticsControls.start('animate');
        refiningDropAnimation.set(1);
      } else if (!refineryDrop && refiningDropAnimation.get() !== 0) {
        refiningAnalyticsControls.stop();
        refiningDropAnimation.set(0);
      }
    }

    if (window.innerWidth <= 1023) {
      let farmerStep = offsetY >= scrollHeight - clientHeight * (6 / 100);
      let sectionStart = offsetY >= scrollHeight;
      let manuStep = offsetY >= scrollHeight + clientHeight * (20 / 100);
      let refineryStep = offsetY >= scrollHeight + clientHeight * (33 / 100);
      let refineryDrop = offsetY >= scrollHeight + clientHeight * (50 / 100);
      let refineryRetract = offsetY >= scrollHeight + clientHeight * (80 / 100);

      let farmerExitStep =
        offsetY >= scrollHeight + clientHeight * (53.5 / 100);
      let dropFallStep = offsetY >= scrollHeight + clientHeight * (67.5 / 100);

      if (farmerStep && farmerAnimation.get() === 0) {
        farmerMobileControls.start('dropCatch');
        farmerAnimation.set(1);
      } else if (!farmerStep && farmerAnimation.get() !== 0) {
        farmerMobileControls.start('initial');
        farmerAnimation.set(0);
      } else if (sectionStart && dropCollisionAnimation.get() === 0) {
        farmerMobileControls.start('fullWidth');
        farmerAnimationControls.start('animate');
        farmerTruckAnimationControls.start(['move', 'flip']);
        sunAnimation.start({ opacity: 1 });
        dropCollisionAnimation.set(1);
      } else if (!sectionStart && dropCollisionAnimation.get() !== 0) {
        farmerMobileControls.start('dropCatch');
        farmerAnimationControls.start('initial');
        farmerTruckAnimationControls.start('initial');
        sunAnimation.start({ opacity: 0 });
        dropCollisionAnimation.set(0);
      } else if (manuStep && manuAnimation.get() === 0) {
        manuMobileControls.start('fullWidth');
        manuAnimation.set(1);
      } else if (!manuStep && manuAnimation.get() !== 0) {
        manuMobileControls.start('initial');
        manuAnimation.set(0);
      } else if (refineryStep && refineryAnimation.get() === 0) {
        refineryMobileControls.start('fullWidth');
        manuAnimationControls.start({ opacity: 1 });
        manuTruckControls.start(['move', 'flip']);
        refineryAnimation.set(1);
      } else if (!refineryStep && refineryAnimation.get() !== 0) {
        refineryMobileControls.start('initial');
        manuAnimationControls.start({ opacity: 0 });
        manuTruckControls.start('initial');
        refineryAnimation.set(0);
      } else if (refineryDrop && refiningDropAnimation.get() === 0) {
        refiningAnalyticsControls.start('animate');
        refiningDropAnimation.set(1);
      } else if (!refineryDrop && refiningDropAnimation.get() !== 0) {
        refiningAnalyticsControls.stop();
        refiningDropAnimation.set(0);
      } else if (farmerExitStep && farmerExitAnimation.get() === 0) {
        farmerMobileControls.start('initial');
        farmerExitAnimation.set(1);
      } else if (!farmerExitStep && farmerExitAnimation.get() !== 0) {
        farmerMobileControls.start('fullWidth');
        farmerExitAnimation.set(0);
      } else if (dropFallStep && dropFallAnimation.get() === 0) {
        manuMobileControls.start('initial');
        refineryMobileControls.start('dropFall');
        dropFallAnimation.set(1);
      } else if (!dropFallStep && dropFallAnimation.get() !== 0) {
        manuMobileControls.start('fullWidth');
        refineryMobileControls.start('fullWidth');
        dropFallAnimation.set(0);
      } else if (refineryRetract && refineryRetractAnimation.get() === 0) {
        refineryMobileControls.start('initial');
        refineryRetractAnimation.set(1);
      } else if (!refineryRetract && refineryRetractAnimation.get() !== 0) {
        refineryMobileControls.start('dropFall');
        refineryRetractAnimation.set(0);
      }
    }
  };

  useEffect(() => {
    farmerMobileControls.start('initial');
    manuMobileControls.start('initial');
    refineryMobileControls.start('initial');
    const unsubscribe = scrollY.onChange(() => {
      if (inView(scrollHeight, clientHeight, scrollY.get()))
        setTimeout(() => {
          requestAnimationFrame(scrollEvents);
        }, 10);
    });
    return () => unsubscribe();
  });

  return (
    <SupplyChainContainer id='process_section'>
      <Frame
        id='process_wrapper'
        top='0px'
        style={{
          width: '100%',
          height: '100vh',
          position: 'sticky',
          backgroundColor: null,
          zIndex: 1,
          overflowX: 'hidden',
        }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            height: '100%',
          }}>
          <Heading>
            <h2>The Future is Here</h2>

            <h3>
              The spirit of California is now commercially available for export
            </h3>
          </Heading>

          <SupplyChainWrapper>
            <ProcessSectionImage>
              <motion.div
                initial='initial'
                animate={farmerControls}
                variants={fadeInLeft}
                id='farmer-block'
                style={{
                  width: '100%',
                  right: 0,
                  bottom: 0,
                  ...borderStyle,
                }}>
                <ManIcon />
                <FarmHouseIcon />
                <SunSection>
                  <SunGlow initial={{ opacity: 0 }} animate={sunAnimation} />
                  <SunIcon id={window.innerWidth > 1023 ? 'sunIcon' : null} />
                </SunSection>

                <BarnIcon />

                <BarnWheelIcon controls={farmerAnimationControls} />
                <FarmTruckIcon controls={farmerTruckAnimationControls} />
              </motion.div>
              <MobileView>
                <h3>01 farms</h3>
              </MobileView>

              <motion.div
                id='manufacture-block'
                initial='initial'
                animate={manuControls}
                variants={fadeInLeft}
                style={{
                  width: '100%',
                  right: 0,
                  bottom: 0,
                  ...borderStyle,
                }}>
                <ManufacturingPlantIcon
                  id={window.innerWidth > 1023 ? 'manufacturingPlant' : null}
                />
                <ManufacturingPlantIconOne />
                <ManufacturingManIcon />
                <TruckIcon controls={manuTruckControls} />
                <motion.div
                  id='smokeAreaSection'
                  initial={{ opacity: 0 }}
                  animate={manuAnimationControls}>
                  <div id='smoke1'>
                    <span className='s0'></span>
                    <span className='s1'></span>
                    <span className='s2'></span>
                    <span className='s3'></span>
                    <span className='s4'></span>
                    <span className='s5'></span>
                    <span className='s6'></span>
                    <span className='s7'></span>
                    <span className='s8'></span>
                    <span className='s9'></span>
                  </div>
                  <div id='smoke2'>
                    <span className='s0'></span>
                    <span className='s1'></span>
                    <span className='s2'></span>
                    <span className='s3'></span>
                    <span className='s4'></span>
                    <span className='s5'></span>
                    <span className='s6'></span>
                    <span className='s7'></span>
                    <span className='s8'></span>
                    <span className='s9'></span>
                  </div>
                </motion.div>
              </motion.div>
              <MobileView>
                <p>Terpene Belt</p>
                <h3> Manufacturing Plant</h3>
              </MobileView>

              <motion.div
                id='refining-block'
                initial='initial'
                animate={refineryControls}
                variants={fadeInLeft}
                style={{
                  width: '100%',
                  right: 0,
                  bottom: 0,
                  margin: 0,
                  ...borderStyle,
                }}>
                <RefineryManIcon />
                <div className='analyticsWrapper'>
                  <AnalyticsY
                    right={161}
                    height={12}
                    controls={refiningAnalyticsControls}
                  />
                  <AnalyticsY
                    right={157.5}
                    height={9}
                    controls={refiningAnalyticsControls}
                  />
                  <AnalyticsY
                    right={154}
                    height={12}
                    controls={refiningAnalyticsControls}
                  />
                  <AnalyticsY
                    right={150.5}
                    height={9}
                    controls={refiningAnalyticsControls}
                  />
                  <AnalyticsY
                    right={147}
                    height={12}
                    controls={refiningAnalyticsControls}
                  />
                </div>
                <BuildingIcon
                  id={window.innerWidth > 1023 ? 'refineryBuilding' : null}
                />
                <BuildingIconOne />
              </motion.div>
              <MobileView>
                <p>Terpene Belt</p>
                <h3>Refinery</h3>
              </MobileView>
            </ProcessSectionImage>
            <ProcessSectionText>
              <motion.div
                initial='initial'
                animate={farmerBoxControls}
                variants={fadeInRight}>
                <div>
                  <h4 className='light-green'>FARMING</h4>
                  <p>
                    World class cannabis grown under the California sun by our
                    team of fragrance farmers. Every regional farm in our
                    network specializes in producing terrior-driven terpene
                    profiles.
                  </p>
                </div>
              </motion.div>

              <motion.div
                initial='initial'
                animate={manuBoxControls}
                variants={fadeInRight}>
                <div>
                  <h4 className='dark-green'>PROCESSING</h4>
                  <p>
                    Our oils are produced from plants harvested at peak ripeness
                    and processed within minutes to preserve everything that
                    makes California cannabis special.
                  </p>
                </div>
              </motion.div>

              <motion.div
                initial='initial'
                animate={refineryBoxControls}
                variants={fadeInRight}>
                <div>
                  <h4 className='orange'>REFINING</h4>
                  <p>
                    Producing oil is just the beginning of the process. Our
                    cannabis chemists tirelessly chase perfection so that you
                    taste the Spirit of California in every drop.
                  </p>
                </div>
              </motion.div>
            </ProcessSectionText>
          </SupplyChainWrapper>

          <SupplyChainWrapperMobile>
            <ProcessSectionImage>
              <AnimatePresence>
                <motion.div
                  id='farmer-block'
                  style={{
                    position: 'relative',
                    x: farmerMobileX,
                    opacity: farmerMobileFade,
                    zIndex: 1,
                    willChange: 'transform',
                    ...borderStyle,
                  }}>
                  <FarmHouseIcon />
                  <SunGlow initial={{ opacity: 0 }} animate={sunAnimation} />
                  <SunIcon id={window.innerWidth <= 1023 ? 'sunIcon' : null} />
                  <ManIcon />
                  <BarnIcon />
                  <BarnWheelIcon controls={farmerAnimationControls} />
                  <FarmTruckIcon controls={farmerTruckAnimationControls} />
                  <MobileView>
                    <motion.div
                      style={{
                        opacity: opacity,
                      }}>
                      <h3 className='light-green'>FARMING</h3>
                    </motion.div>
                  </MobileView>
                </motion.div>
              </AnimatePresence>

              <motion.div
                id='manufacture-block'
                style={{
                  x: manuMobileX,
                  opacity: manuMobileFade,
                  willChange: 'transform',
                  ...borderStyle,
                }}>
                <ManufacturingPlantIcon
                  id={window.innerWidth <= 1023 ? 'manufacturingPlant' : null}
                />
                <ManufacturingPlantIconOne />
                <ManufacturingManIcon />
                <TruckIcon controls={manuTruckControls} />
                <motion.div
                  id='smokeAreaSection'
                  initial={{ opacity: 0 }}
                  animate={manuAnimationControls}>
                  <div id='smoke1'>
                    <span className='s0'></span>
                    <span className='s1'></span>
                    <span className='s2'></span>
                    <span className='s3'></span>
                    <span className='s4'></span>
                    <span className='s5'></span>
                    <span className='s6'></span>
                    <span className='s7'></span>
                    <span className='s8'></span>
                    <span className='s9'></span>
                  </div>
                  <div id='smoke2'>
                    <span className='s0'></span>
                    <span className='s1'></span>
                    <span className='s2'></span>
                    <span className='s3'></span>
                    <span className='s4'></span>
                    <span className='s5'></span>
                    <span className='s6'></span>
                    <span className='s7'></span>
                    <span className='s8'></span>
                    <span className='s9'></span>
                  </div>
                </motion.div>
                <MobileView>
                  <motion.div
                    style={{
                      opacity: opacity,
                    }}>
                    <h3 className='dark-green'>PROCESSING</h3>
                  </motion.div>
                </MobileView>
              </motion.div>
              <AnimatePresence>
                <motion.div
                  id='refining-block'
                  style={{
                    x: refiningMobileX,
                    opacity: refiningMobileFade,
                    willChange: 'transform',
                    ...borderStyle,
                  }}>
                  <RefineryManIcon />
                  <div className='analyticsWrapper'>
                    <AnalyticsY
                      right={147}
                      height={10}
                      controls={refiningAnalyticsControls}
                    />
                    <AnalyticsY
                      right={122}
                      height={7}
                      controls={refiningAnalyticsControls}
                    />
                    <AnalyticsY
                      right={141}
                      height={10}
                      controls={refiningAnalyticsControls}
                    />
                    <AnalyticsY
                      right={138}
                      height={7}
                      controls={refiningAnalyticsControls}
                    />
                    <AnalyticsY
                      right={135}
                      height={10}
                      controls={refiningAnalyticsControls}
                    />
                  </div>

                  <BuildingIcon
                    id={window.innerWidth <= 1023 ? 'refineryBuilding' : null}
                  />
                  <BuildingIconOne />
                  <MobileView>
                    <motion.div
                      style={{
                        opacity: opacity,
                      }}>
                      <h3 className='orange'>REFINING</h3>
                    </motion.div>
                  </MobileView>
                </motion.div>
              </AnimatePresence>
            </ProcessSectionImage>
          </SupplyChainWrapperMobile>
        </div>
      </Frame>
    </SupplyChainContainer>
  );
};

export default SupplyChain;
