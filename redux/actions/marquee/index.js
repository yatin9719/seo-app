const SET_CATEGORY = 'SET_CATEGORY';
const INITIALIZE_CATEGORY = 'INITIALIZE_CATEGORY';

export { SET_CATEGORY, INITIALIZE_CATEGORY };
