import Client from 'shopify-buy';

export const ua = navigator.userAgent.toLowerCase();
export const isAndroid = ua.indexOf('android') > -1;

export const ifiPhone = () =>
  ['iPhone Simulator', 'iPhone'].includes(navigator.platform);

export const getPercentage = (sectionHeight, value) =>
  (sectionHeight * (value / sectionHeight) * 100) / 100;

export const disableScroll = () => {
  document.body.style.overflowY = 'hidden';
  document.body.style.touchAction = 'none';
};

export const enableScroll = () => {
  document.body.style = '';
};

export const client = Client.buildClient({
  storefrontAccessToken: process.env.REACT_APP_STOREFRONT_ACCESSTOKEN,
  domain: process.env.REACT_APP_STOREFRONT_DOMAIN,
});

export const selectStyle = {
  styles: {
    placeholder: (base) => ({
      ...base,
      color: '#1e1e1e',
      fontFamily: 'robotoregular300',
      fontSize: '14px',
    }),
    indicatorSeparator: (base) => ({
      ...base,
      display: 'none',
    }),
    indicatorsContainer: (base) => ({
      ...base,
      paddingRight: '10px',
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        backgroundColor: isDisabled
          ? null
          : isSelected
          ? '#58a08f'
          : isFocused
          ? '#88cfbe'
          : null,
        color: isDisabled
          ? '#ccc'
          : isSelected
          ? '#58a08f'
            ? 'white'
            : 'black'
          : data.color,

        ':active': {
          ...styles[':active'],
          backgroundColor: !isDisabled && (isSelected ? '58a08f' : '58a08f'),
        },
      };
    },
    menu: (base) => ({
      ...base,
      textAlign: 'left',
      margin: '0',
    }),
    menuList: (base) => ({
      ...base,
      boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.25)',
      backgroundColor: '#F9F9F9',
      borderRadius: '5px 0px 5px 0',
      margin: '0',
    }),
  },
  className: 'SelectInput',
};

export const variants = {
  initial: { opacity: 0 },
  animate: { opacity: 1 },
  exit: { opacity: 0 },
};

export const inView = (scrollHeight, clientHeight, scroll) =>
  !!(
    scroll >= scrollHeight - window.innerHeight &&
    scroll <= scrollHeight + clientHeight + window.innerHeight
  );

export const scrollTo = (element, duration) => {
  var e = document.documentElement;
  if (e.scrollTop === 0) {
    var t = e.scrollTop;
    ++e.scrollTop;
    e = t + 1 === e.scrollTop-- ? e : document.body;
  }
  scrollToC(e, e.scrollTop, element, duration);
};

const scrollToC = (element, from, to, duration) => {
  if (duration <= 0) return;
  if (typeof from === 'object') from = from.offsetTop;
  if (typeof to === 'object') to = to.offsetTop;
  scrollToX(element, from, to, 0, 1 / duration, 20, easeInOutCuaic);
};

const scrollToX = (element, xFrom, xTo, t01, speed, step, motion) => {
  if (t01 < 0 || t01 > 1 || speed <= 0) {
    element.scrollTop = xTo;
    return;
  }
  element.scrollTop = xFrom - (xFrom - xTo) * motion(t01);
  t01 += speed * step;

  setTimeout(() => {
    scrollToX(element, xFrom, xTo, t01, speed, step, motion);
  }, step);
};

const easeInOutCuaic = (t) => {
  t /= 0.5;
  if (t < 1) return (t * t * t) / 2;
  t -= 2;
  return (t * t * t + 2) / 2;
};
