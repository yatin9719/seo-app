import * as React from 'react';
import { motion } from 'framer';

const Icon = ({ pathLength, opacity, ...props }) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    width={115 * 2.85}
    viewBox='0 0 115 59.673'
    {...props}>
    <defs>
      <mask id='mask-3'>
        <path
          data-name='Vector 4'
          d='M1 1h113v57.673'
          fill='none'
          stroke='#ffffff'
          strokeLinecap='round'
          strokeLinejoin='bevel'
          strokeWidth={2}
          strokeDasharray='4 4'
        />
      </mask>
    </defs>
    <motion.path
      data-name='Vector 4'
      d='M1 1h113v59.673'
      fill='none'
      stroke='#58a08f'
      style={{ pathLength: pathLength, opacity: opacity }}
      strokeLinecap='round'
      strokeLinejoin='bevel'
      strokeWidth={2}
      mask='url(#mask-3)'
    />
  </svg>
);
export default Icon;
