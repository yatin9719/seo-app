import * as React from 'react';
import { motion } from 'framer';

const Icon = ({ pathLength, opacity, ...props }) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    viewBox='0 0 18 43'
    width={18 * 2.3}
    height={43 * 2.3}
    {...props}>
    <defs>
      <mask id='mask-5'>
        <path
          id='prefix__Layer'
          d='M-17.3 41.16V-.04h35.25v41.2'
          fill='none'
          stroke='#ffffff'
          strokeWidth='1.5'
          strokeLinecap='round'
          strokeLinejoin='bevel'
          strokeDasharray='2.9 2.9'
        />
        {/* <path
          data-name='Vector 9'
          d='M48 93.77V.5H.5'
          fill='none'
          stroke='#ffffff'
          strokeLinecap='round'
          strokeLinejoin='bevel'
          strokeDasharray='8 8'
        /> */}
      </mask>
    </defs>
    <motion.path
      style={{ pathLength, opacity }}
      d='M-17.3 41.16V-.04h35.25v41.2'
      fill='none'
      stroke='#58a08f'
      strokeWidth='1.5'
      strokeLinecap='round'
      strokeLinejoin='bevel'
      mask='url(#mask-5)'
    />
  </svg>
);

export default Icon;
