import React from 'react';
import LegalOneIcon from 'assets/SVG/LegalOneIcon';
import LegalTwoIcon from 'assets/SVG/LegalTwoIcon';
import LegalThreeIcon from 'assets/SVG/LegalThreeIcon';
import {
  Container,
  Wrapper,
  Heading,
  FarmAct,
  IconSection,
} from 'css/components/FarmBill';

const FarmBill = () => {
  return (
    <Container>
      <Wrapper>
        <Heading>
          <h2>
            <div>
              Cannabis<small>+</small> Essential Oils
            </div>
            <div>Regulated & Legal</div>
          </h2>
          <h3>Fragrance, Essential Oil, just like any other</h3>
        </Heading>
        <FarmAct>
          <div>
            <IconSection>
              <LegalOneIcon />
            </IconSection>
            <h4>
              Agriculture Improvement Act of 2018 (2018 Farm Bill) removed hemp
              from Schedule I of the federal Controlled Substances Act (CSA),
              Hemp is no longer federally regulated as a controlled substance.
            </h4>
          </div>
          <div>
            <IconSection>
              <LegalTwoIcon />
            </IconSection>
            <h4>
              In addition to completely removing hemp from the CSA, the Farm
              Bill allows protections for the transportation of hemp across
              state lines, no US State may prohibit the interstate
              transportation or shipment of hemp lawfully produced.
            </h4>
          </div>
          <div>
            <IconSection>
              <LegalThreeIcon />
            </IconSection>
            <h4>
              The 2018 Farm Bill legalized industrial hemp to be treated as any
              other commodity, including the right to export and be imported by
              any country in which Hemp and Hemp importation is legal.
            </h4>
          </div>
        </FarmAct>
      </Wrapper>
    </Container>
  );
};

export default FarmBill;
