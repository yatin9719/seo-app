import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';
const Container = styled.div`
  position: relative;
  background: lightgrey;
  min-height: 300vh;
  background: url(${LegalBg}),
    linear-gradient(180deg, #58a08f33 0%, #ffffff33 100%), #c4c4c433;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: top;
  /* overflow-x: clip; */
  /* display: flex;
  flex-direction: column;
  justify-content: center; */
`;
const Heading = styled.div`
  text-align: center;
  /* padding-top: 77px; */
  /* position: relative; */
  /* padding-top: 100px; */

  > div {
    padding: 50px 0;
  }
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    max-width: 100%;

    .smallText {
      font-size: 17.5px;
    }
  }
  p {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    width: 56%;
    margin: 0 auto;
    margin-top: 10px;
  }
  @media (max-width: 767px) {
    > div {
      padding: 0;
    }
    /* padding-top: 65px; */
    h2 {
      line-height: 16pt;
      font-size: 16pt;
      max-width: 300px;
      margin: 0 auto;
    }
    p {
      font-size: 12ptpx;
      width: 51%;
    }
  }
  @media (max-width: 320px) {
    /* padding-top: 61px; */
    h2 {
      line-height: 16pt;
      font-size: 11pt;
      max-width: 90%;
      margin: 0 auto;
    }
    p {
      font-size: 9pt;
      width: 51%;
    }
  }
`;
const IconSection = styled.div`
  align-items: center;
  max-width: 992px;
  margin: 0 auto;
  display: grid;
  grid-template-rows: 246px 246px;
  grid-template-columns: repeat(auto-fill, minmax(min(50%, 200px), 1fr));
  margin-top: 50px;
  padding-bottom: 50px;
  width: 100%;
  > div {
    position: relative;
    display: grid;
    grid-template-rows: 130px 36px;
    text-align: center;
    text-decoration: none;

    cursor: ${(props) => (props?.pointer ? 'pointer' : null)};
    margin: 15px 36px;

    svg {
      margin: 0 auto;
      align-self: center;
      position: absolute;
      left: 0;
      right: 0;
      width: 70px;
      height: 70px;
    }

    > div {
      height: 100%;
      display: flex;

      > div {
        display: flex;
        position: absolute;
        width: 100%;
        height: 100%;
        justify-content: center;
        align-items: center;
        align-content: center;

        > svg {
          position: relative;
          margin-bottom: 30px;
        }
      }

      > a {
        display: flex;
        width: 100%;
        position: relative;
        z-index: ${(props) => (props?.pointer ? 1 : 0)};

        :hover {
          opacity: 0.8;
        }
        :active {
          opacity: 0.6;
        }
      }
    }

    > p {
      font-family: 'roboto_condensedregular';
      font-size: 22.5px;
      text-transform: capitalize;
      position: absolute;
      bottom: 0;
      right: 0;
      left: 0;
      > a {
        text-decoration: none;
        color: #4a4a4a;
      }
    }
  }
  @media (min-width: 768px) and (max-height: 760px) {
    margin-top: 0;
  }
  @media (max-width: 768px) {
    /* grid-template-rows: auto auto;
    grid-template-columns: repeat(auto-fill, minmax(min(50%, 400px), 1fr));
    margin: 20px auto auto auto; */
    display: flex;
    flex-wrap: wrap;
    padding: 0;
    margin: 0;
    > div {
      grid-template-rows: 76px 18px;
      margin: 0 0 15px 0;
      width: 50%;

      svg {
        height: 45px;
        width: 45px;
      }
    }
    p {
      font-size: 15px !important;
    }
  }
  @media (width: 320px) and (height: 568px) {
    > div {
      grid-template-rows: 53px 18px;
    }
  }
`;

const SwitchContainer = styled.div`
  .switch {
    position: relative;
    display: flex;
    width: 100%;
    height: 60px;
    align-items: center;
    cursor: pointer;
    @media (max-width: 767px) {
      height: 50px;
    }
  }
  h3 {
    position: relative;
    /* z-index: 1; */
    color: #7d7d7d;
    flex: 1;
    text-align: center;
    font-family: 'robotobold';
  }

  .products-append {
    @media (max-width: 768px) {
      display: none;
    }
  }

  .no-append {
    @media (min-width: 768px) {
      display: none;
    }
  }

  .switch input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ffffff;
    -webkit-transition: 0.4s;
    transition: 0.4s;
  }

  .toggle {
    position: absolute;
    content: '';
    height: 50px;
    width: 50%;
    left: 5px;
    border-radius: 34px;
    bottom: 5px;
    background-color: #58a08f;
    -webkit-transition: 0.4s;
    transition: 0.4s;
    @media (max-width: 767px) {
      height: 40px;
    }
  }

  /* input:checked + .slider:before {
    -webkit-transform: translateX(calc(100% - 10px));
    -ms-transform: translateX(calc(100% - 10px));
    transform: translateX(calc(100% - 10px));
  } */

  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 34px;
  }
  @media (max-width: 768px) {
    h3 {
      font-size: 15px;
      font-family: 'roboto_condensedregular';
    }
  }
`;

export { Container, Heading, IconSection, SwitchContainer };
