import styled from 'styled-components';
import { motion } from 'framer';

const Scrollbar = styled(motion.div)`
  width: 10px;
  height: 40px;
  background-color: #a6a6a673;
  position: fixed;
  top: 0;
  right: 0;
  z-index: 999999;
  border-radius: 10px;
`;

export { Scrollbar };
