import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';
import spiritBackground from 'assets/Images/spirit-background.png';
const SectionContainer = styled.div`
  display: flex;
  background: linear-gradient(
    180deg,
    rgba(140, 203, 188, 0.39) 0%,
    rgba(140, 203, 188, 0.39) 99.99%
  );
  max-height: 100vh;
  overflow: hidden;
`;
const HeroWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const HeroBg = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
  -o-object-fit: cover;
`;

const BannerText1 = styled.h2`
  font-size: clamp(17.5px, 3.5vw, 22.5px);
  color: #fff;
  transition-delay: 9s;
  line-height: 29px !important;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  position: relative;
`;
const BannerText2 = styled.h2`
  /* font-size: 50px; */
  font-size: clamp(35px, 3.5vw, 50px);
  color: #fff;
  text-align: center;

  @media (max-width: 840px) {
    line-height: 34px;
    .smallText1 {
      margin-right: 0 !important;
    }
  }
`;
const BannerText3 = styled.h3`
  width: fit-content;
  margin: 0 auto;
  font-size: 22.5px;
  color: #fff;
  font-family: 'roboto_condensedregular';
`;
const BannerTextWrapper = styled.div`
  /* height: 100vh; */
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  background: #00000033;

  .error {
    color: #ffffff;
    text-align: center;
    font-family: 'roboto_condensedregular';

    .retry-link {
      text-decoration: underline;
      cursor: pointer;
    }
  }

  #watchVideos {
    cursor: pointer;
    @media (min-height: 640px) and (max-height: 1024px) {
      margin: 44px;
    }
  }
  .smallText {
    font-size: 7px;
    position: absolute;
    top: -13px;
    margin-left: 5px;
    line-height: 44px;
    @media (max-width: 525px) {
      top: -10px;
    }
  }
  .smallText1 {
    font-size: 14px;
    position: relative;
    top: -21px;
    margin-right: 24px;
    line-height: normal;
    @media (max-width: 525px) {
      top: -13px;
    }
  }
  > svg {
    z-index: 1;
    cursor: pointer;
  }
  @media (max-width: 840px) {
    .BannerText1Mobile {
      font-size: 14px;
    }
    .BannerText2Mobile {
      font-size: 25pt;
      /* line-height: 20pt !important; */
      max-width: 262px;
      .smallText1 {
        top: -10px !important;
        font-size: 10px;
      }
    }
  }
`;
const MuteText = styled.div`
  z-index: 1;
  color: #fff;
  font-family: 'roboto_condensedregular';
`;
const UnMuteText = styled.div`
  z-index: 1;
  color: #fff;
  font-family: 'roboto_condensedregular';
`;
const Skip = styled.div`
  cursor: pointer;
  margin-top: 100px;
  position: absolute;
  bottom: 10px;
  right: 0;
  left: 0;
  color: #fff;
  font-family: 'roboto_condensedregular';

  @media (max-width: 968px) {
    bottom: 100px;
  }

  > h4 {
    width: fit-content;
    margin: 20px auto;
    font-size: 16px;
    text-transform: uppercase;
    cursor: pointer;
  }
`;
const BannerWrapper = styled.div`
  height: 100vh;
  /* max-height: 800px; */
  position: relative;
  width: 100%;
  margin-left: ${(props) => (props.toggle ? '0' : '-100%')};
  transition: margin-left 0.35s ease-in-out, left 0.35s ease-in-out,
    margin-right 0.35s ease-in-out, right 0.35s ease-in-out;
  background: linear-gradient(
    180deg,
    rgba(140, 203, 188, 0.39) 0%,
    rgba(140, 203, 188, 0.39) 99.99%
  );
  @media (max-width: 767px) {
    display: none;
  }
`;

const MultiProductMarquee = styled.div`
  position: relative;
  display: flex;
  min-height: 700px;
  margin-right: ${(props) => (props.toggle ? '-100%' : '0')};
  transition: margin-left 0.35s ease-in-out, left 0.35s ease-in-out,
    margin-right 0.35s ease-in-out, right 0.35s ease-in-out;
  width: 100%;
  position: relative;
  @media (max-width: 767px) {
    margin-right: 0;
    min-height: 100%;
  }
`;

const MultiProductContainers = styled.div`
  background: ${(props) => props?.background};
  > div {
    padding-top: 45%;
    text-align: center;
    transition: transform 0.35s ease-in-out, left 0.35s ease-in-out;
  }
`;
const PlayAgain = styled.div`
  position: absolute;
  top: 420px;
  padding: 0 !important;
  width: 100%;
  height: 246px;
  text-align: center;
  > div {
    background: transparent;
    justify-content: space-between;
    .smallText {
      font-size: 7px;
      position: unset;
      /* top: -11px; */
      margin-left: 5px;
      line-height: 20px;
    }
    > h4 {
      cursor: pointer;
      display: flex;
      align-items: center;
      font-family: 'roboto_condensedregular' !important;
      font-size: 22.5px;
      color: #635d5d;
      font-weight: normal;
      margin-top: 42px;
      > span {
        background: #635d5d;
        padding: 0;
        border-radius: 26px;
        width: 53px;
        height: 53px;
        display: block;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0 15px;
        > img {
          width: 45%;
          margin-left: 6px;
        }
      }
    }
  }
`;
const WebView = styled.div`
  display: flex;
  flex: 1;

  > div {
    flex: 1;
    padding: 15px;
    color: #fff;
    transition: transform 0.2s;
    :hover {
      cursor: pointer;
      svg {
        transform: scale(1.2);
      }
    }
    p {
      font-size: 15px;
      margin: 0;
      font-family: 'robotobold';
      transition: display 0.35s ease-in-out, left 0.35s ease-in-out;
    }
    h4 {
      font-size: 30px;
      margin: 0;
      font-family: 'hansonbold';
      text-transform: uppercase;
      transition: display 0.35s ease-in-out, left 0.35s ease-in-out;
    }
    svg {
      margin-bottom: 10px;
      transition: transform 0.35s ease-in-out, left 0.35s ease-in-out;
    }
  }
  @media (max-width: 840px) {
    display: none;
  }
`;
const IconSection = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: 0 30px;
  > div {
    width: 32%;
    display: flex;
    align-items: center;
    flex-direction: column;
    margin-bottom: 20px;
    p {
      text-transform: uppercase;
    }
    :last-child {
      margin-bottom: 0;
    }
  }
  svg {
    width: 70px;
    height: 70px;
    margin-bottom: 10px;
  }
  @media (max-width: 375px) and (max-height: 812px) {
    margin-bottom: 40px;
    svg {
      width: 50px;
      height: 50px;
    }
  }
  @media (max-width: 375px) and (max-height: 667px) {
    padding-top: 0;
    svg {
      width: 50px;
      height: 50px;
    }
  }
`;
const MobileView = styled.div`
  display: none;

  @media (max-width: 840px) {
    display: flex;
    flex-direction: column;
    padding-top: 100px;

    background-image: linear-gradient(
        180deg,
        rgba(140, 203, 188, 0.39) 0%,
        rgba(247, 149, 48, 0.39) 100%
      ),
      url(${LegalBg}), url(${spiritBackground});
    background-position: center, top, 8% -10%;
    background-size: 108%, 100%, 473%;
    background-repeat: no-repeat;
    justify-content: space-between;
    ${IconSection} {
      > div {
        p {
          font-size: 12px;
          line-height: 15px;
        }
      }
    }
    ${BannerTextWrapper} {
      position: unset;
      background: transparent;
      width: 84%;
      margin: 0 auto;
    }
    ${BannerText1},${BannerText2},${BannerText3} {
      color: #4a4a4a;
      line-height: 37px;
    }
    ${BannerText3} {
      font-size: 15px;
    }
    ${BannerText1} {
      .smallText {
        font-size: 7px;
        position: unset;
        margin-left: 5px;
        line-height: 22px;
      }
    }
    ${BannerText2} {
      .smallText1 {
        margin-right: 0;
        top: -14px;
      }
    }
  }
  @media (max-width: 768px) {
    width: 100vw;
    height: 100vh;
    justify-content: space-evenly;
    padding-top: 62px;
    padding-bottom: 68px;
  }
  @media (max-width: 667px) {
    line-height: 20pt;
    font-size: 12pt;
    .smallText {
      top: -15px;
      font-size: 7px;
      line-height: 15pt;
      margin-left: 2px;
    }
  }
  @media (max-width: 375px) and (max-height: 812px) {
    ${BannerText3} {
      font-size: 15px;
      font-weight: unset;
      line-height: 20px;
      padding-top: 10px;
    }
  }
  @media (max-width: 375px) and (max-height: 667px) {
    ${BannerText3} {
      padding: 10px;
    }
  }
`;

const VideoPlayButton = styled.div`
  font-family: 'roboto_condensedbold';
  text-transform: uppercase;
  margin: 15px 0;
  h4 {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 15px;
  }
  span {
    background: #635d5d;
    padding: 13px;
    border-radius: 50%;
    height: 65px;
    width: 65px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    margin: 0 9px;
    padding: 20px;
    img {
      width: 100%;
      margin-left: 6px;
    }
  }
`;
export {
  SectionContainer,
  HeroBg,
  HeroWrapper,
  BannerText1,
  BannerText2,
  BannerText3,
  BannerTextWrapper,
  MuteText,
  UnMuteText,
  Skip,
  BannerWrapper,
  MultiProductMarquee,
  MultiProductContainers,
  PlayAgain,
  WebView,
  MobileView,
  IconSection,
  VideoPlayButton,
};
