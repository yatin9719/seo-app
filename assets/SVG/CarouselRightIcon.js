import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      width={11}
      height={29}
      viewBox="0 0 11 29"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M2.773 28.212l7.779-13.923H8.335L.556 28.212h2.217zM2.773.366H.556l7.779 13.923h2.217L2.773.366z"
        fill="#58A08F"
      />
    </svg>
  )
}

export default SvgComponent