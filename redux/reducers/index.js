import { combineReducers } from 'redux';
import video from 'redux/reducers/video';
import marquee from 'redux/reducers/marquee';
import loader from 'redux/reducers/loader';
import products from 'redux/reducers/products';
import section from 'redux/reducers/section';

export default combineReducers({
  loader,
  video,
  marquee,
  products,
  section,
});
