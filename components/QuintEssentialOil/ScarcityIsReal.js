import React, { useEffect } from 'react';
import {
  Frame,
  motion,
  useTransform,
  useViewportScroll,
  useAnimation,
  useMotionValue,
  useSpring,
} from 'framer';
import NugBackgroundIcon from 'assets/SVG/NugBackgroundIcon';
import NugIcon from 'assets/SVG/NugIcon';
import {
  Heading,
  NugSection,
  NugWrapper,
  NugIconWrapper,
  WaterStats,
  PlantStats,
  Cannabinoids,
  EssentialOil,
  StatsWrapper,
  WebView,
  MobileView,
} from 'css/components/QuintEssentialOil';
import { inView } from 'utils';

const fadeInLeft = {
  initial: { opacity: 0, x: 150 },
  fadeIn: {
    opacity: [0, 0, 1],
    x: [150, 75, 0],
    transition: { duration: 0.25 },
    steps: [0, 0.5, 1],
  },
  fadeOut: {
    opacity: [1, 0, 0],
    x: [0, 75, 150],
    transition: { duration: 0.25 },
    steps: [0, 0.5, 1],
  },
};

const fadeInMobile = {
  initial: { opacity: 0, x: -150 },
  fadeIn: {
    opacity: [0, 0, 1],
    x: [-150, -75, 0],
    transition: { duration: 0.25 },
    steps: [0, 0.5, 1],
  },
  fadeOut: {
    opacity: [1, 0, 0],
    x: [0, -75, -150],
    transition: { duration: 0.25 },
    steps: [0, 0.5, 1],
  },
};

const ScarcityIsReal = ({
  frameStyle,
  collision,
  checkRef,
  scrollbar,
  fill,
  ...props
}) => {
  const { scrollHeight, clientHeight } = collision || 0;
  const waterControls = useAnimation();
  const waterAnimation = useMotionValue(0);
  const plantControls = useAnimation();
  const plantAnimation = useMotionValue(0);
  const cannControls = useAnimation();
  const cannAnimation = useMotionValue(0);
  const oilControls = useAnimation();
  const oilAnimation = useMotionValue(0);
  const glowControls = useAnimation();
  const glowEndControls = useAnimation();
  const glowEndAnimation = useMotionValue(0);
  const { scrollY } = useViewportScroll();
  const { y } = props;

  const waterInputProps = [
    -scrollHeight - (clientHeight * 1) / 100,
    -scrollHeight - (clientHeight * 7) / 100,
  ];
  const waterAnimate = useTransform(y, waterInputProps, [150, 0]);
  const waterOpacity = useTransform(y, waterInputProps, [0, 1]);
  const waterFill = useTransform(
    waterAnimate,
    [150, 0],
    ['#ffffff', '#50665f']
  );

  const plantInputProps = [
    -scrollHeight - (clientHeight * 20) / 100,
    -scrollHeight - (clientHeight * 30) / 100,
  ];
  const plantOpacity = useTransform(y, plantInputProps, [0, 1]);

  const cannInputProps = [
    -scrollHeight - (clientHeight * 38) / 100,
    -scrollHeight - (clientHeight * 48) / 100,
  ];
  const cannOpacity = useTransform(y, cannInputProps, [0, 1]);

  const oilInputProps = [
    -scrollHeight - (clientHeight * 45) / 100,
    -scrollHeight - (clientHeight * 45.5) / 100,
  ];

  const oilOpacity = useTransform(y, oilInputProps, [0, 1]);
  const nugRotate = useSpring(useTransform(y, oilInputProps, [0, -180]));

  const glowInputProps = [
    -scrollHeight - (clientHeight * 45.5) / 100,
    -scrollHeight - (clientHeight * 46) / 100,
  ];

  const glowOpacity = useTransform(y, glowInputProps, [0, 1]);

  const scrollEvents = (nugHeight) => {
    let offsetY = document.documentElement.scrollTop;
    let waterStep = offsetY >= scrollHeight + (clientHeight * 1) / 100;
    let plantStep = offsetY >= scrollHeight + (clientHeight * 15) / 100;
    let cannStep = offsetY >= scrollHeight + (clientHeight * 30) / 100;
    let oilStep = offsetY >= scrollHeight + (clientHeight * 45) / 100;
    let nugEnd = window.innerWidth <= 767 ? 85 : 86;

    if (
      offsetY >= scrollHeight + clientHeight * (nugEnd / 100) &&
      glowEndAnimation.get() === 0
    ) {
      glowEndControls.start({ fill: '#50665f' });
      glowEndAnimation.set(1);
    } else if (
      offsetY < scrollHeight + clientHeight * (nugEnd / 100) &&
      glowEndAnimation.get() === 1
    ) {
      glowEndControls.start({ fill: '#ffb600' });
      glowEndAnimation.set(0);
    }
    if (waterStep && waterAnimation.get() === 0) {
      waterControls.start('fadeIn');
      waterAnimation.set(1);
    } else if (!waterStep && waterAnimation.get() !== 0) {
      waterControls.start('fadeOut');
      waterAnimation.set(0);
    } else if (plantStep && plantAnimation.get() === 0) {
      plantControls.start('fadeIn');
      plantAnimation.set(1);
    } else if (!plantStep && plantAnimation.get() !== 0) {
      plantControls.start('fadeOut');
      plantAnimation.set(0);
    } else if (cannStep && cannAnimation.get() === 0) {
      cannControls.start('fadeIn');
      cannAnimation.set(1);
    } else if (!cannStep && cannAnimation.get() !== 0) {
      cannControls.start('fadeOut');
      cannAnimation.set(0);
    } else if (oilStep && oilAnimation.get() === 0) {
      oilControls.start('fadeIn');
      glowControls.start({ opacity: 1, transition: { delay: 0.5 } });
      oilAnimation.set(1);
    } else if (!oilStep && oilAnimation.get() !== 0) {
      glowControls.start({ opacity: 0, transition: { duration: 0.1 } });
      oilControls.start('fadeOut');
      oilAnimation.set(0);
    }
  };

  useEffect(() => {
    let nugHeight = document
      .getElementById('nug')
      ?.getBoundingClientRect()?.height;
    if (window.innerWidth <= 768) {
      nugHeight = nugHeight - 100;
    }

    const unsubscribe = scrollY.onChange(() => {
      if (inView(scrollHeight, clientHeight, scrollY.get()))
        setTimeout(() => {
          requestAnimationFrame(() => scrollEvents(nugHeight));
        }, 10);
    });
    return () => unsubscribe();
  });

  return (
    <div
      id='nugSection'
      style={{
        position: 'relative',
        height: '420vh',
      }}>
      <Frame
        id='essentailFramer'
        top='0px'
        style={{
          ...frameStyle,
          zIndex: 2,
          position: 'sticky',
          height: '100vh',
        }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            height: '100%',
            overflowX: 'hidden',
            overflowY: 'hidden',
          }}>
          <Heading>
            <h2 className='scarcity-process'>
              THE Process Starts with Fresh flower
            </h2>
            {window.innerWidth <= 767 && (
              <div>
                <h3
                  className='aristotle-taught'
                  style={{ margin: '10px auto' }}>
                  Our Fresh Never Frozen™ Flower Captures The Spirit of
                  California
                </h3>
                {/* <h3 className='aristotle-taught' style={{ margin: '0 auto' }}>
                  Flower Captures The Spirit of California
                </h3> */}
              </div>
            )}
            {window.innerWidth > 767 && (
              <h3 className='aristotle-taught'>
                Our Fresh Never Frozen™ Flower Captures The Spirit of California
              </h3>
            )}
          </Heading>

          <NugSection>
            <NugWrapper>
              <NugIconWrapper>
                <NugIcon
                  id='nug'
                  waterOpacity={waterOpacity}
                  plantOpacity={plantOpacity}
                  cannOpacity={cannOpacity}
                  oilOpacity={oilOpacity}
                  waterFill={waterFill}
                  endContact={glowEndControls}
                  ref={checkRef}
                  className='nug'
                  style={{
                    rotate: nugRotate,
                    willChange: 'transform',
                    originX: '50%',
                    originY: '50%',
                  }}
                />
                <motion.div style={{ opacity: glowOpacity }}>
                  <NugBackgroundIcon className='nugBackground' />
                </motion.div>

                <motion.div
                  style={{ opacity: glowOpacity }}
                  className='glowBackground'
                />
              </NugIconWrapper>

              <StatsWrapper>
                <WebView>
                  <WaterStats>
                    <motion.div
                      initial='initial'
                      animate={waterControls}
                      variants={fadeInLeft}>
                      <h4>
                        80<span>%</span>
                      </h4>
                      <p>Water</p>
                    </motion.div>
                  </WaterStats>
                  <PlantStats>
                    <motion.div
                      initial='initial'
                      animate={plantControls}
                      variants={fadeInLeft}>
                      <h4>
                        15<span>%</span>
                      </h4>
                      <p>Plant Material</p>
                    </motion.div>
                  </PlantStats>

                  <Cannabinoids>
                    <motion.div
                      initial='initial'
                      animate={cannControls}
                      variants={fadeInLeft}>
                      <h4>
                        4.5<span>%</span>
                      </h4>
                      <p>Cannabinoids</p>
                    </motion.div>
                  </Cannabinoids>

                  <EssentialOil>
                    <motion.div
                      initial='initial'
                      animate={oilControls}
                      variants={fadeInLeft}>
                      <h4>
                        0.5<span>%</span>
                      </h4>
                      <p className='bold'>Essential Oil</p>
                    </motion.div>
                  </EssentialOil>
                </WebView>
                <MobileView>
                  <Cannabinoids
                    style={{
                      color: '#4A4A4A',
                    }}
                    initial='initial'
                    animate={cannControls}
                    variants={fadeInMobile}>
                    <h4>
                      4.5<span>%</span>
                    </h4>
                    <p>Cannabinoids</p>
                  </Cannabinoids>
                  <PlantStats
                    style={{
                      color: '#4A4A4A',
                    }}
                    initial='initial'
                    animate={plantControls}
                    variants={fadeInMobile}>
                    <h4>
                      15<span>%</span>
                    </h4>
                    <p>Plant Material</p>
                  </PlantStats>

                  <WaterStats
                    style={{
                      color: '#4A4A4A',
                    }}
                    initial='initial'
                    animate={waterControls}
                    variants={fadeInMobile}>
                    <h4>
                      80<span>%</span>
                    </h4>
                    <p>Water</p>
                  </WaterStats>
                </MobileView>
                <MobileView>
                  <EssentialOil>
                    <motion.div
                      initial='initial'
                      animate={oilControls}
                      variants={fadeInMobile}>
                      <h4>
                        0.5<span>%</span>
                      </h4>
                      <p className='bold'>Essential Oil</p>
                    </motion.div>
                  </EssentialOil>
                </MobileView>
              </StatsWrapper>
            </NugWrapper>
          </NugSection>
        </div>
      </Frame>
    </div>
  );
};

export default ScarcityIsReal;
