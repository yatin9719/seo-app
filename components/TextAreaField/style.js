import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: (props) => ({
    borderRadius: '4px',
    fontFamily: props?.fontFamily || 'inherit',
    width: props?.width,
    backgroundColor: props?.backgroundColor,
    height: 'auto !important',

    '& label': {
      fontFamily: props?.fontFamily || 'inherit',
      color: props?.labelColor,
      fontSize: props?.labelFontSize,
    },
    '& .MuiFormControl-root': {
      height: 'auto',
    },
    '& .MuiOutlinedInput-multiline': {
      padding: '10px 10px',
    },

    '& .MuiInputLabel-outlined': {
      color: props?.labelColor,
      transform: props?.labelTransform,
    },
    '& .MuiInputLabel-shrink': {
      fontFamily: 'robotobold',
      color: props.labelColorFocus,
      fontSize: props?.labelFontSizeFocus,
      transform: props?.labelTransformFocus,
    },
    '& .MuiOutlinedInput-notchedOutline': {
      top: '0px',
      height: '100%',
      '& legend': {
        display: 'none',
      },
    },

    '& .MuiInput-underline': {
      fontFamily: 'inherit',
      '& input': {
        color: props.color,
        paddingBottom: 15,
      },
      '&::before': {
        borderColor: props.borderColor,
        borderWidth: props.borderWidth,
        boxShadow: props.boxShadow,
      },
    },
    '& .MuiOutlinedInput-root': {
      fontFamily: 'inherit',
      height: '100%',
      '& input': {
        color: props.color,
      },

      '&:hover fieldset': {
        borderColor: props.borderColor,
      },
      '&.Mui-focused fieldset': {
        borderColor: props.borderColor,
      },
    },
  }),
});

export default useStyles;
