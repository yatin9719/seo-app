import * as a from 'redux/actions/loader';

const initial = false;

const loader = (state = initial, action = null) => {
  switch (action.type) {
    case a.INITIALIZE_LOADER:
      return initial;
    case a.TOGGLE_LOADER:
      return action.payload;
    default:
      return state;
  }
};

export default loader;
