import styled from 'styled-components';
const Container = styled.div``;
const Wrapper = styled.div`
  text-align: center;
  /* margin-top: 133px; */
`;
const Note = styled.div`
  position: relative;
  width: clamp(300px, 44vh, 367px);
  /* max-height: 157px; */
  margin: 0 auto;
  border: 2px solid #4a4a4a;
  padding: 15px 20px;
  border-radius: 14px;
  border-left-width: 4px;
  border-bottom-width: 4px;
  margin-bottom: 5px;
  background-color: #ffffff;
  bottom: 5px;
  z-index: 3;
  margin-right: 10px;
  :before {
    content: ' ';
    position: absolute;
    width: 30px;
    height: 24px;
    left: auto;
    right: 82px;
    bottom: -24.5px;
    background: linear-gradient(45deg, transparent 50%, #4a4a4a 50%);
  }
  :after {
    content: ' ';
    position: absolute;
    width: 26px;
    height: 23px;
    left: auto;
    right: 83.5px;
    bottom: -19px;
    /* border: 12px solid; */
    /* border-color: #fff #fff transparent transparent; */
    background: linear-gradient(45deg, transparent 50%, #fff 50%);
  }
  @media (min-height: 640px) and (max-height: 1024px) {
    /* padding: 15px; */
    text-align: left;
  }

  > p {
    /* overflow: hidden; */
    font-size: clamp(12px, 1.7vh, 15px);
    height: '100%';
    font-family: 'robotoregular';
    /* border-right: 0.15em solid green; The typwriter cursor */
    margin: 0 auto; /* Gives that scrolling effect as the typing happens */
    word-break: break-word;
    white-space: normal;
    /* animation: typing 1.5s steps(30, end), blink-caret 0.75s step-end; */

    /* @keyframes typing {
      from {
        width: 0;
      }
      to {
        width: 100%;
      }
    }

    @keyframes blink-caret {
      from,
      to {
        border-color: transparent;
      }
      50% {
        border-color: green;
      }
    } */
  }
`;
const SvgWrapper = styled.div`
  margin: 0 23px auto auto;
  position: relative;
  z-index: 2;
  display: flex;
  justify-content: flex-end;
  bottom: 3px;
  /* z-index: 0; */
  img.chatIcon {
    position: absolute;
    top: -8px;
    right: 68px;
    width: 34px;
    height: auto;
  }
  /* ::before {
    content: '';
    display: block;
    height: 14px;
    width: 18px;
    background-color: transparent;
    border-left: 2px solid;
    position: absolute;
    right: -12px;
    top: -18px;
    border-top: 4px solid #fff;
  }

  ::after {
    content: '';
    display: block;
    height: 26px;
    width: 10px;
    background-color: transparent;
    border-right: 2px solid;
    position: absolute;
    right: -6px;
    top: -26px;
    transform: rotate(52deg);
  } */
`;
export { Container, Wrapper, Note, SvgWrapper };
