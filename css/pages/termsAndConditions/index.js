import styled from 'styled-components';
import { motion } from 'framer';

const Container = styled(motion.div)`
  width: 100%;
`;

const BodyContainer = styled.div`
  padding: 40px 40px;
  @media (max-width: 964px) {
    padding: 40px 30px;
  }
`;

const BodyWrapper = styled.div`
  max-width: 900px;
  padding: 30px;
  margin: 0 auto;
  text-align: center;

  @media (max-width: 964px) {
    padding: 30px 0;
  }

  > p {
    font-family: 'robotoregular';
    text-align: center;
    line-height: 1.75rem;
    font-size: 16px;
    letter-spacing: 0.05rem;
  }
  > span {
    font-family: 'robotobold';
    text-align: center;
    letter-spacing: 0.05rem;
  }
`;

const TermsWrapper = styled.div`
  > h1 {
    text-align: center;
    text-transform: uppercase;
    margin-top: 40px;
    margin-bottom: 50px;
  }

  > ol {
    text-align: center;
    margin: 0 0 20px 40px;
    > li {
      text-align: left;
      font-family: robotoregular;
      margin-bottom: 0;
      line-height: 1.75rem;
      letter-spacing: 0.03rem;
      > a {
        position: relative;
        border-bottom: 2px solid rgb(75, 75, 75, 0.2);
        text-decoration: none;
        color: #4a4a4a;
        cursor: pointer;
        ::after {
          content: '';
          position: absolute;
          bottom: -2px;
          left: 0px;
          width: 0%;
          border-bottom: 2px solid #4a4a4a;
          transition: width 0.5s ease;
        }
        :hover::after {
          width: 100%;
        }
      }
    }
  }
`;

const DocumentsWrapper = styled.div`
  > h1 {
    text-align: center;
    text-transform: uppercase;
    margin-bottom: 50px;
  }

  > a {
    font-family: 'robotobold';
    position: relative;
    border-bottom: 2px solid rgb(75, 75, 75, 0.2);
    color: #4a4a4a;
    text-decoration: none;
    cursor: pointer;
    ::after {
      content: '';
      position: absolute;
      bottom: -2px;
      left: 0px;
      width: 0%;
      border-bottom: 2px solid #4a4a4a;
      transition: width 0.5s ease;
    }
    :hover::after {
      width: 100%;
    }
  }
`;

export {
  Container,
  BodyContainer,
  BodyWrapper,
  DocumentsWrapper,
  TermsWrapper,
};
