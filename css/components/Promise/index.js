import styled from 'styled-components';

const Container = styled.div`
  padding-bottom: 55px;
  /* overflow-x: clip; */
  .button {
    position: relative;
    display: block;
    overflow: hidden;
    box-shadow: 0 0 0 0em rgba(247, 149, 48, 0),
      0em 0.05em 0.1em rgba(0, 0, 0, 0.2);
    transform: translate3d(0, 0, 0) scale(1);
  }

  .button::before,
  .button::after {
    position: absolute;
    content: '';
  }

  .button::before {
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    width: 400px;
    height: 400px;
    background-color: rgba(255, 255, 255, 0.1);
    border-radius: 100%;
    opacity: 1;
    transform: translate3d(0, 0, 0) scale(0);

    @media (max-width: 968px) {
      width: 100%;
      height: 300px;
    }
  }

  .button::after {
    transform: translate3d(0, 0, 0);
  }

  .button.is-animating {
    -webkit-animation: button-outer 3000ms infinite;
    animation: button-outer 3000ms infinite;
  }
  .button.is-animating::before {
    -webkit-animation: button-inner 3000ms infinite;
    animation: button-inner 3000ms infinite;
  }
  .button.is-animating::after {
    -webkit-animation: button-icon 3000ms infinite;
    animation: button-icon 3000ms infinite;
  }

  @keyframes button-outer {
    0% {
      transform: translate3d(0, 0, 0) scale(1);
      box-shadow: 0 0 0 0em rgba(247, 149, 48, 0),
        0em 0.05em 0.1em rgba(0, 0, 0, 0.3);
    }
    33% {
      transform: translate3d(0, 0, 0) scale(1.1);
      box-shadow: 0 0 0 0em rgba(247, 149, 48, 0.1),
        0em 0.05em 0.1em rgba(0, 0, 0, 0.7);
    }
    66% {
      transform: translate3d(0, 0, 0) scale(1);
      box-shadow: 0 0 0 1em rgba(247, 149, 48, 0),
        0em 0.05em 0.1em rgba(0, 0, 0, 0.3);
    }
    100% {
      transform: translate3d(0, 0, 0) scale(1);
      box-shadow: 0 0 0 0em rgba(247, 149, 48, 0),
        0em 0.05em 0.1em rgba(0, 0, 0, 0.3);
    }
  }

  @keyframes button-inner {
    0% {
      opacity: 1;
      transform: translate3d(0, 0, 0) scale(0);
    }
    33% {
      opacity: 1;
      transform: translate3d(0, 0, 0) scale(0.9);
    }
    66% {
      opacity: 0;
      transform: translate3d(0, 0, 0) scale(0);
    }
    100% {
      opacity: 0;
      transform: translate3d(0, 0, 0) scale(0);
    }
  }

  @keyframes button-icon {
    0% {
      transform: translate3d(0em, 0, 0);
    }
    2% {
      transform: translate3d(0.01em, 0, 0);
    }
    4% {
      transform: translate3d(-0.01em, 0, 0);
    }
    6% {
      transform: translate3d(0.01em, 0, 0);
    }
    8% {
      transform: translate3d(-0.01em, 0, 0);
    }
    10% {
      transform: translate3d(0.01em, 0, 0);
    }
    12% {
      transform: translate3d(-0.01em, 0, 0);
    }
    14% {
      transform: translate3d(0.01em, 0, 0);
    }
    16% {
      transform: translate3d(-0.01em, 0, 0);
    }
    18% {
      transform: translate3d(0.01em, 0, 0);
    }
    20% {
      transform: translate3d(-0.01em, 0, 0);
    }
    22% {
      transform: translate3d(0.01em, 0, 0);
    }
    24% {
      transform: translate3d(-0.01em, 0, 0);
    }
    26% {
      transform: translate3d(0.01em, 0, 0);
    }
    28% {
      transform: translate3d(-0.01em, 0, 0);
    }
    30% {
      transform: translate3d(0.01em, 0, 0);
    }
    32% {
      transform: translate3d(-0.01em, 0, 0);
    }
    34% {
      transform: translate3d(0.01em, 0, 0);
    }
    36% {
      transform: translate3d(-0.01em, 0, 0);
    }
    38% {
      transform: translate3d(0.01em, 0, 0);
    }
    40% {
      transform: translate3d(-0.01em, 0, 0);
    }
    42% {
      transform: translate3d(0.01em, 0, 0);
    }
    44% {
      transform: translate3d(-0.01em, 0, 0);
    }
    46% {
      transform: translate3d(0em, 0, 0);
    }
  }
`;
const Heading = styled.div`
  text-align: center;
  padding-top: 200px;
  position: relative;
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    /* max-width: 74%; */
    line-height: 30px;
    margin: 0 auto;
    span {
      color: #58a08f;
    }
  }
  h3 {
    font-size: 30px;
    font-family: 'robotobold';
    margin: 0 auto;
    margin-top: 15px;
  }
  @media (max-width: 767px) {
    padding-top: 100px;
    h3 {
      font-size: 14pt;
      max-width: 182px;
      line-height: 24px;
      margin-top: 5px;
    }
    h2 {
      font-size: 16pt;
      line-height: 16pt;
      max-width: 257px;
    }
  }
  @media (max-width: 320px) {
    h3 {
      font-size: 9pt;
      max-width: 278px;
      line-height: 24px;
    }
    h2 {
      font-size: 11pt;
      line-height: 16pt;
      max-width: 257px;
    }
  }
`;

const IconSection = styled.div`
  display: flex;
  justify-content: center;
  margin: 50px 0;
  flex-wrap: wrap;
  align-items: center;
  position: relative;
  > svg {
    margin: 15px;
  }
`;
const DesktopView = styled.div`
  max-width: 600px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(121px, 1fr));
  padding-top: 65px;
  > svg {
    margin: 12.5px auto;
  }
  @media (max-width: 500px) {
    display: none;
  }
`;
const MobileView = styled.div`
  max-width: 600px;
  display: none;
  flex-direction: column;
  align-items: center;
  svg {
    margin: 0 5px;
  }
  @media (max-width: 500px) {
    padding-top: 55px;
    display: flex;
  }
`;
const Button = styled.a`
  background: #f79530;
  border-radius: 8px;
  font-size: 20px;
  text-transform: uppercase;
  text-decoration: none;
  max-width: 439px;
  width: 82%;
  margin: auto;
  padding: 17px 35px;
  color: #fff;
  margin-top: 37px;
  text-align: center;
  transition: all 0.3s ease-in-out 0s;
  cursor: pointer;
  outline: none;
  position: relative;
  border: 5px solid #f79530;
  box-shadow: 0 0 0 0em rgba(#3498db, 0), 0em 0.05em 0.1em rgba(#000000, 0.2);
  transform: translate3d(0, 0, 0) scale(1);
  display: block;
  :hover {
    color: #f79530;
    background-color: #ffffff;
  }
  @media (max-width: 767px) {
    max-width: 308px;
  }
`;
export { Container, Heading, IconSection, DesktopView, MobileView, Button };
