const PORTAL_ID = '8089446';
const SUBMIT_FORM = `https://api.hsforms.com/submissions/v3/integration/submit/${PORTAL_ID}`;
const FORM_DETAILS = `${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_API_PORT}/api/hubspot/getform`;

const WORLD_MAP_FORMID = '8f5b0755-7843-4fc7-92e6-f35cdf3561e3';
const SAMPLES_FORMID = 'dfb6aa70-4eb7-484e-8b93-38dfb5c09f0b';
const CONTACT_US_FORMID = '9affbc6c-8db0-444e-ab5f-1a8b2c0d169b';
const SHOP_FORMID = 'eb5cc04e-ff47-4675-9cd2-7cc0290823e0';

export {
  PORTAL_ID,
  SUBMIT_FORM,
  FORM_DETAILS,
  WORLD_MAP_FORMID,
  SAMPLES_FORMID,
  CONTACT_US_FORMID,
  SHOP_FORMID,
};
