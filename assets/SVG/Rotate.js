import * as React from 'react';

const Icon = (props) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    width={40 * 3}
    height={31.963 * 3}
    viewBox='0 0 40 31.963'
    {...props}>
    <g data-name='Group 3202'>
      <path
        data-name='Icon material-reply'
        d='M11.849 2.344V0l4.995 4.1-4.995 4.1V5.8C8.281 5.8 5.784 6.738 4 8.788c.714-2.929 2.854-5.858 7.849-6.444z'
        fill='#fefefe'
      />
      <g data-name='Group 3201'>
        <path
          data-name='Union 3'
          d='M1.928 31.708A1.934 1.934 0 010 29.781V12.428A1.934 1.934 0 011.928 10.5h26.993a1.934 1.934 0 011.928 1.928v17.353a1.934 1.934 0 01-1.928 1.928zm3.857-1.928h21.208V12.428H5.784z'
          fill='#404040'
        />
      </g>
      <g data-name='Group 3200'>
        <path
          data-name='Union 4'
          d='M20.719 31.963a1.934 1.934 0 01-1.928-1.927V3.043a1.934 1.934 0 011.928-1.928h17.353A1.934 1.934 0 0140 3.043v26.993a1.934 1.934 0 01-1.928 1.928zm0-5.784h17.353V4.971H20.719z'
          fill='#fefefe'
        />
      </g>
    </g>
  </svg>
);

export default Icon;
