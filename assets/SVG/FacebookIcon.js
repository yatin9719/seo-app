import * as React from 'react';

const Icon = () => (
  <svg width={60} height={60}>
    <title />
    <title>{'background'}</title>
    <path fill='none' d='M-1-1h582v402H-1z' />
    <g>
      <title>{'Layer 1'}</title>
      <path
        fill='#58a08f'
        d='M30 60c16.569 0 30-13.431 30-30C60 13.431 46.569 0 30 0 13.431 0 0 13.431 0 30c0 16.569 13.431 30 30 30z'
      />
      <path
        fill='#FFF'
        d='M25.462 47.314V30h-3.519v-5.74h3.52v-3.47c0-4.682 1.399-8.058 6.525-8.058h6.098v5.727h-4.294c-2.15 0-2.64 1.43-2.64 2.926v2.875h6.617L36.866 30h-5.714v17.315h-5.69z'
      />
    </g>
  </svg>
);

export default Icon;
