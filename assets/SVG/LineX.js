import * as React from 'react';
import { motion } from 'framer';

const Icon = ({ pathProps, pathLength, opacity, ...props }) => (
  <svg
    height={27 * 4.8}
    viewBox='0 0 3 27'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}>
    <defs>
      <mask id='mask-2'>
        <path
          stroke='#ffffff'
          strokeWidth={1}
          strokeMiterlimit={12}
          strokeLinecap='round'
          strokeLinejoin='round'
          strokeDasharray='2.5 2.5'
          d='M1.5 1.741v23.34'
        />
      </mask>
    </defs>
    <motion.path
      stroke='#58A08F'
      strokeWidth={1}
      strokeMiterlimit={12}
      strokeLinecap='round'
      strokeLinejoin='round'
      style={{ pathLength: pathLength, opacity: opacity }}
      d='M1.5 1.741v23.34'
      mask='url(#mask-2)'
    />
  </svg>
);

export default Icon;
