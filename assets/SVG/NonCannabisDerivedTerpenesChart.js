import * as React from 'react';
import { motion } from 'framer';

const Icon = ({ opacity, controls }) => {
  const variants = {
    initial: { pathLength: 0, opacity: 0 },
    animate: { pathLength: 1, opacity: 1 },
    opacity: { opacity: 0.5 },
    hide: { opacity: 0, transition: { delay: 2.5, duration: 0.1 } },
  };

  return (
    <motion.svg
      xmlns='http://www.w3.org/2000/svg'
      viewBox='0 0 422.3 115.2'
      width={460 * 3}
      className='NonCannabisDerivedTerpenesChart'>
      <motion.path
        d='M.2 104.6h21.9s.7-19.3.7-19.1 1.9 19.1 1.9 19.1h15L41 80.2l1.1 24.3h2.7L46 62.8l1.1 41.7h16.4L65 69.6s1 35.1 1 34.9h28.7l2-73.1 1.7 73.1H287l2.5-44.7 1.1 44.7h15.6l1.6-14.3 1.1 14.3h112.9'
        fill='none'
        stroke='#d72700'
        strokeMiterlimit={10}
        initial='initial'
        variants={variants}
        animate={controls}
        transition={{ duration: 2 }}
      />
    </motion.svg>
  );
};

export default Icon;
