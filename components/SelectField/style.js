import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: (props) => ({
    borderRadius: '4px',
    fontFamily: 'inherit',
    width: props?.width,
    backgroundColor: props?.backgroundColor,
    '& label': {
      fontFamily: `inherit`,
      color: props?.labelColor,
      fontSize: props?.labelFontSize,
    },
    '& .MuiOutlinedInput-input': {
      padding: props?.padding,
    },
    '& .MuiInputLabel-outlined': {
      color: props?.labelColor,
      transform: props?.labelTransform,
    },
    '& .MuiInputLabel-shrink': {
      fontFamily: 'robotobold',
      color: props.labelColorFocus,
      fontSize: props?.labelFontSizeFocus,
      transform: props?.labelTransformFocus,
    },
    '& .MuiOutlinedInput-notchedOutline': {
      top: '0px',
      height: '100%',
      '& legend': {
        display: 'none',
      },
    },

    '& .MuiInput-underline': {
      fontFamily: 'inherit',
      '& input': {
        color: props.color,
        paddingBottom: 15,
      },
      '&::before': {
        borderColor: props.borderColor,
        borderWidth: props.borderWidth,
        boxShadow: props.boxShadow,
      },
    },
    '& .MuiOutlinedInput-root': {
      fontFamily: 'inherit',
      height: '100%',
      '& input': {
        color: props.color,
        padding: props.padding,
      },

      '&:hover fieldset': {
        borderColor: props.borderColor,
      },
      '&.Mui-focused fieldset': {
        borderColor: props.borderColor,
      },
      '&.MuiPopover-paper': {
        backgroundColor: props.popoverBackground,
      },
    },
  }),
});

export default useStyles;
