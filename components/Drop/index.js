import React, { useState, useEffect } from 'react';
import DropIcon from 'assets/SVG/DropIcon';
import {
  motion,
  useTransform,
  useMotionValue,
  useViewportScroll,
  useAnimation,
} from 'framer';
import styled from 'styled-components';
import { getPercentage, ifiPhone } from 'utils';

const Drop = ({
  dropSize,
  dropFlip,
  scrollbar,
  collision,
  controls,
  ...props
}) => {
  const { y } = props;
  const { scrollY } = useViewportScroll();
  const { nug, spiritExit, process, drop, science } = collision;
  const nugCollision = useMotionValue(0);
  const dropXControls = useAnimation();
  const dropShiftAnimation = useMotionValue(0);
  const dropExitAnimation = useMotionValue(0);
  const innerHeight = window.innerHeight;
  const [isMounted, setIsMounted] = useState(false);
  const [dropHeight, setDropHeight] = useState({
    farmerHeight: drop?.farmerHeight,
    manufactureHeight: drop?.manufactureHeight,
    refiningHeight: drop?.refiningHeight,
    thcHeight: drop?.thcHeight,
    spiritHeight: undefined,
  });

  const dropShift = useTransform(
    scrollY,
    [
      process?.scrollHeight,
      process?.scrollHeight + 80,
      process?.scrollHeight + process?.clientHeight * (59 / 100),
      process?.scrollHeight + process?.clientHeight * (63 / 100),
    ],
    [
      'calc(-50% + 0px)',
      'calc(-50% + 59px)',
      'calc(-50% + 59px)',
      'calc(-50% + 0px)',
    ]
  );

  const configureDropHeight = () => {
    let processSection = document
      .getElementById('process_section')
      .getBoundingClientRect().top;
    let labSection = document
      .getElementById('labSection')
      .getBoundingClientRect().top;
    let nugSection = document
      .getElementById('nugSection')
      .getBoundingClientRect().top;
    let nug = document.getElementById('nug').getBoundingClientRect().top;
    let nugHeight = document.getElementById('nug').getBoundingClientRect()
      .height;
    let spirit = document.getElementById('spirit').getBoundingClientRect().top;
    let spiritHeight = document.getElementById('spirit').getBoundingClientRect()
      .height;
    let farmer = document.getElementById('sunIcon').getBoundingClientRect().top;
    let manufacture = document
      .getElementById('manufacturingPlant')
      .getBoundingClientRect().top;
    let refining = document
      .getElementById('refineryBuilding')
      .getBoundingClientRect().top;
    let thc = document.getElementById('lowThcContainer').getBoundingClientRect()
      .top;
    nugCollision.set(1);
    setDropHeight({
      ...dropHeight,
      farmerHeight: farmer - processSection,
      refiningHeight: refining - processSection,
      manufactureHeight: manufacture - processSection,
      thcHeight: thc - labSection,
      spiritHeight: spirit,
      spiritClientHeight: spiritHeight,
      nugHeight: nug - nugSection,
      nugClientHeight: nugHeight,
    });
  };

  const dropScale = useTransform(
    y,
    [
      -nug?.scrollHeight + (nug?.clientHeight * 15) / 100,
      -nug?.scrollHeight + (nug?.clientHeight * 10) / 100,
      -process?.scrollHeight - getPercentage(3000, 2000),
      -process?.scrollHeight - getPercentage(3000, 2200),
      -science?.scrollHeight + science?.clientHeight * (15 / 100),
    ],
    [1, 0.6, 0.4, 0.5, 0.5]
  );

  const dropColor = useTransform(
    y,
    [
      innerHeight <= 731
        ? -process?.scrollHeight - getPercentage(3000, 2000)
        : -process?.scrollHeight - getPercentage(3000, 2000),
      -process?.scrollHeight - getPercentage(3000, 2100),
    ],
    ['#f9c014', '#65dfff']
  );

  const dropTopInput = [
    0,
    -spiritExit?.scrollHeight - spiritExit?.clientHeight,
    window.innerWidth <= 968
      ? -nug?.scrollHeight + (nug?.clientHeight * 4) / 100
      : -nug?.scrollHeight - (nug?.clientHeight * 9) / 100,
    -nug?.scrollHeight - (nug?.clientHeight * 40) / 100,
    -nug?.scrollHeight - (nug?.clientHeight * 75) / 100,

    -nug?.scrollHeight - nug?.clientHeight + (nug?.clientHeight * 30) / 100,
    -process?.scrollHeight - (process?.clientHeight * 12.5) / 100,
    -process?.scrollHeight - (process?.clientHeight * 25.5) / 100,
    -process?.scrollHeight - (process?.clientHeight * 37.5) / 100,
    window.innerWidth <= 968
      ? -process?.scrollHeight - (process?.clientHeight * 44.5) / 100
      : -process?.scrollHeight - (process?.clientHeight * 63.5) / 100,
  ];

  const dropTopOutput = [
    dropHeight?.spiritHeight + dropHeight?.spiritClientHeight / 2,
    dropHeight?.spiritHeight + dropHeight?.spiritClientHeight / 2,
    dropHeight?.nugHeight + dropHeight?.nugClientHeight / 2,
    dropHeight?.nugHeight + dropHeight?.nugClientHeight / 2,
    dropHeight?.nugHeight + dropHeight?.nugClientHeight / 2,
    window?.innerHeight <= 731 && window.innerWidth <= 768
      ? dropHeight?.farmerHeight - 15
      : dropHeight?.farmerHeight - 15,
    window?.innerHeight <= 731 && window.innerWidth <= 768
      ? dropHeight?.farmerHeight - 15
      : dropHeight?.farmerHeight - 15,
    window?.innerHeight <= 731 && window.innerWidth <= 768
      ? dropHeight?.manufactureHeight - 15
      : dropHeight?.manufactureHeight,
    window?.innerHeight <= 731 && window.innerWidth <= 768
      ? dropHeight?.manufactureHeight - 15
      : dropHeight?.manufactureHeight,
    window?.innerHeight <= 731 && window.innerWidth <= 768
      ? dropHeight?.refiningHeight + 10
      : dropHeight?.refiningHeight + 25,
  ];

  const dropTopAnimate = useTransform(y, dropTopInput, dropTopOutput);

  const dropOpacity = useTransform(
    scrollY,
    [
      0,
      innerHeight <= 667
        ? spiritExit?.scrollHeight + getPercentage(1550, 800)
        : spiritExit?.scrollHeight + getPercentage(1550, 499),
      innerHeight <= 667
        ? spiritExit?.scrollHeight + getPercentage(1550, 841)
        : spiritExit?.scrollHeight + getPercentage(1550, 500),
      nug?.scrollHeight + getPercentage(2800, 99),
      nug?.scrollHeight + getPercentage(2800, 100),
      nug?.scrollHeight + (nug?.clientHeight * 80) / 100,
      nug?.scrollHeight + (nug?.clientHeight * 80.1) / 100,
      science?.scrollHeight - science?.clientHeight * (30 / 100),
      science?.scrollHeight - science?.clientHeight * (22 / 100),
    ],
    [0, 0, 1, 1, 0, 0, 1, 1, 0]
  );

  useEffect(() => {
    let unsubscribe = false;
    if (spiritExit) {
      unsubscribe = scrollY.onChange(() => {
        let offsetY = scrollY.get();
        let dropCollision = offsetY >= process?.scrollHeight;
        let dropFallStep =
          offsetY >=
          process?.scrollHeight + process?.clientHeight * (67.5 / 100);
        if (offsetY >= spiritExit?.scrollHeight && nugCollision.get() !== 1) {
          configureDropHeight();
          nugCollision.set(1);
        } else if (
          offsetY < spiritExit?.scrollHeight &&
          nugCollision.get() !== 0
        ) {
          nugCollision.set(0);
        }

        if (dropCollision && dropShiftAnimation.get() === 0) {
          dropXControls.start('shift');
          dropShiftAnimation.set(1);
        } else if (!dropCollision && dropShiftAnimation.get() !== 0) {
          dropXControls.start('initial');
          dropShiftAnimation.set(0);
        } else if (dropFallStep && dropExitAnimation.get() === 0) {
          dropXControls.start('initial');
          dropExitAnimation.set(1);
        } else if (!dropFallStep && dropExitAnimation.get() !== 0) {
          dropXControls.start('shift');
          dropExitAnimation.set(0);
        }
      });
    }
    setIsMounted(true);
    return () => {
      if (unsubscribe) unsubscribe();
    };
  });

  const dropShiftAnimations =
    window.innerWidth <= 968
      ? {
          initial: 'initial',

          style: {
            top: dropTopAnimate,
            x: dropShift,
            y: ifiPhone || window.innerWidth <= 767 ? 0 : scrollY,
          },
        }
      : {
          style: {
            top: dropTopAnimate,
            x: '-50%',
            y: ifiPhone || window.innerWidth <= 767 ? 0 : scrollY,
          },
        };

  return (
    <>
      {isMounted && (
        <DropWrapper {...dropShiftAnimations}>
          <DropIcon
            id='dropIcon'
            style={{
              scale: dropScale,
              opacity: dropOpacity,
            }}
            animate={controls}
            fill={dropColor}
          />
        </DropWrapper>
      )}
    </>
  );
};

export default Drop;

const DropWrapper = styled(motion.div)`
  width: 35px;
  height: 0px;
  position: ${ifiPhone || window.innerWidth <= 767 ? 'fixed' : 'absolute'};
  left: 50%;
  right: 0;
  bottom: 0;
  z-index: 1;
`;
