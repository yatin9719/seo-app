import styled from 'styled-components';
import { motion } from 'framer';

const ButtonWrapper = styled.div`
  max-width: 662px;
  margin: 0 auto;
  width: 100%;
  padding: 0 42px;
  @media (max-width: 768px) {
    padding: 5vh 42px;
    @media (max-height: 667px) {
    padding: 2vh 42px;
  }
  }
  @media (max-width: 768px) and (-webkit-device-pixel-ratio: 3){
    padding: 5vh 42px; }
`;

const Switch = styled(motion.div)`
  width: 100%;
  height: 55px;
  background-color: #ffffff;
  display: flex;
  justify-content: ${(props) => (props?.isOn ? 'flex-start' : 'flex-end')};
  border-radius: 50px;
  padding: 5px;
  cursor: pointer;
  position: relative;
  color: #7d7d7d;
  letter-spacing: 0.015em;
`;

const Handle = styled(motion.div)`
  width: 50%;
  height: 45px;
  color: white;
  background-color: ${(props) => (props?.disabled ? 'gray' : '#58a08f')};
  border-radius: 40px;
`;
const TextSection = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  display: flex;
  align-items: center;
  > h3 {
    flex: 1;
    text-align: center;
    font-size: 17px;
    font-family: 'robotoregular';
  }
  @media (max-width: 768px) {
    > h3 {
      font-size: 15px;
      font-family: 'roboto_condensedregular';
    }
  }
`;
export { Switch, Handle, ButtonWrapper, TextSection };
