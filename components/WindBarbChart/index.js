import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import styled from 'styled-components';

const BottomLabelContainer = styled.div`
  height: 10px;
  width: 99%;
  border-right: 2px solid #9e9e9e;
  border-left: 2px solid #9e9e9e;
  border-bottom: 2px solid #9e9e9e;
  position: relative;
  text-align: center;
  margin-bottom: 43px;
  & > span {
    padding-top: 20px;
    display: block;
    font-family: 'robotobold';
    text-transform: uppercase;
    letter-spacing: 2px;
    font-size: 13px;
  }
`;

const WindBarbChart = ({ title, data }) => {
  const options = {
    title: {
      text: '',
    },
    xAxis: {
      lineWidth: 3,
      lineColor: '#58A08F',
      labels: { enabled: false },
    },
    yAxis: {
      visible: false,
    },
    legend: { enabled: false },
    chart: {
      style: { width: '100%' },
      backgroundColor: 'transparent',
      spacing: [10, 3, 10, 4],
    },
    credits: { enabled: false },
    series: data,
  };
  return (
    <>
      <HighchartsReact highcharts={Highcharts} options={options} />
      {title && (
        <BottomLabelContainer>
          <span>{title}</span>
        </BottomLabelContainer>
      )}
    </>
  );
};

export default WindBarbChart;
