import React, { useRef, useEffect } from 'react';
import { useTransform, useViewportScroll } from 'framer';
import EssentialOilSection from 'components/QuintEssentialOil/QuintEssentialOil';
import ScarcityIsReal from 'components/QuintEssentialOil/ScarcityIsReal';
import SupplyChain from 'components/SupplyChain';
import { Container } from 'css/components/QuintEssentialOil';
import ScienceVsCulture from 'components/ScienceVsCulture';
import ForBg from 'assets/Images/gradient.png';
import { inView } from 'utils';

const frameStyle = {
  backgroundColor: null,
  height: null,
  width: '100%',
};

const QuintEssentialOil = ({ y, ...props }) => {
  const containerRef = useRef(null);
  const { scrollY } = useViewportScroll();
  const backgroundPos = useTransform(
    y,
    [0, -props?.science?.scrollHeight - props?.science?.clientHeight],
    [0, -window.innerHeight * 7]
  );

  const scrollEvents = () => {
    if (containerRef?.current)
      containerRef.current.style.backgroundPositionY = `${backgroundPos.get()}px, ${
        backgroundPos.get() + window.innerHeight * 2
      }px, ${backgroundPos.get() + window.innerHeight * 6}px`;
  };

  useEffect(() => {
    const unsubscribe = y.onChange(() => {
      if (
        inView(
          0,
          props?.science?.scrollHeight + window.innerHeight,
          scrollY.get()
        )
      )
        setTimeout(() => {
          requestAnimationFrame(() => scrollEvents());
        }, 10);
    });
    return () => unsubscribe();
  });

  return (
    <Container ref={containerRef}>
      <EssentialOilSection
        frameStyle={frameStyle}
        collision={props?.spirit}
        y={y}
      />

      <ScarcityIsReal frameStyle={frameStyle} collision={props?.nug} y={y} />

      <SupplyChain collision={props?.process} y={y} drop={props?.drop} />
      <ScienceVsCulture
        process={props?.process}
        science={props?.science}
        frameStyle={frameStyle}
        y={y}
      />
      <img
        src={ForBg}
        alt='bg'
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          height: '100%',
          width: '100%',
        }}
      />
    </Container>
  );
};

export default QuintEssentialOil;
