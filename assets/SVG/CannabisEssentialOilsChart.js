import * as React from 'react';
import { motion } from 'framer';

const stopVariants = {
  initial: {
    stopColor: 'rgb(255 255 255 / 0%)',
  },
  animate: (color) => {
    return {
      stopColor: ['rgb(255 255 255 / 0%)', color],
    };
  },
};

const pathVariants = {
  initial: { pathLength: 0, opacity: 1 },
  animate: { pathLength: 1, opacity: 1 },
  hide: { opacity: 0, transition: { delay: 2.5, duration: 0.1 } },
};

const circleVariants = {
  initial: { opacity: 0 },
  animate: { opacity: 1 },
};

const Icon = ({ controls }) => (
  <svg
    id='prefix__Layer_1'
    xmlns='http://www.w3.org/2000/svg'
    viewBox='0 0 422.3 115.2'
    xmlSpace='preserve'
    fill='none'
    className='CannabisEssentialOilsChart'>
    <style>
      {
        '.prefix__st1{fill:none;stroke:#6e9d90;stroke-miterlimit:10}.prefix__st2{fill:#6e9d90;enable-background:new}'
      }
    </style>
    <g id='prefix__Layer_5'>
      <linearGradient
        id='prefix__SVGID_1_'
        gradientUnits='userSpaceOnUse'
        x1={211.17}
        y1={3.48}
        x2={211.17}
        y2={107.35}
        gradientTransform='matrix(1 0 0 -1 0 118)'>
        <motion.stop
          offset={0}
          custom={'rgb(242 229 222 / 100%)'}
          initial='initial'
          animate={controls}
          variants={stopVariants}
          transition={{ duration: 1, delay: 1.5 }}
        />
        <motion.stop
          offset={0.09}
          custom={'rgb(201 208 201 / 100%)'}
          initial='initial'
          animate={controls}
          variants={stopVariants}
          transition={{ duration: 1, delay: 1.5 }}
        />
        <motion.stop
          offset={1}
          initial='initial'
          custom={'rgb(88 160 143 / 100%);'}
          animate={controls}
          variants={stopVariants}
          transition={{ duration: 1, delay: 1.5 }}
        />
      </linearGradient>

      <motion.path
        d='M.5 104.5h9.4c.4-.1.9-.1 1.3 0h7.6l1.2-11 1 11h1.3l.6-26.7 1.3 26.7h4.1l1-1.9 1.1 1.9h9.8l1-28.3 1.2 28.3c.9-.1 1.8-.1 2.6 0 .2.2 1.2-60.3 1.2-60.3l.7 60.4H52s1-8.2 1-8 1.2 8 1.2 8h.7l1-13.6 1.1 13.6h.8l1-14.1 1.2 14.1h1.8l.9-7.8 1 7.8 1.7-81.3 1.8 81.3h.7l.7-8.3.7 8.3h3.4l1.5-50.9.9 50.9h4.3l.6-12.4.8 12.4h3.3l1.2-2 1 2h8.3L97.1 1.9l1.6 102.6h1.7l1.9-24.9.9 24.9h1.2l.5-7 .7 7h4.1l.8-14 1.2 14h2.3l1-9.6 1.1 9.6h6.1l.8-7.2.6 7.2h1.2l.3-7 .3 7h2.4l.6-9 .3 9h1.8l.3-3.4.3 3.4h8.7l.8-7.5.7 7.5h4.6l1-7.4 1 7.4h2.9l.6-6.8.9 6.8h1.4l1.2-11.9 1.1 11.9h6l.8-6.8.9 6.8H182l.4-2 .4 2h25.9l.6-1.9.6 1.9H258l.6-8.2.5 8.2h.5l1-8.6.8 8.6h1.4l.4-8.2.5 8.2h3.8l.6-4 1 4h4l.5-9.3.4 9.3h5.5l1.1-8.2 1.2 8.2h3.2l1.1-8.4 1 8.4 2.6-66 1.1 66h2.6l.4-6.8.3 6.8h1.2l1.5-13.7 1.1 13.7.9-17.7 1.3 11.2.3 6.4 1.2-7 1.2 7h.8l1.3-8.1 1.2 8.1s.6.1.6 0 1.3-27.8 1.3-27.8l1 27.8h1.7l1.2-6.9 1.2 6.9h5.2l1.7-3.1 1.1 2.8h3.2l1.4-7.6 1.1 7.6h2.3l1.3-8 1.2 8h3.6l1.3-15.3 1.6 15.3h1.8l.8-6.8 1 6.8h.4l1.1-7.1.4 7.1h.3l1.5-6.8 1.2 6.8h4.2l1.4-3.2.8 3.4h.2l.4-3.2.8 3.2 1.2-5 1.3 5h6.5l.7-6.9.8 6.8.9-.1 1.2-7.2 1.2 7.2h9l.9-2.8.9 2.8h42.5'
        fill='url(#prefix__SVGID_1_)'
        stroke='#6e9d90'
        strokeLinecap='round'
        strokeLinejoin='round'
        initial='initial'
        animate={controls}
        variants={pathVariants}
        transition={{ duration: 2 }}
      />
      <motion.g
        initial='initial'
        animate={controls}
        variants={circleVariants}
        transition={{ duration: 1, delay: 1 }}>
        <motion.circle className='prefix__st2' cx={23.1} cy={77.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={20.2} cy={93.3} r={1.9} />
        <motion.circle className='prefix__st2' cx={29.5} cy={102.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={41.5} cy={76.3} r={1.9} />
        <motion.circle className='prefix__st2' cx={46.2} cy={43.9} r={1.9} />
        <motion.circle className='prefix__st2' cx={65.3} cy={22.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={74.2} cy={53.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={97.1} cy={1.9} r={1.9} />
        <motion.circle className='prefix__st2' cx={102.2} cy={79.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={104.9} cy={97.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={110.4} cy={90.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={115} cy={95.1} r={1.9} />
        <motion.circle className='prefix__st2' cx={122.9} cy={97.3} r={1.9} />
        <motion.circle className='prefix__st2' cx={125} cy={97.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={128.5} cy={95.6} r={1.9} />
        <motion.circle className='prefix__st2' cx={130.8} cy={101.1} r={1.9} />
        <motion.circle className='prefix__st2' cx={140.7} cy={97} r={1.9} />
        <motion.circle className='prefix__st2' cx={146.9} cy={97.3} r={1.9} />
        <motion.circle className='prefix__st2' cx={151.5} cy={97.9} r={1.9} />
        <motion.circle className='prefix__st2' cx={155.1} cy={92.6} r={1.9} />
        <motion.circle className='prefix__st2' cx={162.9} cy={97.9} r={1.9} />
        <motion.circle className='prefix__st2' cx={182.4} cy={102.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={209.3} cy={102.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={258.8} cy={96.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={260.7} cy={96.2} r={1.9} />
        <motion.circle className='prefix__st2' cx={263.3} cy={96.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={268.3} cy={100.9} r={1.9} />
        <motion.circle className='prefix__st2' cx={273.9} cy={95.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={280.8} cy={96.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={286.2} cy={96.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={293.9} cy={97.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={301.7} cy={97.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={304.9} cy={96.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={299} cy={87.2} r={1.9} />
        <motion.circle className='prefix__st2' cx={308.2} cy={76.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={290} cy={38.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={297.1} cy={91} r={1.9} />
        <motion.circle className='prefix__st2' cx={312} cy={97.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={320.3} cy={101.6} r={1.9} />
        <motion.circle className='prefix__st2' cx={325.9} cy={96.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={330.5} cy={96.5} r={1.9} />
        <motion.circle className='prefix__st2' cx={336.6} cy={89.1} r={1.9} />
        <motion.circle className='prefix__st2' cx={341} cy={97.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={343.5} cy={96.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={345.7} cy={97.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={352.5} cy={100.9} r={1.9} />
        <motion.circle className='prefix__st2' cx={354.1} cy={101.2} r={1.9} />
        <motion.circle className='prefix__st2' cx={356} cy={99.7} r={1.9} />
        <motion.circle className='prefix__st2' cx={364.6} cy={97.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={367.4} cy={97} r={1.9} />
        <motion.circle className='prefix__st2' cx={378.5} cy={101.6} r={1.9} />
        <motion.circle className='prefix__st2' cx={85.3} cy={102.8} r={1.9} />
        <motion.circle className='prefix__st2' cx={80.1} cy={92.3} r={1.9} />
        <motion.circle className='prefix__st2' cx={68.6} cy={96.1} r={1.9} />
        <motion.circle className='prefix__st2' cx={62.7} cy={96.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={58.9} cy={90.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={55.8} cy={90.4} r={1.9} />
        <motion.circle className='prefix__st2' cx={53} cy={96.1} r={1.9} />
      </motion.g>
    </g>
  </svg>
);

export default Icon;
