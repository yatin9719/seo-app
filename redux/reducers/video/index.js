import * as a from 'redux/actions/video';

const initial = false;

const video = (state = initial, action = null) => {
  switch (action.type) {
    case a.INITIALIZE_VIDEO_PLAY:
      return initial;
    case a.SET_VIDEO_PLAY:
      return action.payload;
    default:
      return state;
  }
};

export default video;
