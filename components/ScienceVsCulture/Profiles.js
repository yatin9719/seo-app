import React from 'react';
import { motion } from 'framer';
import { IconSection } from 'css/components/ScienceVsCulture';

const ProfileSection = ({
  animate,
  variants,
  filterData,
  profiles,
  profileControls,
  profileVariants,
  active,
  activeProducts,
  allData,
}) => {
  return (
    <IconSection
      initial='initial'
      animate={animate}
      variants={variants}
      className={window.innerWidth <= 767 ? 'moreProfile' : null}>
      {Object.keys(profiles)?.map((profile, index) => {
        let Icon = profiles[profile]?.Icon;

        if (allData?.[profile])
          return (
            <motion.div
              key={`profile-icon-${index}`}
              animate={profileControls}
              custom={{
                delay: index,
                opacity: active
                  ? active === profile
                    ? 1
                    : 0.3
                  : activeProducts?.includes(profile)
                  ? 1
                  : 0.3,
              }}
              variants={profileVariants}>
              <Icon
                // height='80px'
                color={profiles?.[profile]?.color}
                onClick={() => filterData(profile)}
              />
              <p
                style={{ color: profiles?.[profile]?.color }}
                onClick={() => filterData(profile)}>
                {profile}
              </p>
            </motion.div>
          );
        else return null;
      })}
    </IconSection>
  );
};

export default ProfileSection;
