import * as a from 'redux/actions/section';

const initial = true;

const section = (state = initial, action = null) => {
  switch (action.type) {
    case a.INITIALIZE_SECTION:
      return initial;
    case a.SET_SECTION:
      return action.payload;
    default:
      return state;
  }
};

export default section;
