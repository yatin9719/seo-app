import * as React from 'react';

const Icon = () => (
  <svg width={229} height={61} viewBox='0 0 229 61' fill='none'>
    <path
      d='M1.51 58.192V1.628h226.222v57.673'
      stroke='#58A08F'
      strokeWidth={2}
      strokeLinecap='round'
      strokeLinejoin='bevel'
      strokeDasharray='3 3'
    />
  </svg>
);

export default Icon;
