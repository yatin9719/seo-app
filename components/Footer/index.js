import React, { useState } from 'react';
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import FbIcon from 'assets/Images/fb.svg';
import InstagramIcon from 'assets/Images/insta.svg';
import LinkDin from 'assets/Images/ld.svg';
import {
  Container,
  Wrapper,
  WebView,
  MobileView,
  InfoSection,
  ReadMoreAccordion,
} from 'css/components/Footer';

const Footer = (props) => {
  const [readMore, setReadMore] = useState(false);

  return (
    <Container id='footer'>
      <Wrapper>
        <InfoSection>
          <div>
            <a className='link' href='/terms-and-conditions'>
              Terms &amp; Conditions
            </a>
          </div>
          <div>
            <a className='link' href='/compliance'>
              Compliance
            </a>
          </div>
          <div>
            <a href='mailto:Hi@TerpeneBeltFarms.com'>Hi@TerpeneBeltFarms.com</a>
          </div>
          <div>
            <a href='tel:1(855)275-3336'>1 (855) 275-3336</a>
          </div>
          <div className='socialLink'>
            <a
              href='https://www.facebook.com/terpenebeltfarms'
              target='_blank'
              rel='noreferrer'
              className='link'>
              <img src={FbIcon} alt='fbIcon' width='25px' />
            </a>
            <a
              href='https://www.instagram.com/terpenebeltfarms'
              target='_blank'
              rel='noreferrer'
              className='link'>
              <img src={InstagramIcon} alt='instaIcon' width='25px' />
            </a>
            <a
              href='https://www.linkedin.com/company/terpenebeltfarms'
              target='_blank'
              rel='noreferrer'
              className='link'>
              <img src={LinkDin} alt='LinkDin' width='25px' />
            </a>
          </div>
        </InfoSection>
        <div>
          <h5>
            <sup>+</sup> LAWFULLY GROWN UNDER THE 2018 FARM BILL AS HEMP
            <MobileView>
              <ReadMoreAccordion className='readMoreAccordion'>
                <Accordion
                  allowZeroExpanded
                  onChange={() => {
                    setReadMore(!readMore);
                    window.scrollTo({ top: document.body.scrollHeight });
                  }}>
                  <AccordionItem>
                    <AccordionItemHeading>
                      <AccordionItemButton>
                        {!readMore ? 'Read More' : 'Read Less'}
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                      Terpene Belt Farms™ was created to make California-grown
                      Fresh Never Frozen™ Cannabis<sup>+</sup> Essential Oil
                      commercially available across the globe. Our California
                      Essential Oils capture the essence of the plant and carry
                      the terroir of the land. The spirit of CA Cannabis is
                      available for export.
                      <p className='term'>© 2021 TERPENE BELT FARMS</p>
                    </AccordionItemPanel>
                  </AccordionItem>
                </Accordion>
              </ReadMoreAccordion>
            </MobileView>
          </h5>
          <WebView>
            <p>
              Terpene Belt Farms™ was created to make California-grown Fresh
              Never Frozen™ Cannabis<sup>+</sup> Essential Oil commercially
              available across the globe. Our California Essential Oils capture
              the essence of the plant and carry the terroir of the land. The
              spirit of CA Cannabis is available for export.
            </p>
            <p className='term'>© 2021 TERPENE BELT FARMS</p>
          </WebView>
        </div>
      </Wrapper>
    </Container>
  );
};

export default Footer;
