import React, { useRef, useEffect, useState } from 'react';
import styled from 'styled-components';
import Video from 'assets/Video/ClearBeakerWithAdded.mp4';
import { SplitText } from 'components/SplitText';

const WaitOver = React.memo(({ collision, scrollbar, ...props }) => {
  const video = useRef(null);
  const [transparent, setTransparent] = useState(true);
  const [scrolled, setScrolled] = useState(false);
  const { y } = props;

  useEffect(() => {
    y.onChange(() => {
      let offsetY = Math.abs(y.get());
      if (
        offsetY > collision?.scrollHeight - 750 &&
        !scrolled &&
        offsetY < collision?.scrollHeight + collision?.clientHeight
      ) {
        try {
          if (video?.current && video?.current?.paused) {
            video?.current?.play();
          }
        } catch (e) {
          let error;
        }
        setTransparent(true);
        setScrolled(true);
      } else if (
        offsetY < collision?.scrollHeight - 750 &&
        scrolled &&
        offsetY < collision?.scrollHeight + collision?.clientHeight
      ) {
        try {
          if (video?.current && !video?.current?.paused) {
            video?.current?.pause();
            video.current.currentTime = 0;
          }
        } catch (e) {
          let error;
        }
        setTransparent(false);
        setScrolled(false);
      }
    });
  }, [collision, y, scrolled]);

  return (
    <Container id='waitisover_section'>
      <Wrapper>
        <VideoWrapper>
          <VideoBg
            ref={video}
            src={Video}
            type='video/mp4'
            loop
            muted='muted'
            preload='auto'
            playsInline
          />
        </VideoWrapper>
        <TextWrapper transparent={transparent}>
          <BannerText1>
            <SplitText> Cannabis</SplitText>
            <SplitText className='smallText1'>+</SplitText>
            <SplitText>Essential Oil</SplitText>
          </BannerText1>
          <BannerText1>
            <SplitText> That Powers Formulations Across State Lines</SplitText>
          </BannerText1>
          <BannerText3>
            <SplitText>Open New Markets, Build Your Brand Overnight</SplitText>
          </BannerText3>
        </TextWrapper>
      </Wrapper>
    </Container>
  );
});

export default WaitOver;
const Container = styled.div`
  z-index: 1;
`;
const Wrapper = styled.div`
  min-height: 650px;
  height: 105vh;
  position: relative;
`;
const TextWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transition: background 0.9s ease-in-out !important;
  /* background: ${(props) =>
    props.transparent ? '#00000087' : 'rgba(0, 0, 0, 1)'}; */
  background: rgba(0, 0, 0, 0.29);
  z-index: 99999;

  .smallText1 {
    font-size: clamp(12px, 3.5vw, 18px);
    position: relative;
    top: -20px;
    margin-right: 24px;
    line-height: normal;

    @media (max-width: 968px) {
      top: -9px;
    }
  }
`;
const VideoWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const VideoBg = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
  -o-object-fit: cover;
`;

const BannerText1 = styled.div`
  font-size: clamp(27px, 3.5vw, 50px);
  color: #fff;
  transition-delay: 9s;
  text-transform: uppercase;
  line-height: clamp(27px, 3.5vw, 47px);
  text-align: center;
  max-width: 1300px;
`;

const BannerText3 = styled.div`
  font-size: clamp(17px, 3.5vw, 19px);
  line-height: 21px;
  color: #fff;
  font-family: 'roboto_condensedregular';
  margin-top: 15px;
  max-width: 62%;
  text-align: center;
`;
