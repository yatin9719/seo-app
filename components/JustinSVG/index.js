import React from 'react';
import { motion } from 'framer';
import JustinSVGIcon from 'assets/SVG/JustinSVGIcon';
import { Container, Wrapper, Note, SvgWrapper } from 'css/components/JustinSVG';

const sentence = {
  hidden: { opacity: 1 },
  visible: { opacity: 1, transition: { delay: 0.5, staggerChildren: 0.008 } },
};

const letter = {
  hidden: { opacity: 0, y: 100 },
  visible: { opacity: 1, y: 0 },
};

const JustinSVG = ({ mid }) => {
  const non_cannabis =
    'The attempt to recreate a complex essential oil using only a handful of the largest traits may have worked in the past.';

  const essential_1 =
    'Check out the vast number of green peaks in the Jack cannabis-derived essential oil.The peaks are the “personality traits” of a cannabis strain. Each peak represents a unique compound in that strain. It’s the diversity and frequency of the small peaks (trace & unidentified terpenes) that truly make the difference.';

  return (
    <Container>
      <Wrapper>
        {!mid && (
          <Note>
            <motion.p variants={sentence} initial='hidden' animate='visible'>
              {non_cannabis.split(' ').map((char, index) => {
                return (
                  <motion.span key={char + '-' + index} variants={letter}>
                    {char + ' '}
                  </motion.span>
                );
              })}
            </motion.p>
          </Note>
        )}
        {mid && (
          <Note>
            <motion.p variants={sentence} initial='hidden' animate='visible'>
              {essential_1.split(' ').map((char, index) => {
                return (
                  <motion.span key={char + '-' + index} variants={letter}>
                    {char + ' '}
                  </motion.span>
                );
              })}
            </motion.p>
          </Note>
        )}

        <SvgWrapper>
          <JustinSVGIcon />
        </SvgWrapper>
      </Wrapper>
    </Container>
  );
};

export default JustinSVG;
