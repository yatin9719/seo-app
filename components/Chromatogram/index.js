import React, { useState, useEffect } from 'react';
import {
  Frame,
  motion,
  useViewportScroll,
  useAnimation,
  useMotionValue,
} from 'framer';

import JustinSVG from 'components/JustinSVG';
import CannabisEssentialOilsChart from 'assets/SVG/CannabisEssentialOilsChart';
import NonCannabisDerivedTerpenesChart from 'assets/SVG/NonCannabisDerivedTerpenesChart';
import {
  Container,
  Wrapper,
  Heading,
  GraphSection,
  GraphSectionWrapper,
  LabelSection,
  CannabisOil,
  NonCannabisOil,
  LabelWrapper,
  MobileWrapper,
  JustinWrapper,
} from 'components/Chromatogram/style';
import { inView } from 'utils';

const Chromatogram = ({ chart, ...props }) => {
  const { scrollHeight, clientHeight } = chart || 0;
  const [startSection, setStartSection] = useState(false);
  const [midSection, setMidSection] = useState(false);
  const nonChartToggleControls = useAnimation();
  const chartToggleControls = useAnimation();
  const nonChartControls = useAnimation();
  const chartControls = useAnimation();
  const startAnimation = useMotionValue(0);
  const midAnimation = useMotionValue(0);
  const showCannabisAnimation = useMotionValue(0);
  const showNonCannabisAnimation = useMotionValue(0);
  const { scrollY } = useViewportScroll();

  useEffect(() => {
    let unsubscribe = false;

    const scrollEvents = () => {
      let offsetY = scrollY.get();
      let startStep = offsetY >= scrollHeight;
      let midStep = offsetY > scrollHeight + clientHeight * (23.5 / 100);

      if (window.innerWidth > 768) {
        if (startStep && startAnimation.get() === 0) {
          nonChartControls.start(['animate']);
          nonChartToggleControls.start({
            background: 'linear-gradient(180deg,#ffbfb0 0%,#c72400 100%)',
          });
          setStartSection(true);
          startAnimation.set(1);
          showNonCannabisAnimation.set(1);
        } else if (!startStep && startAnimation.get() === 1) {
          nonChartControls.start(['initial', 'hide']);
          nonChartToggleControls.start({
            background: 'transparent',
          });
          setStartSection(false);
          startAnimation.set(0);
          showNonCannabisAnimation.set(0);
        } else if (midStep && midAnimation.get() === 0) {
          chartToggleControls.start({
            background: 'linear-gradient(180deg, #a6ded0 0%, #206857 100%)',
          });
          chartControls.start('animate');
          nonChartControls.start(['animate', 'opacity']);
          setMidSection(true);
          midAnimation.set(1);
          showCannabisAnimation.set(1);
        } else if (!midStep && midAnimation.get() === 1) {
          chartToggleControls.start({ background: 'transparent' });
          chartControls.start(['initial', 'hide']);
          nonChartControls.start('animate');
          setMidSection(false);
          midAnimation.set(0);
          showCannabisAnimation.set(0);
        }
      } else {
        if (startStep && startAnimation.get() === 0) {
          nonChartControls.start('animate');
          chartControls.start('animate');
          setStartSection(true);
          startAnimation.set(1);
        } else if (!startStep && startAnimation.get() === 1) {
          nonChartControls.start(['initial', 'hide']);
          chartControls.start(['initial', 'hide']);
          setStartSection(false);
          startAnimation.set(0);
        }
      }
    };

    if (chart) {
      unsubscribe = scrollY.onChange(() => {
        if (
          inView(scrollHeight - window.innerHeight, clientHeight, scrollY.get())
        )
          setTimeout(() => {
            requestAnimationFrame(() => scrollEvents());
          }, 10);
      });
    }
    return () => {
      if (unsubscribe) {
        unsubscribe();
      }
    };
  });

  const selectCannabisOil = () => {
    if (showCannabisAnimation.get() === 1) {
      chartToggleControls.start({ background: 'transparent' });
      chartControls.start(['initial', 'hide']);
      showCannabisAnimation.set(0);
    } else {
      chartToggleControls.start({
        background: 'linear-gradient(180deg,#a6ded0 0%,#206857 100%)',
      });
      chartControls.start('animate');
      showCannabisAnimation.set(1);
    }
  };

  const selectNonCannabisOil = () => {
    if (showNonCannabisAnimation.get() === 1) {
      nonChartToggleControls.start({ background: 'transparent' });
      nonChartControls.start(['initial', 'hide']);
      showNonCannabisAnimation.set(0);
    } else if (showNonCannabisAnimation.get() === 0) {
      nonChartToggleControls.start({
        background: 'linear-gradient(180deg,#ffbfb0 0%,#c72400 100%)',
      });
      nonChartControls.start('animate');
      showNonCannabisAnimation.set(1);
    }
  };
  return (
    <Container id='chromatogram_section'>
      {window.innerWidth > 768 && (
        <Frame
          id='webFrame'
          top='0px'
          style={{
            width: '100%',
            backgroundColor: null,
            position: 'sticky',
            height: '100vh',
          }}>
          <Wrapper className='webView'>
            <Heading>
              <div>
                <h2>
                  Cannabis<span className='plusIcon'>+</span> Essential Oil
                </h2>
                <small className='smallText'>VS</small>
                <h2>The Fake Stuff</h2>
              </div>
              <h3>
                The Science Behind Cannabis<span className='plusIcon'>+</span>{' '}
                Essential Oils vs Non-Cannabis Terpenes
              </h3>
            </Heading>

            <LabelSection>
              <NonCannabisOil disabled={!startSection}>
                <motion.div
                  initial={{ background: 'transparent' }}
                  animate={nonChartToggleControls}
                  onClick={startSection ? selectNonCannabisOil : null}
                />
                <span>Non-Cannabis Derived Terpenes</span>
              </NonCannabisOil>
              <CannabisOil disabled={!midSection}>
                <motion.div
                  initial={{ background: 'transparent' }}
                  animate={chartToggleControls}
                  onClick={midSection ? selectCannabisOil : null}
                />
                <span>
                  Terpene Belt Farms Cannabis<span className='plusIcon'>+</span>{' '}
                  Essential Oils
                </span>
              </CannabisOil>
            </LabelSection>

            <GraphSection>
              <GraphSectionWrapper>
                <CannabisEssentialOilsChart controls={chartControls} />

                <NonCannabisDerivedTerpenesChart
                  controls={nonChartControls}
                  opacity={midSection}
                />
                <LabelWrapper>
                  <div className='CannabisEssentialOilsChartLabel'>
                    <div>Monoterpenes</div>
                  </div>
                  <div className='blank'></div>
                  <div className='NonCannabisDerivedTerpenesChartLabel'>
                    <div>Sesquiterpenes</div>
                  </div>
                </LabelWrapper>
              </GraphSectionWrapper>
              {startSection && (
                <JustinWrapper
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}>
                  {window.innerWidth > 968 && <JustinSVG mid={midSection} />}
                </JustinWrapper>
              )}
            </GraphSection>
          </Wrapper>
        </Frame>
      )}
      <Frame
        id='webFrame'
        top='0px'
        style={{
          height: null,
          width: '100%',
          backgroundColor: null,
          position: 'sticky',
          minHeight: '100vh',
        }}>
        <Wrapper className='mobileView'>
          <Heading>
            <div>
              <h2>
                Cannabis<span className='plusIcon'>+</span> Essential Oil
              </h2>
              <small className='smallText'>VS THE</small>
              <h2>Fake Stuff</h2>
            </div>
            <h3>
              The Science Behind Cannabis<span className='plusIcon'>+</span>{' '}
              Essential Oils vs Non-Cannabis Terpenes
            </h3>
          </Heading>
          <MobileWrapper>
            <GraphSection>
              <GraphSectionWrapper>
                <CannabisEssentialOilsChart controls={chartControls} />

                <LabelWrapper>
                  <div className='CannabisEssentialOilsChartLabel'>
                    <div>Monoterpenes</div>
                  </div>
                  <div className='blank'></div>
                  <div className='NonCannabisDerivedTerpenesChartLabel'>
                    <div>Sesquiterpenes</div>
                  </div>
                </LabelWrapper>
              </GraphSectionWrapper>
            </GraphSection>
            <LabelSection>
              <CannabisOil>
                <div>
                  CANNABIS<span>+</span> ESSENTIAL OILS
                </div>
                <span>
                  Check out the vast number of green peaks in the Jack
                  cannabis-derived essential oil.The peaks are the “personality
                  traits” of a cannabis strain. Each peak represents a unique
                  compound in that strain. It’s the diversity and frequency of
                  the small peaks (trace &#38; unidentified terpenes) that truly
                  make the difference.
                </span>
              </CannabisOil>
            </LabelSection>
          </MobileWrapper>

          <MobileWrapper>
            <GraphSection>
              <GraphSectionWrapper>
                <NonCannabisDerivedTerpenesChart controls={nonChartControls} />

                <LabelWrapper>
                  <div className='CannabisEssentialOilsChartLabel'>
                    <div>Monoterpenes</div>
                  </div>
                  <div className='blank'></div>
                  <div className='NonCannabisDerivedTerpenesChartLabel'>
                    <div>Sesquiterpenes</div>
                  </div>
                </LabelWrapper>
              </GraphSectionWrapper>
            </GraphSection>
            <LabelSection>
              <NonCannabisOil>
                <div>NON-CANNABIS TERPENES</div>
                <span>
                  Here we see synthetic oil is copying a handful of the largest
                  traits in the real oil and artificially creating a look-alike
                  that is missing most of the personality traits of the real
                  cannabis.
                </span>
              </NonCannabisOil>
            </LabelSection>
          </MobileWrapper>
        </Wrapper>
      </Frame>
    </Container>
  );
};

export default Chromatogram;
