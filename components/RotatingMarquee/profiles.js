import CitrusIcon from 'assets/SVG/CitrusIcon';
import FruitIcon from 'assets/SVG/FruitIcon';
import GasIcon from 'assets/SVG/GasIcon';
import PineIcon from 'assets/SVG/PineIcon';
import SweetIcon from 'assets/SVG/SweetIcon';
import ExoticIcon from 'assets/SVG/ExoticIcon';
import SourIcon from 'assets/SVG/SourIcon';

const profiles = [
  {
    category: 'citrus',
    color: '#CF7519',
    name: 'Citrus',
    Icon: CitrusIcon,
    gradient:
      'linear-gradient(180deg, #f79530cc 0%, #f79530cc 0.01%, #ffeeddcc 100%)',
  },
  {
    category: 'exotic',
    color: '#64A997',
    name: 'Exotic',
    Icon: ExoticIcon,
    gradient:
      'linear-gradient(180deg,#64A997cc 0%,#64A997cc 0.01%,#8dc6b724 100%)',
  },
  {
    category: 'fruit',
    color: '#EF527D',
    name: 'Fruit',
    Icon: FruitIcon,
    gradient:
      'linear-gradient(180deg, #ef527dcc 0%, #ffdde6cc 100%), #c4c4c4cc',
  },
  {
    category: 'gas',
    color: '#A0744B',
    name: 'Gas',
    Icon: GasIcon,
    gradient:
      'linear-gradient(180deg, #bc9c7ecc 0%, #ffedddcc 100%), #c4c4c4cc',
  },
  {
    category: 'pine',
    color: '#94A753',
    name: 'Pine',
    Icon: PineIcon,
    gradient:
      'linear-gradient(180deg, #94a753cc 0%, #f8ffdfcc 100%), #c4c4c4cc',
  },
  {
    category: 'sour',
    color: '#D09626',
    name: 'Sour',
    Icon: SourIcon,
    gradient:
      'linear-gradient(180deg,#D09626cc 0%,#D09626cc 0.01%,rgb(255 197 137 / 38%) 100%),#c4c4c4cc',
  },
  {
    category: 'sweet',
    color: '#A85BA3',
    name: 'Sweet',
    Icon: SweetIcon,
    gradient:
      'linear-gradient(180deg,#a85ba3cc 0%,#ad62a8cc 0.01%,#ffddfdcc 100%),#c4c4c4cc',
  },
];

export default profiles;
