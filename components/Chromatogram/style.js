import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';
import { motion } from 'framer';

const Heading = styled(motion.div)`
  text-align: center;
  /* padding-top: 100px; */
  /* padding-top: 77px; */
  /* position: relative; */
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    max-width: 593px;
    margin: 0 auto;
    line-height: 24px;
  }
  .smallText {
    font-size: 17.5px;
  }
  span.plusIcon {
    line-height: 10px;
    font-size: 13px;
    position: relative;
    top: -11px;
  }
  h3 {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    width: 75%;
    margin: 0 auto;
    margin-top: 15px;
    font-weight: unset;
    span.plusIcon {
      line-height: 10px;
      font-size: 13px;
      position: relative;
      top: -3px;
    }
  }
  @media (max-width: 767px) {
    /* padding-top: 65px; */
    h2 {
      line-height: 16pt;
      font-size: 16pt;
      max-width: 324px;
      margin: 0 auto;
    }
    h3 {
      font-size: 12pt;
      width: 280px;
      padding-bottom: 30px;
      font-weight: unset;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -7.5px;
    }
    .smallText {
      font-size: 14.5px;
    }
  }
  @media (max-width: 320px) {
    padding-top: 61px;
    h2 {
      line-height: 26px;
      font-size: 11pt;
      max-width: 90%;
      margin: 0 auto;
    }
    h3 {
      font-size: 9pt;
      width: 301px;
      font-weight: unset;
      padding-bottom: 30px;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -7.5px;
    }
  }
`;
const GraphSection = styled.div`
  display: flex;
  margin-top: 60px;
  padding: 0 20px;
  max-width: 1380px;
  margin: 0 auto;
  align-items: flex-end;
  position: relative;
  width: 100%;
  .CannabisEssentialOilsChart {
    position: absolute;
    bottom: -25px;
    right: 0;
    left: 0;
    width: 100%;
    z-index: 0;
  }
  .NonCannabisDerivedTerpenesChart {
    position: absolute;
    bottom: -24px;
    left: 0px;
    right: 0;
    width: 100%;
    z-index: 1;
  }
  .CannabisEssentialOilsChartLabel {
    height: 15px;
    border: 2px solid #9e9e9e;
    border-top: none;
    width: 50.9%;
    position: relative;

    > div {
      position: absolute;
      right: 0;
      left: 0;
      bottom: -30px;
      font-family: 'robotobold';
      text-transform: uppercase;
      font-size: 12px;
      letter-spacing: 2px;
      color: #4a4a4a;
    }
  }
  .blank {
    width: 8.8%;
  }
  .NonCannabisDerivedTerpenesChartLabel {
    height: 15px;
    border: 2px solid #9e9e9e;
    border-top: none;
    width: 39.3%;
    position: relative;
    > div {
      position: absolute;
      right: 0;
      left: 0;
      bottom: -30px;
      font-family: 'robotobold';
      text-transform: uppercase;
      font-size: 12px;
      letter-spacing: 2px;
      color: #4a4a4a;
    }
  }
  /* > path.highcharts-points {
    fill: red !important;
  } */
  @media (max-width: 768px) {
    padding: 0;
    .CannabisEssentialOilsChart,
    .NonCannabisDerivedTerpenesChart {
      margin-top: 100px;
      left: -14px;
      width: 113%;
      bottom: -5px;
    }
    .CannabisEssentialOilsChartLabel {
      width: 53.9%;
    }
    .blank {
      width: 11.8%;
    }
    .NonCannabisDerivedTerpenesChartLabel {
      width: 34.3%;
    }
    .CannabisEssentialOilsChartLabel,
    .NonCannabisDerivedTerpenesChartLabel {
      height: 5px;
      > div {
        bottom: -14px;
        font-size: 8px;
      }
    }
  }
  @media (max-width: 640px) {
    padding: 0;
    .CannabisEssentialOilsChart,
    .NonCannabisDerivedTerpenesChart {
      margin-top: 30px;
      left: -14px;
      width: 113%;
    }
    .CannabisEssentialOilsChartLabel {
      width: 53.9%;
    }
    .blank {
      width: 11.8%;
    }
    .NonCannabisDerivedTerpenesChartLabel {
      width: 34.3%;
    }
    .CannabisEssentialOilsChartLabel,
    .NonCannabisDerivedTerpenesChartLabel {
      height: 5px;
      > div {
        bottom: -14px;
        font-size: 8px;
      }
    }
  }
`;
const LabelWrapper = styled.div`
  display: flex;
  grid-gap: 10px;
  position: absolute;
  width: 100%;
  bottom: -14px;
  text-align: center;
  @media (max-width: 768px) {
    grid-gap: 0;
    bottom: -6px;
    width: 98%;
    margin: 0 auto;
    right: 0;
    left: 0;
  }
`;
const GraphSectionWrapper = styled.div`
  /* flex: 1; */
  position: relative;
  height: 30vh;
  margin-top: 20px;
  margin-bottom: 60px;
  /* margin-right: 75px; */
  width: calc(100% - 48px);
  @media (max-width: 1024px) {
    margin-top: 0;
  }
  @media (max-width: 768px) {
    height: auto;
    margin-top: 0;
    margin-bottom: 10px;
    margin-right: 0;
    width: 100%;
  }
  .prefix__st2 {
    animation: fade-in 3s ease-in-out;
    opacity: 0.5;
  }

  @keyframes fade-in {
    0% {
      opacity: 0;
    }
    50% {
      opacity: 0;
    }
    100% {
      opacity: 0.5;
    }
  }

  .pulsatingPts {
    height: 4px;
    width: 4px;
    filter: drop-shadow(0rem 0rem 0rem #58a08f);
    animation: pulse-black 2s infinite;
    opacity: 1;
  }

  @keyframes pulse-black {
    0% {
      filter: drop-shadow(0rem 0rem 0.2rem #58a08f);
    }

    100% {
      filter: drop-shadow(0rem 0rem 0.5rem #58a08f),
        drop-shadow(0rem 0rem 0.5rem #58a08f),
        drop-shadow(0rem 0rem 0.5rem #58a08f),
        drop-shadow(0rem 0rem 0.5rem #58a08f);
    }
  }
`;

const LabelSection = styled.div`
  display: flex;
  justify-content: center;
  padding: 0 20px;
  flex-wrap: wrap;
  max-width: 1380px;
  margin: clamp(2vh, 3vh, 6vh) auto;
  width: 100%;
  /* > div {
    flex-direction: column;
    max-width: 270px;
    text-align: center;
    width: 100%;
     > span {
      margin-top: 15px;
     }
  } */
`;
const Wrapper = styled.div`
  /* max-width: 1396px;
  margin: 0 auto;
  padding: 0 20px; */
`;
const CannabisOil = styled.div`
  display: flex;
  align-items: center;
  font-size: 14px;
  flex-direction: column;
  text-align: center;
  span.plusIcon {
    line-height: 10px;
    font-size: 12px;
    position: relative;
    top: -3px;
  }
  > div {
    min-height: 40px;
    min-width: 40px;
    border: 3px solid #58a08f;
    box-sizing: border-box;
    border-radius: 5px;
    cursor: pointer;
    background: transparent;
    opacity: ${(props) => (props.disabled ? 0.6 : 1)};

    > span {
      font-size: 11px;
      position: relative;
      top: -4px;
    }
  }

  > span {
    font-family: 'robotobold';
    font-size: 15px;
    color: #58a08f;
    max-width: 200px;
    line-height: 18px;
    margin-top: 9px;
  }

  @media (max-width: 768px) {
    flex-direction: column;
    > div {
      border: none;
      color: #58a08f;
      margin: 0;
      cursor: default;
      min-height: 26px;
      font-size: 17px;
      /* margin-top: 24px; */
    }
    > span {
      font-family: 'robotoregular';
      padding: 0px;
      text-align: left;
      color: #525f5c;
      font-size: 14px;
      line-height: 23px;
      max-width: 100%;
      margin-bottom: 15px;
      margin: 0px 20px;
    }
  }
  @media (max-width: 320px) {
    flex-direction: column;
    > div {
      border: none;
      color: #58a08f;
      margin: 0;
      cursor: default;
      min-height: 26px;
      font-size: 15px;
      /* margin-top: 24px; */
    }
    > span {
      color: #525f5c;
      font-size: 14px;
      line-height: 23px;
      max-width: 100%;
      margin-bottom: 15px;
      margin: 0px 20px;
    }
  }
`;
const NonCannabisOil = styled(motion.div)`
  display: flex;
  align-items: center;
  font-size: 14px;
  flex-direction: column;
  /* justify-content: center; */
  text-align: center;

  > div {
    min-height: 40px;
    min-width: 40px;
    border: 3px solid #d72700;
    box-sizing: border-box;
    border-radius: 5px;
    cursor: pointer;
    opacity: ${(props) => (props.disabled ? 0.6 : 1)};
    background: transparent;
    /* background: ${(props) =>
      props.selected
        ? 'linear-gradient(180deg,#ffbfb0 0%,#c72400 100%)'
        : 'transparent'}; */
  }

  > span {
    font-family: 'robotobold';
    font-size: 15px;
    color: #d72700;
    max-width: 150px;
    line-height: 18px;
    margin-top: 9px;
  }

  @media (max-width: 768px) {
    flex-direction: column;
    > div {
      border: none;
      color: #d72700;
      margin: 0;
      cursor: default;
      min-height: 26px;
      font-size: 17px;
      /* margin-top: 24px; */
    }
    > span {
      font-family: 'robotoregular';
      padding: 0;
      text-align: left;
      color: #525f5c;
      font-size: 14px;
      line-height: 23px;
      margin-bottom: 15px;
      max-width: 100%;
      margin: 0px 20px;
    }
  }
`;
const Container = styled.div`
  /* overflow-x: hidden; */
  background: url(${LegalBg}),
    linear-gradient(180deg, #58a08f33 0%, #ffffff33 100%), #c4c4c433;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: top;
  height: ${window.innerWidth <= 768 ? '150vh' : '369vh'};
  position: relative;
  /* padding: 0px 0 80px 0; */
  /* min-height:100vh; */

  .mobileView {
    display: none;
    overflow-x: hidden;

    @media (max-width: 768px) {
      display: block;
      min-height: 100vh;
      padding: 60px 0 20px;
    }
    ${LabelWrapper} {
      padding: 0;
    }
  }

  .webFrame {
    @media (max-width: 768px) {
      display: none;
    }
  }

  .webView {
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    ${LabelSection} {
      > div {
        flex-direction: column;
        max-width: 270px;
        text-align: center;
        width: 100%;
        > span {
          margin-top: 1.7vh;
        }
      }
    }
    @media (max-width: 768px) {
      display: none;
    }
  }
`;

const MobileWrapper = styled.div`
  margin: 90px 0 40px 0;

  @media (max-width: 768px) {
    margin: 230px 0 0px 0;
  }
  @media (max-width: 640px) {
    margin: 120px 0 0px 0;
  }
`;

const Button = styled.div`
  font-size: clamp(16px, 2vh, 20px);
  background: #58a08f;
  border-radius: 7.5px;
  width: fit-content;
  margin: 0 auto;
  padding: 17px 45px;
  color: #fff;
  text-transform: uppercase;
  span.plusIcon {
    line-height: 10px;
    font-size: 12px;
    position: relative;
    top: -4px;
    @media (max-width: 923px) {
      top: -3.5px;
      font-size: 10px;
    }
  }

  @media (min-height: 640px) and (max-height: 1024px) {
    font-size: clamp(12px, 2vh, 20px);
    padding: 15px 45px;
  }
  @media (max-width: 923px) {
    font-size: 15px;
    font-family: 'roboto_condensedregular';
  }
  @media (max-width: 375px) and (max-height: 667px) {
    font-size: 13px;
    padding: 8px 30px;
  }
  @media (max-width: 320px) {
    font-size: 10px;
    padding: 8px 30px;
  }
`;

const ButtonWrapper = styled.div`
  flex: none !important;
  margin-top: clamp(50px, 20vh, 10px);
  @media (min-height: 640px) and (max-height: 1024px) {
    margin-top: 20px;
  }
`;

const PeaksWrapper = styled.div`
  position: relative;
  > div {
    position: absolute;

    :nth-child(2) {
      left: 314px;
      top: -28px;
    }

    :nth-child(4) {
      left: 314px;
      top: -28px;
    }
  }
`;

const Peaks = styled.div`
  height: 11px;
  width: 11px;
  background: #5aa190;
  border-radius: 50%;
`;
const JustinWrapper = styled(motion.div)`
  position: absolute;
  right: 0;
  bottom: 14.5%;
  @media (height: 768px) and (width: 1024px) {
    bottom: 46px;
  }
`;
export {
  Container,
  Heading,
  GraphSection,
  LabelSection,
  GraphSectionWrapper,
  Wrapper,
  CannabisOil,
  NonCannabisOil,
  LabelWrapper,
  Button,
  ButtonWrapper,
  MobileWrapper,
  PeaksWrapper,
  Peaks,
  JustinWrapper,
};
