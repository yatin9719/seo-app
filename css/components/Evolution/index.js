import styled from 'styled-components';
import LegalBg from 'assets/Images/LegalBg.png';
import { motion } from 'framer';
const Container = styled.div`
  position: relative;
  background: url(${LegalBg}),
    linear-gradient(180deg, #58a08f33 0%, #ffffff33 100%), #c4c4c433;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: top;
  /* padding-bottom: 130px; */
  height: 300vh;
  /* overflow-x: clip; */
  @media (max-width: 923px) {
    padding-bottom: 0;
  }

  @media (max-height: 667px) {
    padding-bottom: 0;
    height: 2300px;
  }
  @media (max-width: 375px) and (max-height: 667px) {
    height: 2165px;
  }
  .secondWrapper {
    position: relative;
    display: flex;
    margin-top: 3vh;
    padding: 0 15px;
    width: 100%;

    .innerSection {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      width: 100%;
    }

    /* > div {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      width: 100%;
    } */

    .dottedLineY {
      position: absolute;
      width: 100%;
      height: 100%;

      > div {
        display: flex;
        align-items: center;
        width: 50%;
        height: 50%;
        margin: 0 auto;
        position: relative;
        > div {
          position: absolute;
          bottom: 0;
          right: 0;
          left: 0;
          border: 7px dashed #58a08f;
          border-bottom: none;
          width: 100%;
          height: 100%;
          overflow: hidden;
          img {
            width: 100%;
            height: fit-content;
            object-fit: fill;
          }
        }
      }
    }
    /* .dottedLineX {
      position: absolute;
      top: -120px;
      width: 100%;
      height: 100%;

      > div {
        position: relative;
        left: 5px;
        display: flex;
        align-items: flex-start;
        justify-content: center;
        width: 40px;
        height: 50%;
        width: 16px;
        border-left: 7px dashed #58a08f;
        margin: 0 auto;
      }
    } */
  }
`;
const WrapperOne = styled.div`
  max-width: 1186px;
  width: 100%;
  margin: 0 auto;
  margin-top: 0;
  padding: 0 15px;
  position: relative;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;

  /* @media (min-height: 640px) and (max-height: 1024px) {
    margin-top: -28px;
  } */
  /* padding: 200px 0; */
  > div {
    /* margin: 20px 10px; */
    justify-self: center;
    align-self: baseline;
    flex-direction: column;
    position: relative;
    :nth-child(2) {
      .flip-card-inner {
        .flip-card-front {
          background: #74afa2;
          color: #fff;
        }
        p {
          background: #74afa2;
          color: #fff;
        }
      }
    }
  }
  .dottedSVGLineX {
    position: absolute;
    left: calc(50% - 7px);
    bottom: -98px;
    height: clamp(68px, 11.5vw, 174px);
    width: clamp(9px, 2vw, 15px);
    /* @media only screen and (max-width: 1024px) {
      bottom: -20.2vh;
      height: 15vh;
    } */
    @media only screen and (max-width: 768px) {
      bottom: -22.2vh;
      height: 18vh;
    }
  }

  .dottedLineRightX {
    position: absolute;
    left: calc(50% - 5px);
    bottom: -282px;
    width: clamp(68px, 26.5vw, 305px);
    height: 357px;

    /* @media only screen and (max-width: 1126px) {
      left: calc(48% - 15px);
    }  */
    @media only screen and (max-width: 768px) {
      bottom: -36vh;
      width: 39vh;
    }
  }

  .dottedLineLeftX {
    position: absolute;
    right: calc(50% - 5px);
    bottom: -282px;
    width: clamp(68px, 26.5vw, 305px);
    height: 357px;
    transform: rotateY(180deg);
    /* @media only screen and (max-width: 1126px) {
      right: calc(48% - 15px);
    }  */
    @media only screen and (max-width: 768px) {
      bottom: -36vh;
      width: 39vh;
    }
  }

  .dottedLineX {
    position: absolute;
    bottom: -250px;
    width: 100%;
    height: 100%;

    > div {
      position: relative;
      left: 5px;
      display: flex;
      align-items: flex-start;
      justify-content: center;
      width: 40px;
      height: 50%;
      width: 16px;
      border-left: 7px dashed #58a08f;
      margin: 0 auto;
    }
  }
  .industrial {
    svg {
      bottom: -7px;
      left: -39px;
    }
  }

  .lowTHC {
    svg {
      height: 149px;
    }
    > div {
      transform: scale(1);
      animation: pulse-black 2s infinite;
      @keyframes pulse-black {
        0% {
          transform: scale(0.95);
          /* box-shadow: 0 0 0 0 rgba(88, 160, 143, 0.7); */
        }

        70% {
          transform: scale(1);
          /* box-shadow: 0 0 0 10px rgba(0, 0, 0, 0); */
        }

        100% {
          transform: scale(0.95);
          /* box-shadow: 0 0 0 0 rgba(0, 0, 0, 0); */
        }
      }
    }
  }
`;

const WrapperTwo = styled.div`
  max-width: 1186px;
  width: 100%;
  margin: 0 auto;

  > div {
    /* margin: 20px 10px;
    justify-self: center;
    align-self: baseline;
    flex-direction: column; */
  }
  .industrial {
    svg {
      left: -22px;
      top: 22px;
    }
  }

  .lowTHC {
    svg {
      height: clamp(87px, 12vh, 126px);
      margin: 0 auto;
      top: 21px;
      /* @media only screen and (max-width: 1024px) {
        width: 110px;
      }
      @media only screen and (max-width: 768px) {
        width: clamp(87px, 16vh, 148px);
      } */
    }
    :nth-child(2) {
      .flip-card-inner {
        .flip-card-front {
          background: #74afa2;
          color: #fff;
        }

        p {
          background: #74afa2;
          color: #fff;
        }
      }
    }

    > div {
      transform: scale(1);
      animation: pulse-black 2s infinite;
      @keyframes pulse-black {
        0% {
          transform: scale(0.95);
          /* box-shadow: 0 0 0 0 rgba(88, 160, 143, 0.7); */
        }

        70% {
          transform: scale(1);
          /* box-shadow: 0 0 0 10px rgba(0, 0, 0, 0); */
        }

        100% {
          transform: scale(0.95);
          /* box-shadow: 0 0 0 0 rgba(0, 0, 0, 0); */
        }
      }
    }
  }
`;

const Heading = styled.div`
  text-align: center;
  position: relative;
  /* padding-top: 100px; */
  /* 
  @media only screen and (max-width: 320px) {
    padding-top: 45px;
  }

  @media (min-height: 640px) and (max-height: 1024px) {
    padding-top: 80px;
  }
  @media only screen and (max-width: 375px) and (max-height: 667px) {
    padding-top: 62px;
  } */
  > div {
    display: flex;
    justify-content: center;
    align-items: baseline;
    > h2 {
      margin: 0 5.5px;
    }
  }
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
    /* max-width: 74%; */
    /* margin: 0 auto; */
  }
  span {
    margin: 0px 7px;
  }
  h3 {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    width: 75%;
    margin: 0 auto;
    font-weight: unset;
    margin-top: 10px;
  }
  @media (max-width: 767px) {
    margin-bottom: 15%;
    > div {
      flex-direction: column;
      align-items: center;
    }
    h2 {
      font-size: 16pt;
      line-height: 16pt;
    }
    h3 {
      font-size: 12pt;
      font-weight: unset;
      max-width: 74%;
    }
    .smallText {
      font-size: 11pt;
    }
    @media (max-height: 667px) {
      margin-bottom: 2vh;
    }
  }
  @media (max-width: 767px) and (-webkit-device-pixel-ratio: 3) {
    margin-bottom: 10%;
  }
  @media (max-width: 320px) {
    /* padding-top: 61px; */
    > div {
      flex-direction: column;
      align-items: center;
    }
    h2 {
      font-size: 11pt;
      line-height: 9pt;
    }
    span {
      font-size: 9pt;
    }
    h3 {
      font-size: 9pt;
      max-width: 74%;
      font-weight: unset;
      margin-top: 0;
    }
  }
`;

const EvolutionBoxContainers = styled(motion.div)`
  max-width: 317px;
  width: 100%;
  border-radius: 3px;
  position: relative;
  align-self: center;
  justify-self: center;
  z-index: 1;
  text-align: center;
  cursor: pointer;
  /* @media only screen and (max-width: 1024px) {
    max-width: 284px;
  }
  @media only screen and (max-width: 768px) {
    max-width: 317px;
  } */
  :focus {
    outline: none;
    background: none;
    :transparent ;
  }
  svg {
    position: relative;
    z-index: 1;
  }
  svg.syntheticsIcon {
    height: clamp(52px, 11vh, 114px);
    margin: 0 auto;
    top: 26px;
    /* @media only screen and (max-width: 1024px) {
      width: 70px;
    }
    @media only screen and (max-width: 320px) {
      right: 3px;
      width: clamp(52px, 8vh, 148px);
    } */
  }
  svg.botanical {
    height: clamp(68px, 11vh, 109px);
    margin: 0 auto;
    top: 24px;

    /* @media only screen and (max-width: 1024px) {
      width: 110px;
    }
    @media only screen and (max-width: 768px) {
      width: clamp(68px, 11vh, 148px);
    } */
  }
  svg.cannabisIcon {
    height: clamp(88px, 14vh, 148px);
    top: 37px;
    margin: 0 auto;
    /* @media only screen and (max-width: 1024px) {
      width: 110px;
    }
    @media only screen and (max-width: 768px) {
      top: -3px;
      width: clamp(88px, 11vh, 148px);
    } */
    @media only screen and (max-width: 667px) {
      height: 60px !important;
      top: -48px !important;
      width: auto;
    }
  }
  svg.IndustrialHempIcon {
    height: clamp(87px, 12vh, 126px);
    margin: 0 auto;
    /* @media only screen and (max-width: 1024px) {
      width: 110px;
    }
    @media only screen and (max-width: 768px) {
      width: clamp(87px, 15vh, 148px);
    } */
  }
  svg.thcCannabisIcon {
    height: clamp(90px, 12vh, 126px);
    margin: 0 auto;
    top: 21px;
    /* @media only screen and (max-width: 1024px) {
      width: 70px;
    }
    @media only screen and (max-width: 768px) {
      width: clamp(52px, 10vh, 148px);
    } */
  }
  > div {
    font-size: 22.5px;
    color: #58a08f;
    /* border: 5px solid #58a08f;
    padding: 18px 25px;
    display: flex;
    justify-content: center; */
    /* align-items: center;
    min-height: 104px; */
    /* border-radius: 4px;
    flex-direction: column; */
    transform: scale(1);

    transition: min-height 0.15s ease-out;
    /* position: absolute;
    right: 0;
    left: 0; */
    bottom: 18px;
    p {
      font-size: 17.5px;
    }
  }

  .flip-card {
    background-color: transparent;
    /* width: 300px;
    height: 300px; */
    perspective: 1000px;
    /* height: clamp(95px, 3vh, 147px); */
    /* height:100%; */
    height: clamp(89px, 15vh, 115px);
    @media only screen and (max-width: 923px) {
      height: auto;
    }
  }

  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
  }

  /* .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  } */

  .flip-card-front,
  .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }

  .flip-card-front {
    background-color: #f0f1f1;
    color: #58a08f;
    font-size: clamp(18px, 3vh, 22px);
    /* height: clamp(89px, 15vh, 115px); */
    max-height: 147px;
    display: flex;
    align-items: center;
    justify-content: center;
    border: 5px solid #58a08f;
    border-radius: 7.5px;
    text-transform: uppercase;
    /* @media (max-width: 1024px) {
      height: 103px;
    } */
    @media (max-width: 768px) {
      height: clamp(89px, 9vh, 147px);
    }
  }
  @media (max-width: 768px) {
    svg {
      position: absolute;
    }
  }

  .flip-card-back {
    /* background-color: #2980b9; */
    color: white;
    transform: rotateY(180deg);
    > p {
      background: white;
      padding: 12px 20px 9px;
      /* height: clamp(89px,15vh,115px); */
      border: 5px solid #58a08f;
      color: #4a4a4a;
      font-size: clamp(14px, 2vh, 16px);
      min-height: 89px;
      font-family: 'robotoregular';
      border-radius: 7.5px;
      display: flex;
      align-items: center;
      height: 100%;
    }
  }
`;
const Star = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: transparent !important;
  transform: scale(1.2);
  img {
    animation: twinkleOne 0.75s alternate infinite;
    animation-delay: 0.88462s;
    :nth-child(even) {
      animation-delay: 0.88462s;
    }
    :nth-child(odd) {
      animation-delay: 1s;
    }
    :first-child {
      position: absolute;
      left: 0;
      top: 51%;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
    }
    :nth-child(2) {
      position: absolute;
      left: 0;
      bottom: 54px;
      width: 25px;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(3) {
      position: absolute;
      left: 31px;
      bottom: 40px;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
    }
    :nth-child(4) {
      position: absolute;
      left: 37px;
      bottom: -12px;
      width: 10px;
      animation: twinkleTwo 0.6s alternate infinite;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(5) {
      position: absolute;
      left: 45px;
      bottom: 12px;
      width: 14px;
      animation: twinkleTwo 0.6s alternate infinite;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(6) {
      position: absolute;
      left: 121px;
      bottom: 3px;
      width: 14px;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(7) {
      position: absolute;
      left: 83px;
      bottom: -15px;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
      @media (max-width: 922px) {
        bottom: -10px;
      }
    }
    :nth-child(8) {
      position: absolute;
      right: 38px;
      bottom: 1px;
      width: 14px;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(9) {
      position: absolute;
      right: 10px;
      bottom: 26px;
      width: 19px;
      animation: twinkleTwo 0.6s alternate infinite;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(10) {
      position: absolute;
      right: 42px;
      bottom: 68px;
      width: 9px;
    }
    :nth-child(11) {
      position: absolute;
      right: 17px;
      top: 56px;
      width: 8px;
      animation: twinkleTwo 0.6s alternate infinite;
      @media (max-width: 922px) {
        display: none;
      }
    }
    :nth-child(12) {
      position: absolute;
      right: 10px;
      top: 40px;
      width: 21px;
      animation: twinkleTwo 0.7s alternate infinite;
    }
    :nth-child(13) {
      position: absolute;
      left: 71px;
      top: 5px;
      width: 8px;
      @media (max-width: 922px) {
        display: none;
      }
    }
  }

  @keyframes twinkleOne {
    0% {
      opacity: 0;
    }

    100% {
      opacity: 1;
      transform: scale(1.2);
    }
  }
`;
const WebView = styled.div`
  @media (max-width: 922px) {
    display: none;
  }
`;
const Button = styled.div`
  font-size: clamp(16px, 2vh, 20px);
  background: #58a08f;
  border-radius: 7.5px;
  width: fit-content;
  margin: 0 auto;
  padding: 17px 45px;
  color: #fff;
  text-transform: uppercase;
  span.plusIcon {
    line-height: 10px;
    font-size: 12px;
    position: relative;
    top: -4px;
    @media (max-width: 923px) {
      top: -4px;
      font-size: 10px;
    }
  }

  @media (min-height: 640px) and (max-height: 1024px) {
    font-size: clamp(12px, 2vh, 20px);
    padding: 15px 45px;
  }
  @media (max-width: 923px) {
    font-size: 15px;
    font-family: 'roboto_condensedregular';
  }
  @media (max-width: 375px) and (max-height: 667px) {
    font-size: 13px;
    padding: 8px 30px;
  }
  @media (max-width: 320px) {
    font-size: 10px;
    padding: 8px 30px;
  }
`;

const ButtonWrapper = styled(motion.div)`
  /* position: absolute;
  top: 680px;
  left: 0;
  right: 0; */
  margin-top: 10%;

  @media (min-height: 640px) and (max-height: 1024px) {
    margin-top: 10.5%;
  }
  @media (max-width: 375px) and (max-height: 667px) {
    margin-top: 0;
  }
  @media (max-width: 1024px) {
    margin-top: 10%;
  }
  @media (max-width: 320px) {
    margin-top: 5%;
  }
`;

const MobileView = styled.div`
  position: relative;
  /* margin-top: -24px; */
  /* margin-bottom: 50px; */
  @media (min-width: 923px) {
    display: none;
  }
  .cannabisDiv {
    @media (max-width: 375px) and (max-height: 667px) {
      height: 86px !important;
    }
    > div {
      width: 170px !important;

      @media (max-width: 1024px) {
        width: 200px !important;
      }
      @media (max-width: 768px) {
        width: 200px !important;
      }
      @media (max-width: 620px) {
        width: 170px !important;
      }
      @media (max-width: 320px) {
        width: 136px !important;
      }
    }
  }
  .fourthWrapper {
    margin-top: 20px !important;
  }
  .secondWrapper {
    margin-top: -6px !important;
  }
  .secondWrapper,
  .fourthWrapper {
    margin: 0 auto;
  }
  svg.IndustrialHempIcon {
    bottom: -13px;
    right: 26px !important;
    width: auto;
    @media (min-width: 320px) {
      right: 8px;
    }
  }
  > div {
    height: 121px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    max-width: 400px;
    width: 92%;
    margin: 0 auto;
    @media (max-width: 1024px) {
      max-width: 481px;
    }
    @media (max-width: 768px) {
      max-width: 481px;
    }
    ${EvolutionBoxContainers} {
      /* width: 150px; */
      margin: 0 auto;
      @media (max-width: 1024px) {
        width: 200px;
      }
      @media (max-width: 768px) {
        width: 200px;
      }
      @media (max-width: 600px) {
        width: 150px;
        height: 50%;
        margin-top: 58px !important;
      }
      @media (max-width: 320px) and (max-height: 568px) {
        width: 133px;
      }
      @media (max-height: 667px) {
        .lowThcCannabis {
          right: 1px;
        }
      }
      /* @media (max-width: 375px) and (max-height: 667px) {
      height: 86px !important;
    } */
      h4 {
        line-height: 14px;
      }
      .flip-card-front {
        font-size: 13px;
        height: 50px;
        border: 2px solid #58a08f;
        background-color: #fff;
      }
      svg {
        height: 56px;
        top: -47px;
        right: 0px;
        left: 0;
        margin: 0 auto;
      }
      .flip-card-back > p {
        padding: 5px;
        border: 3px solid #58a08f;
        font-size: 11px;
        min-height: auto;
      }
    }
  }
  .mr-t20 {
    svg {
      width: 90px !important;
    }
  }
  .bgGreen {
    .flip-card-front {
      background-color: #58a08f !important;
      color: #fff;
    }
    .flip-card-back p {
      background-color: #58a08f !important;
      color: #fff;
    }
  }

  .dottedLineYMobileSVG {
    position: absolute;
    left: calc(50% - 5.5px);
    top: 90px;
    min-height: 240px;
    width: 10px;
  }

  .dottedLineYMobileRightSVG {
    position: absolute;
    left: calc(50% - 1px);
    bottom: 174.25px;
  }

  .dottedLineYMobileLeftSVG {
    position: absolute;
    right: calc(50% + -1px);
    bottom: 174.3px;
    transform: rotateY(180deg);
  }

  .dottedLineXMobile {
    position: absolute;
    border-right: 3px dashed #58a08f;
    width: 50%;
    top: 146px;
    height: 138px;
  }
  .dottedLineYMobile {
    position: absolute;
    border: 3px dashed #58a08f;
    width: 70%;
    top: 286px;
    height: 246px;
    /* margin: 0 auto; */
    right: 0;
    left: 0;
    border-bottom: 0;
  }

  @media (max-width: 600px) {
    .secondWrapper {
      margin-top: -26px !important;
    }
    .secondWrapper,
    .fourthWrapper {
      height: 112px;
      margin-top: -19px;
    }

    .fourthWrapper {
      margin-top: 40px !important;
    }

    > div {
      height: 125px;
      margin-top: -4px;

      ${EvolutionBoxContainers} {
        /* width: 150px; */
        margin: 0 auto;
        h4 {
          line-height: 9pt;
          font-size: 9pt;
        }
        .flip-card-front {
          height: 45px;
        }
        /* svg {
          height: 58px;
          bottom: 7px;
          top: -6px;
        } */
        .flip-card-back > p {
          padding: 5px;
          border: 3px solid #58a08f;
          font-size: 1.5vh;
          min-height: auto;
          height: clamp(66px, 11vh, 147px);
        }
      }
    }
  }
  @media (max-height: 568px) {
    .secondWrapper {
      margin-top: -31px !important;
    }
    .fourthWrapper {
      margin-top: 40px !important;
    }
  }
  @media only screen and (max-width: 375px) and (max-height: 812px) and (min-height: 668px) {
    .secondWrapper,
    .fourthWrapper {
      height: 112px;
    }
    .secondWrapper {
      margin-top: -9px !important;
    }
    .fourthWrapper {
      margin-top: 40px !important;
    }
    > div {
      ${EvolutionBoxContainers} {
        svg {
          height: 78px !important;
          top: -70px !important;
        }
        .syntheticsIcon,
        .botanical {
          height: 66px !important;
          top: -58px !important;
        }
        .cannabisIcon {
          top: -63px !important;
        }
      }
    }

    .dottedLineYMobileSVG {
      width: 7px;
      left: calc(50% - 3px);
    }
  }
  @media (max-height: 667px) {
    > div {
      ${EvolutionBoxContainers} {
        svg {
          height: 50px;
        }
      }
    }
  }
`;
export {
  Container,
  Heading,
  WrapperOne,
  WrapperTwo,
  EvolutionBoxContainers,
  Star,
  WebView,
  MobileView,
  Button,
  ButtonWrapper,
};
