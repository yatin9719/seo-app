import * as React from 'react';
import { motion } from 'framer';

const Icon = () => (
  <motion.svg
    width={298}
    height={2}
    viewBox='0 0 298 2'
    className='roadLineIcon'>
    <path d='M297.9 1.46H.4' stroke='#010101' strokeMiterlimit={10} />
  </motion.svg>
);

export default Icon;
