import styled from 'styled-components';
import { motion } from 'framer';
const SunGlow = styled(motion.div)`
  top: 50px;
  right: 52px;
  position: absolute;
  background: #f9c014;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  animation: glowing 2s infinite;
  /* overflow-x: clip; */

  /* @media (max-width: 1024px) {
    top: 85px;
  } */
  @media (max-width: 768px) {
    top: 22px;
  }

  @keyframes glowing {
    0% {
      box-shadow: 0 0 10px -10px #f9c014;
    }
    40% {
      box-shadow: 0 0 10px 8px #f9c014;
    }
    60% {
      box-shadow: 0 0 10px 8px #f9c014;
    }
    100% {
      box-shadow: 0 0 10px -10px #f9c014;
    }
  }

  @keyframes glow {
    from {
      box-shadow: 0 0 10px -10px #f9c014;
    }
    to {
      box-shadow: 0 0 10px 10px #f9c014;
    }
  }
`;
const ProcessSectionImage = styled.div`
  /* min-width: calc(50% + 39px);  */
  min-width: calc(50% + 58px);

  #smokeAreaSection {
    position: relative;
    width: 100%;
    height: 100%;
  }
  > div {
    position: relative;
    height: clamp(113px, 17vh, 150px);
    margin-bottom: 3.9vh;
    :last-child {
      margin: 0;
      /* margin-bottom: 0 !important; */
    }
    /* border-bottom-style: double;
    border-width: 6px;
    border-color: #000; */

    #smoke1 {
      position: absolute;
      z-index: 1;
      width: 1px;
      height: 160px;
      right: 62px;
      bottom: 52px;
    }

    #smoke2 {
      position: absolute;
      z-index: 2;
      width: 1px;
      height: 160px;
      right: 80px;
      bottom: 50px;
    }

    @media (max-width: 1024px) {
      #smoke1 {
        position: absolute;
        z-index: 1;
        width: 1px;
        height: 160px;
        bottom: 73px;
      }
      #smoke2 {
        position: absolute;
        z-index: 2;
        width: 1px;
        height: 160px;
        bottom: 71px;
      }
    }
    @media (max-width: 1023px) {
      #smoke1 {
        right: calc(50% - 72px);
      }
      #smoke2 {
        right: calc(50% - 54px);
      }
    }

    @media (max-width: 768px) {
      height: 91px;
      margin-bottom: 74px;
      #smoke1 {
        position: absolute;
        z-index: 1;
        width: 1px;
        height: 160px;
        /* right: 56px;
        bottom: 64px; */
        right: calc(50% - 72px);
        bottom: 73px;
      }
      #smoke2 {
        position: absolute;
        z-index: 1;
        width: 1px;
        height: 160px;
        /* right: 75px;
        bottom: 63px; */
        right: calc(50% - 54px);
        bottom: 71px;
      }
    }

    /* smoke balls */
    @media (max-width: 600px) {
      height: 86px;
      margin-bottom: 74px !important;
      #smoke1 {
        right: calc(50% - 58px);
        bottom: 60px;
      }
      #smoke2 {
        bottom: 59px;
        right: calc(50% - 44px);
      }
    }
    @media (max-width: 600px) and (-webkit-device-pixel-ratio: 3) {
      margin-bottom: 11vh !important;
    }
    @media (max-width: 375px) and (max-height: 667px) {
      height: 75px;
      margin-bottom: 74px !important;
    }
    @media (max-width: 320px) {
      height: 70px;
    }

    #smoke1 span,
    #smoke2 span {
      display: block;
      position: absolute;
      bottom: ${window.innerWidth > 1100 ? '-38px' : '-55px'};
      left: 50%;
      margin-left: -22px;
      height: 0px;
      width: 0px;
      border: 35px solid #fcfafa;
      border-radius: 35px;
      left: -11px;
      opacity: 0;
      transform: scale(0);
    }

    /* Smoke animation */

    @keyframes smokeL {
      0% {
        transform: scale(0.2) translate(0, 0);
      }
      10% {
        opacity: 1;
        transform: scale(0.2) translate(0, -5px);
      }
      100% {
        opacity: 0;
        transform: scale(1) translate(-20px, -130px);
      }
    }

    @keyframes smokeR {
      0% {
        transform: scale(0.2) translate(0, 0);
      }
      10% {
        opacity: 1;
        transform: scale(0.2) translate(0, -5px);
      }
      100% {
        opacity: 0;
        transform: scale(1) translate(20px, -130px);
      }
    }

    #smoke1 .s0,
    #smoke2 .s0 {
      animation: smokeL 10s 0s infinite;
    }
    #smoke1 .s1,
    #smoke2 .s1 {
      animation: smokeR 10s 1s infinite;
    }
    #smoke1 .s2,
    #smoke2 .s2 {
      animation: smokeL 10s 2s infinite;
    }
    #smoke1 .s3,
    #smoke2 .s3 {
      animation: smokeR 10s 3s infinite;
    }
    #smoke1 .s4,
    #smoke2 .s4 {
      animation: smokeL 10s 4s infinite;
    }
    #smoke1 .s5,
    #smoke2 .s5 {
      animation: smokeR 10s 5s infinite;
    }
    #smoke1 .s6,
    #smoke2 .s6 {
      animation: smokeL 10s 6s infinite;
    }
    #smoke1 .s7,
    #smoke2 .s7 {
      animation: smokeR 10s 7s infinite;
    }
    #smoke1 .s8,
    #smoke2 .s8 {
      animation: smokeL 10s 8s infinite;
    }
    #smoke1 .s9,
    #smoke2 .s9 {
      animation: smokeR 10s 9s infinite;
    }

    .analyticsWrapper {
      position: absolute;
      display: flex;
      right: 153px;
      bottom: 13px;
      max-width: 20px;
      transform: rotateX(180deg);
      z-index: 3;
      height: 15px;
      background-color: #fff;
      > svg {
        position: unset;
      }
    }

    svg {
      position: absolute;
    }
    .farmHouseIcon {
      right: 24px;
      bottom: -6px;
      z-index: 1;
    }
    .farmWindow {
      right: 0;
      bottom: 0;
    }
    .forkIcon {
      right: 81px;
      bottom: 0;
    }
    .sunIcon {
      top: 43px;
      right: 46px;
    }
    .manIcon {
      right: 89px;
      bottom: -1px;
    }
    .windTurbine {
      bottom: -4px;
      right: 192px;
      z-index: 1;
    }
    .barnWheel {
      bottom: 34px;
      right: 194px;
    }
    .farmTruckIcon {
      bottom: 0px;
      right: 200px;
    }
    .manufacturingPlant {
      bottom: -5px;
      right: 17px;
      z-index: 2;
    }
    .manufacturingPlantOne {
      bottom: -1px;
      right: 82px;
      z-index: 1;
    }
    .manufacturingTruck {
      bottom: -6.5px;
      right: 70px;
    }
    .manufacturingMan {
      z-index: 2;
      bottom: 0px;
      right: 96px;
    }
    .refineryManIcon {
      right: 96px;
      bottom: 0px;
      z-index: 1;
    }
    .buildingIcon {
      right: 38px;
      bottom: -5px;
      z-index: 1;
    }
    .buildingIconOne {
      right: 75px;
      bottom: -1px;
      z-index: 0;
    }
  }
  @media (max-width: 1024px) {
    > div {
      .windTurbine {
        bottom: -4px;
      }
      .farmTruckIcon {
        bottom: -1px;
      }
      .farmHouseIcon {
        bottom: -7px;
      }
      .manIcon {
        bottom: -2px;
      }
      .manufacturingMan {
        bottom: -1px;
      }
      .manufacturingTruck {
        bottom: -7.5px;
      }
      .refineryManIcon {
        bottom: -1px;
      }
    }
  }
  @media (max-width: 1023px) {
    min-width: 100%;
    > div {
      .manIcon,
      .refineryManIcon,
      .manufacturingMan {
        right: 50%;
        transform: translateX(50%);
        bottom: 0px;
      }
      .windTurbine {
        right: calc(50% + 115px);
        transform: translateX(50%);
      }
      .barnWheel {
        right: calc(50% + 85px);
        bottom: 38px;
        transform: translateX(50%);
      }
      .manufacturingPlant {
        right: calc(50% - 63px);
        transform: translateX(50%);
      }
      .manufacturingPlantOne {
        right: calc(50% - 63px);
        transform: translateX(50%);
      }
      .manufacturingTruck {
        right: calc(50% - 62px);
        transform: translateX(50%);
        bottom: -6.5px;
      }
      .farmHouseIcon {
        right: calc(50% - 63px);
        transform: translateX(50%);
      }
      .sunIcon {
        right: calc(50% - 85px);
        transform: translateX(50%);
      }
      ${SunGlow} {
        right: calc(50% - 75px);
        transform: translateX(50%);
      }
      .farmTruckIcon {
        right: calc(50% + 95px);
        transform: translateX(50%);
      }
      .analyticsWrapper {
        right: calc(50% + 17px);
        transform: translateX(50%) rotateX('-180%');
        z-index: 2;
      }
      .buildingIcon {
        right: calc(50% - 60px);
        transform: translateX(50%);
      }
      .buildingIconOne {
        right: calc(50% - 60px);
        transform: translateX(50%);
      }
    }
  }
  @media (max-width: 667px) {
    /* min-width: 100%; */
    > div {
      .manIcon {
        bottom: -10px;
        right: 50%;
        transform: translateX(50%) scale(0.8);
      }
      .refineryManIcon,
      .manufacturingMan {
        right: 50%;
        transform: translateX(50%) scale(0.8);
        bottom: -9px;
      }
      .windTurbine {
        right: calc(50% + 115px);
        transform: translateX(50%) scale(0.85);
        bottom: -8px;
      }
      .barnWheel {
        right: calc(50% + 88px);
        bottom: 27px;
        transform: translateX(50%) scale(0.8);
      }
      .manufacturingPlant {
        right: calc(50% - 63px);
        transform: translateX(50%);
        transform: translateX(50%) scale(0.8);
        bottom: -9px;
      }
      .manufacturingPlantOne {
        right: calc(50% - 25px);
        transform: translateX(50%);
        transform: translateX(50%) scale(0.8);
        bottom: -7px;
      }
      .manufacturingTruck {
        right: calc(50% - 62px);
        transform: translateX(50%) scale(0.8);
        bottom: -9.5px;
        width: 59px;
      }
      .farmHouseIcon {
        right: calc(50% - 51px);
        transform: translateX(50%) scale(0.8);
        bottom: -10px;
      }
      .sunIcon {
        right: calc(50% - 71px);
        transform: translateX(50%) scale(0.8);
        top: 8px;
      }
      ${SunGlow} {
        right: calc(50% - 59px);
        transform: translateX(50%) scale(0.8);
        top: 17px;
      }
      .farmTruckIcon {
        right: calc(50% + 95px);
        transform: translateX(50%) scale(0.8);
        bottom: -5.5px;
        width: 30px;
      }
      .analyticsWrapper {
        right: calc(50% + 11px);
        transform: translateX(50%) rotateX('-180%') scale(0.8);
        z-index: 2;
        bottom: 9px;
        max-width: 16px;
      }
      .buildingIcon {
        right: calc(50% - 60px);
        transform: translateX(50%) scale(0.8);
        bottom: -13px;
      }
      .buildingIconOne {
        right: calc(50% - 35px);
        transform: translateX(50%);
      }
    }
  }
  @media (max-width: 375px) and (max-height: 667px) {
    /* min-width: 100%; */
    > div {
      .manIcon,
      .refineryManIcon,
      .manufacturingMan {
        right: 50%;
        transform: translateX(50%) scale(0.8);
        bottom: -9px;
      }
      .windTurbine {
        right: calc(50% + 115px);
        transform: translateX(50%) scale(0.8);
        bottom: -9px;
      }
      .barnWheel {
        right: calc(50% + 88px);
        bottom: 27px;
        transform: translateX(50%) scale(0.8);
      }

      .manufacturingTruck {
        right: calc(50% - 62px);
        transform: translateX(50%) scale(0.8);
        bottom: -9.5px;
        width: 59px;
      }
      .farmHouseIcon {
        right: calc(50% - 51px);
        transform: translateX(50%) scale(0.8);
        bottom: -10px;
      }
      .sunIcon {
        right: calc(50% - 71px);
        transform: translateX(50%) scale(0.8);
      }
      /* ${SunGlow} {
        right: calc(50% - 59px);
        transform: translateX(50%) scale(0.8);
        top: 6px;
      } */
      .farmTruckIcon {
        right: calc(50% + 95px);
        transform: translateX(50%) scale(0.8);
        bottom: -6px;
        width: 30px;
      }
      .analyticsWrapper {
        right: calc(50% + 11px);
        transform: translateX(50%) rotateX('-180%') scale(0.8);
        z-index: 2;
        bottom: 9px;
      }
    }
  }
  @media (max-width: 320px) {
    /* min-width: 100%; */
    > div {
      .manIcon,
      .refineryManIcon,
      .manufacturingMan {
        right: 50%;
        transform: translateX(50%) scale(0.8);
        bottom: -9px;
      }
      .windTurbine {
        right: calc(50% + 115px);
        transform: translateX(50%) scale(0.8);
        bottom: -9px;
      }
      .barnWheel {
        right: calc(50% + 88px);
        bottom: 27px;
        transform: translateX(50%) scale(0.8);
      }
      .manufacturingPlant {
        right: calc(50% - 48px);
        transform: translateX(50%) scale(0.8);
        bottom: -10px;
      }
      .manufacturingTruck {
        right: calc(50% - 62px);
        transform: translateX(50%) scale(0.8);
        bottom: -9.5px;
        width: 59px;
      }
      .farmHouseIcon {
        right: calc(50% - 51px);
        transform: translateX(50%) scale(0.8);
        bottom: -10px;
      }
      .sunIcon {
        right: calc(50% - 71px);
        transform: translateX(50%) scale(0.8);
      }
      /* ${SunGlow} {
        right: calc(50% - 59px);
        transform: translateX(50%) scale(0.8);
        top: 0px;
      } */
      .farmTruckIcon {
        right: calc(50% + 95px);
        transform: translateX(50%) scale(0.8);
        bottom: -6px;
        width: 30px;
      }
      .analyticsWrapper {
        right: calc(50% + 11px);
        transform: translateX(50%) rotateX('-180%') scale(0.8);
        z-index: 2;
        bottom: 9px;
      }
      .buildingIcon {
        right: calc(50% - 48px);
        transform: translateX(50%) scale(0.8);
        bottom: -13px;
      }
      .buildingIconOne {
        right: calc(50% - 60px);
        transform: translateX(50%);
      }
    }
  }
`;
const ProcessSectionText = styled.div`
  > div {
    background-color: #ffffffab;
    text-transform: uppercase;
    margin-bottom: 3.9vh;
    padding: 2vh 47.5px;
    height: clamp(113px, 17vh, 150px);
    align-items: center;
    display: flex;
    :last-child {
      margin: 0;
    }
    /* width: calc(50% - 59px); */
    > div {
      > div {
        :first-child {
          margin-bottom: 15px;
        }
      }
    }
    .light-green {
      color: #adc98d;
    }
    .dark-green {
      color: #8ccbbc;
    }
    .orange {
      color: #f89631;
    }
    h4 {
      font-size: 17.5px;
      font-family: 'hansonbold';
      margin-bottom: 15px;
    }
    /* h4 {
      font-size: 12.5px;
      font-family: 'hansonbold';
    } */
    p {
      font-size: 15px;
      font-family: 'robotoregular';
      text-transform: none;
      max-width: 446px;
    }
  }
  @media (max-width: 1250px) {
    > div {
      h3 {
        font-size: 17px;
      }
      p {
        font-size: 11px;
      }
    }
  }
`;
const SupplyChainWrapper = styled.div`
  display: flex;
  margin-top: clamp(24px, 4vh, 50px);
  /* grid-gap: 45px; */
  > div {
    flex: 1;
  }
  @media (max-width: 1023px) {
    display: none;
  }
`;
const Heading = styled.div`
  text-align: center;
  position: relative;
  max-width: 90%;
  margin: 0 auto;
  /* padding-top: 100px; */
  h2 {
    color: #4a4a4a;
    text-transform: uppercase;
    font-size: 30px;
  }
  span.plusIcon {
    line-height: 10px;
    font-size: 13px;
    position: relative;
    top: -11px;
  }
  h3 {
    font-size: 17.5px;
    font-family: 'roboto_condensedregular';
    max-width: 100%;
    font-weight: unset;
    margin: 10px auto auto auto;
  }
  @media (max-width: 767px) {
    padding-bottom: 18%;
    h2 {
      font-size: 16pt;
      line-height: 16pt;
    }
    /* h3:first-child {
      font-size: 13px;
      line-height: 15px;
    } */
    h3 {
      font-size: 12pt;
      max-width: 260px;
      margin-top: 15px;
      font-weight: unset;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -8px;
    }
    @media (max-height: 667px) {
      padding-bottom: 15px;
    }
  }
  @media (max-width: 767px) and (-webkit-device-pixel-ratio: 3) {
    padding-bottom: 15%;
  }
  /* 2532x1170 pixels at 460ppi */
  /* @media only screen 
    and (device-width: 390px) 
    and (device-height: 844px) 
     {      padding-bottom: 24%;
 } */

  @media (max-width: 320px) {
    padding-top: 61px;
    h2 {
      font-size: 11pt;
      line-height: 14pt;
    }
    /* h3:first-child {
      font-size: 13px;
      line-height: 15px;
    } */
    h3 {
      font-size: 9pt;
      max-width: 260px;
      font-weight: unset;
      margin-top: 1px;
      margin-bottom: 13px;
    }
    span.plusIcon {
      line-height: 10px;
      font-size: 10px;
      position: relative;
      top: -8px;
    }
  }
`;
const SupplyChainContainer = styled.div`
  height: 390vh;
  min-height: 3000px;
  position: relative;

  @media (max-width: 968px) {
    height: 450vh;
    margin-top: 0px;
    padding-bottom: 30px;
  }

  /* overflow: hidden; */
`;
const MobileView = styled(motion.div)`
  position: absolute;
  bottom: -35px;
  right: 0;
  text-align: center;
  left: 0;
  > div > p {
    font-family: 'robotoregular';
  }
  > div > h3 {
    text-transform: uppercase;
    font-size: 14pt;
  }
  .light-green {
    color: #adc98d;
  }
  .dark-green {
    color: #8ccbbc;
  }
  .orange {
    color: #f89631;
  }
  @media (min-width: 1023px) {
    display: none;
  }
  @media (max-height: 667px) {
    bottom: -27px;

    > div > h3 {
      font-size: 12pt;
    }
  }
`;

const SupplyChainWrapperMobile = styled.div`
  display: none;
  /* margin-top: 100px; */
  /* .sunIcon {
    right: 40px;
  }
  .buildingIcon {
    right: -3px !important;
  }
  ${SunGlow} {
    right: 47px !important;
  } */
  @media (max-width: 1023px) {
    display: flex;
  }
  @media (max-height: 667px) {
    margin-top: 10px;
  }
`;
const SunSection = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;
export {
  SupplyChainContainer,
  SupplyChainWrapper,
  ProcessSectionImage,
  ProcessSectionText,
  Heading,
  MobileView,
  SunGlow,
  SunSection,
  SupplyChainWrapperMobile,
};
