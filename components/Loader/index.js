import React from 'react';
import { Container, Wrapper, LoaderWrapper } from 'css/components/Loader';
import AnimatedLogo from 'assets/Gifs/TBF_loading_spinner4_small.gif';

const Loader = () => {
  return (
    <Container>
      <Wrapper>
        <LoaderWrapper>
          <img height={50} src={AnimatedLogo} alt='TBF Loader' />
        </LoaderWrapper>
      </Wrapper>
    </Container>
  );
};

export default Loader;
