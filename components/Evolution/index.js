import React, { useState, useEffect } from 'react';
import EvolutionBox from 'components/Evolution/EvolutionBox';
import LineY from 'assets/SVG/LineX';
import LineYMobile from 'assets/SVG/LineXMobile';
import LineYMobileRight from 'assets/SVG/LineXMobileRight';
import LineXRight from 'assets/SVG/LineXRight';
import ThcCannabis from 'assets/SVG/ThcCannabis';
import Cannabis from 'assets/SVG/Cannabis';
import Synthetics from 'assets/SVG/Synthetics';
import Botanical from 'assets/SVG/Botanical';
import IndustrialHemp from 'assets/SVG/IndustrialHemp';
import StarImage from 'assets/Images/star.png';
import LowThcCannabis from 'assets/SVG/LowThcCannabis';
import { Frame, useTransform, useViewportScroll } from 'framer';
import { getPercentage, inView } from 'utils';

import {
  Container,
  Heading,
  WrapperOne,
  WrapperTwo,
  Star,
  WebView,
  MobileView,
} from 'css/components/Evolution';

const initialState = {
  1: false,
  2: false,
  3: false,
  4: false,
  5: false,
  6: false,
};

const Evolution = React.memo(({ lab, ...props }) => {
  const [open, setOpen] = useState(initialState);
  const { scrollHeight, clientHeight } = lab || 0;
  const { scrollY } = useViewportScroll();
  const [startSection, setStartSection] = useState(false);
  const [endSection, setEndSection] = useState(true);
  const { y } = props;

  const scrollEvents = () => {
    let offsetY = Math.abs(y.get());
    if (offsetY < scrollHeight && startSection) setStartSection(false);
    else if (
      offsetY >= scrollHeight &&
      offsetY <= scrollHeight + getPercentage(2430, 1600) &&
      endSection &&
      !startSection
    ) {
      setEndSection(false);
      setStartSection(true);
    } else if (
      offsetY > scrollHeight + getPercentage(2430, 1600) &&
      !endSection
    )
      setEndSection(true);
  };

  useEffect(() => {
    const unsubscribe = scrollY.onChange(() => {
      if (inView(scrollHeight, clientHeight, scrollY.get()))
        setTimeout(() => {
          requestAnimationFrame(scrollEvents);
        }, 10);
    });
    return () => unsubscribe();
  });

  const rightPodInputs = [
    -lab?.scrollHeight - getPercentage(2430, 200),
    -lab?.scrollHeight - getPercentage(2430, 400),
  ];
  const rightPodsX = useTransform(y, rightPodInputs, [24, 0]);
  const leftPodsX = useTransform(y, rightPodInputs, [-24, 0]);
  const rightPodsOpacity = useTransform(y, rightPodInputs, [0, 1]);

  const pathLength = useTransform(
    y,
    [
      -lab?.scrollHeight - getPercentage(2430, 400),
      -lab?.scrollHeight - getPercentage(2430, 700),
    ],
    [0, 1]
  );
  const pathLengthX = useTransform(
    y,
    [
      -lab?.scrollHeight - getPercentage(2430, 600),
      -lab?.scrollHeight - getPercentage(2430, 850),
    ],
    [0, 1]
  );

  const opacityPath = useTransform(
    y,
    [
      -lab?.scrollHeight - getPercentage(2430, 600),
      -lab?.scrollHeight - getPercentage(2430, 610),
    ],
    [0, 1]
  );

  const opacityPath1 = useTransform(
    y,
    [
      -lab?.scrollHeight - getPercentage(2430, 400),
      -lab?.scrollHeight - getPercentage(2430, 410),
    ],
    [0, 1]
  );

  const inputPropsLowTHC = [
    -lab?.scrollHeight - getPercentage(2430, 900),
    -lab?.scrollHeight - getPercentage(2430, 1000),
  ];
  const yAnimateLowTHC = useTransform(y, inputPropsLowTHC, [100, 0]);
  const opcaityLowTHC = useTransform(yAnimateLowTHC, [100, 0], [0, 1]);

  const inputPropsHighTHC = [
    -lab?.scrollHeight - getPercentage(2430, 1000),
    -lab?.scrollHeight - getPercentage(2430, 1100),
  ];
  const xAnimateHighTHC = useTransform(y, inputPropsHighTHC, [24, 0]);
  const xMinusAnimateHighTHC = useTransform(y, inputPropsHighTHC, [-24, 0]);
  const opcaityHighTHC = useTransform(xAnimateHighTHC, [24, 0], [0, 1]);

  const toggleOpen = (key) => {
    if (open?.[key]) setOpen(initialState);
    else setOpen({ ...initialState, [key]: true });
  };

  const openBox = (key) => {
    setOpen({ ...initialState, [key]: true });
  };
  const highEvents =
    window.innerWidth > 923 ? { onHover: () => openBox('6') } : {};
  const lowEvents =
    window.innerWidth > 923 ? { onHover: () => openBox('5') } : {};
  const industryEvents =
    window.innerWidth > 923 ? { onHover: () => openBox('4') } : {};
  const botEvents =
    window.innerWidth > 923 ? { onHover: () => openBox('3') } : {};
  const cannEvents =
    window.innerWidth > 923 ? { onHover: () => openBox('2') } : {};
  const synEvents =
    window.innerWidth > 923 ? { onHover: () => openBox('1') } : {};

  return (
    <Container id='labSection'>
      <Frame
        top='0px'
        style={{
          backgroundColor: null,
          position: 'sticky',
          width: '100%',
          height: '100vh',
          zIndex: '1',
        }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            height: '100%',
          }}>
          <Heading>
            <div>
              <h2>THE EVOLUTION</h2>
              <h2 className='smallText'>OF</h2>
              <h2>flAVOR</h2>
            </div>
            {window.innerWidth <= 767 && (
              <h3>
                Is your cannabis product 100% cannabis? Tap icons to learn more.
              </h3>
            )}
            {window.innerWidth > 767 && (
              <h3>
                Is your cannabis actually cannabis? Hover over icons to learn
                more.
              </h3>
            )}
          </Heading>
          {window.innerWidth > 923 && (
            <>
              <WebView>
                <WrapperOne>
                  <EvolutionBox
                    icon={<Synthetics />}
                    heading={'Synthetic'}
                    onClick={() => toggleOpen('1')}
                    {...synEvents}
                    open={open?.['1']}
                    detail={
                      'Created from artificial sources, low cost method to recreate terpene isolates.'
                    }
                    style={{ x: leftPodsX, opacity: rightPodsOpacity }}
                  />

                  <EvolutionBox
                    icon={<Cannabis />}
                    heading='CANNABIS'
                    detail='C. sativa, direct from the source.
                100% true to the plant.'
                    onClick={() => toggleOpen('2')}
                    {...cannEvents}
                    open={open?.['2']}
                  />
                  <EvolutionBox
                    icon={<Botanical />}
                    heading='BOTANICAL'
                    detail='Produced from non-cannabis sources for flavoring, carry no effect.'
                    onClick={() => toggleOpen('3')}
                    {...botEvents}
                    open={open?.['3']}
                    style={{ x: rightPodsX, opacity: rightPodsOpacity }}
                  />
                  <LineY
                    className='dottedSVGLineX'
                    pathLength={pathLength}
                    opacity={opacityPath1}
                    height='15vh'
                  />
                  <>
                    <LineXRight
                      className='dottedLineRightX'
                      pathLength={pathLengthX}
                      opacity={opacityPath}
                      width='31vh'
                    />
                    <LineXRight
                      className='dottedLineLeftX'
                      pathLength={pathLengthX}
                      opacity={opacityPath}
                      width='31vh'
                    />
                  </>
                </WrapperOne>

                <WrapperTwo className='secondWrapper'>
                  <div className='innerSection'>
                    <EvolutionBox
                      keyVal='1'
                      className='industrial'
                      icon={<IndustrialHemp />}
                      style={{
                        opacity: opcaityHighTHC,
                        x: xMinusAnimateHighTHC,
                      }}
                      onClick={() => toggleOpen('4')}
                      {...industryEvents}
                      open={open?.['4']}
                      heading='INDUSTRIAL HEMP'
                      detail='Bred for fiber and related uses, lacks terpene content.'
                    />
                    <EvolutionBox
                      icon={<LowThcCannabis />}
                      id='lowThcContainer'
                      heading='LOW-THC CANNABIS'
                      onClick={() => toggleOpen('5')}
                      {...lowEvents}
                      open={open?.['5']}
                      style={{ opacity: opcaityLowTHC, y: yAnimateLowTHC }}
                      detail='Complex terpene-rich cultivars bred into compliance with the 2018 Farm Bill. '
                      className='lowTHC'>
                      <Star>
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                      </Star>
                    </EvolutionBox>
                    <EvolutionBox
                      icon={<ThcCannabis />}
                      heading='HIGH-THC
          CANNABIS'
                      onClick={() => toggleOpen('6')}
                      {...highEvents}
                      open={open?.['6']}
                      detail='Terpene rich cultivars locked within each regulated state market.'
                      style={{ opacity: opcaityHighTHC, x: xAnimateHighTHC }}
                    />
                  </div>
                </WrapperTwo>
              </WebView>
            </>
          )}
          {window.innerWidth <= 923 && (
            <>
              <MobileView>
                <LineYMobile
                  pathLength={pathLength}
                  className='dottedLineYMobileSVG'
                  opacity={opacityPath1}
                />
                <LineYMobileRight
                  className='dottedLineYMobileRightSVG'
                  pathLength={pathLengthX}
                  opacity={opacityPath}
                />
                <LineYMobileRight
                  className='dottedLineYMobileLeftSVG'
                  pathLength={pathLengthX}
                  opacity={opacityPath}
                />

                <div
                  className='bgGreen cannabisDiv'
                  style={{ height: '100px' }}>
                  <EvolutionBox
                    icon={<Cannabis />}
                    heading='CANNABIS'
                    onClick={() => toggleOpen('2')}
                    {...cannEvents}
                    open={open?.['2']}
                    detail='C. sativa, direct from the source.
                100% true to the plant.'
                  />
                </div>
                <div className='secondWrapper'>
                  <EvolutionBox
                    icon={<Synthetics />}
                    heading={'Synthetic'}
                    onClick={() => toggleOpen('1')}
                    {...synEvents}
                    open={open?.['1']}
                    detail={
                      'Created from artificial sources, low cost method to recreate terpene isolates.'
                    }
                    style={{ x: leftPodsX, opacity: rightPodsOpacity }}
                  />

                  <EvolutionBox
                    icon={<Botanical />}
                    heading='BOTANICAL'
                    detail='Produced from non-cannabis sources for flavoring, carry no effect.'
                    onClick={() => toggleOpen('3')}
                    {...botEvents}
                    open={open?.['3']}
                    style={{ x: rightPodsX, opacity: rightPodsOpacity }}
                  />
                </div>
                <div className='fourthWrapper'>
                  <EvolutionBox
                    keyVal='1'
                    className='industrial'
                    icon={<IndustrialHemp />}
                    style={{ opacity: opcaityHighTHC, x: xMinusAnimateHighTHC }}
                    onClick={() => toggleOpen('4')}
                    {...industryEvents}
                    open={open?.['4']}
                    heading='INDUSTRIAL HEMP'
                    detail='Bred for fiber and related uses, lacks terpene content.'
                  />

                  <EvolutionBox
                    icon={<ThcCannabis />}
                    heading='HIGH-THC
          CANNABIS'
                    onClick={() => toggleOpen('6')}
                    {...highEvents}
                    open={open?.['6']}
                    detail='Terpene rich cultivars locked within each regulated state market.'
                    style={{ opacity: opcaityHighTHC, x: xAnimateHighTHC }}
                  />
                </div>

                <div className='bgGreen mr-t20'>
                  <EvolutionBox
                    icon={<LowThcCannabis />}
                    id='lowThcContainer'
                    heading='LOW-THC CANNABIS'
                    onClick={() => toggleOpen('5')}
                    {...lowEvents}
                    open={open?.['5']}
                    detail='Complex terpene-rich cultivars bred into compliance with the 2018 Farm Bill. '
                    style={{ opacity: opcaityLowTHC, y: yAnimateLowTHC }}
                    className='lowTHC'>
                    {!open?.['5'] && (
                      <Star style={{ transform: 'scale(0.8)' }}>
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                        <img src={StarImage} alt='sparkling' />
                      </Star>
                    )}
                  </EvolutionBox>
                </div>
              </MobileView>
            </>
          )}
        </div>
      </Frame>
    </Container>
  );
});
export default Evolution;
