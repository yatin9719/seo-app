import * as React from 'react';

const Icon = () => (
  <svg width={23} height={16.1} viewBox='0 0 10 7' fill='none'>
    <rect
      x={0.112}
      y={0.412}
      width={'100%'}
      height={1.588}
      rx={0.794}
      fill='#595959'
    />
    <rect
      x={0.112}
      y={2.55}
      width={'100%'}
      height={1.588}
      rx={0.794}
      fill='#595959'
    />
    <rect
      x={0.112}
      y={4.688}
      width={'100%'}
      height={1.588}
      rx={0.794}
      fill='#595959'
    />
  </svg>
);

export default Icon;
