import { useState, useEffect } from 'react';
import { Modal } from 'react-responsive-modal';
import 'react-responsive-modal/styles.css';
import styled from 'styled-components';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import { TOGGLE_LOADER } from 'redux/actions/loader';
import { submitEmail } from 'components/Modal/queries';
import STATES from 'components/WorldMap/states';
import BackText from 'assets/SVG/BackIconText';
const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

let states = [
  'East_Connecticut',
  'East_Delaware',
  'East_Maine',
  'East_Maryland',
  'East_Massachusetts',
  'East_New_Hampshire',
  'East_New_Jersey',
  'East_Rhode_Island',
  'East_Vermont',
  'test',
];

const CustomModal = ({
  onClose,
  open,
  active,
  email,
  scrollHeight,
  setEmail,
  inputRef,
  setActive,
  ...props
}) => {
  const [stage, setStage] = useState('ready');
  const [selected, setSelected] = useState(false);
  const [back, setBack] = useState(false);
  const dispatch = useDispatch();
  const [submitted, setSubmitted] = useState([]);
  const { mutate, isLoading, isSuccess, isError } = useMutation(submitEmail);

  useEffect(() => {
    if (isLoading) dispatch({ type: TOGGLE_LOADER, payload: true });
    else dispatch({ type: TOGGLE_LOADER, payload: false });
    if (isError) setStage('error');
    if (isSuccess) {
      if (submitted.length < 2) setStage('ready');
      else setStage('success');
      setSubmitted([...submitted, active]);
    }
  }, [isError, isSuccess, isLoading, dispatch]);

  useEffect(() => {
    if (stage === 'confirm') setActive('test');
  }, [stage]);

  useEffect(() => {
    if (active !== 'test') {
      if (submitted.length < 2 && !states.includes(active)) setStage('ready');
      if (states.includes(active) && !selected && submitted.length <= 2) {
        setStage('confirm');
        setBack(true);
      } else if (selected && submitted.length <= 2) {
        setStage('ready');
      }
    }
  }, [active]);

  const submit = async () => {
    let success = re.test(email.toLowerCase());
    let country = active;
    STATES.forEach((item) => {
      if (
        item?.name?.replace('East_', '')?.replaceAll('_', ' ') ===
        active?.replace('East_', '')?.replaceAll('_', ' ')
      ) {
        country = 'United States';
      }
    });
    let location = {
      state: active?.replace('East_', '').replaceAll('_', ' '),
      country: country.replace('East_', '').replaceAll('_', ' '),
    };

    if (success) {
      mutate({ email: email, ...location });
      dispatch({ type: TOGGLE_LOADER, payload: false });
    } else {
      setStage('error');
    }
    setSelected(false);
    window.scrollTo(0, scrollHeight);
    setBack(false);
  };
  return (
    <Modal
      open={open}
      onClose={() => {
        setSelected(false);
        setBack(false);
        window.scrollTo(0, scrollHeight);
        onClose();
      }}
      center
      preventScroll={true}
      closeOnOverlayClick={false}>
      <Container>
        <div>
          {!submitted.includes(active) && stage === 'ready' && (
            <>
              {back && (
                <BackText
                  onClick={() => {
                    setStage('confirm');
                  }}
                />
              )}
              <h3>YES!</h3>
              <p className='legalExport'>
                It’s legal to export to you in{' '}
                {active?.replace('East_', '').replaceAll('_', ' ')}. Please
                enter a valid email and we’ll share more details.
              </p>
              <Email>
                <input
                  type='email'
                  ref={inputRef}
                  placeholder='ENTER VALID EMAIL'
                  value={email}
                  onChange={(e) => {
                    setEmail(e?.target?.value);
                  }}
                />
              </Email>
              <Next
                onClick={re.test(email.toLowerCase()) ? submit : null}
                disabled={!re.test(email.toLowerCase())}>
                Submit
              </Next>
            </>
          )}
          {(submitted.includes(active) || stage === 'success') &&
            stage !== 'confirm' && (
              <>
                <h3>THANK YOU.</h3>
                <p>We’ll reach out with more details shortly.</p>
              </>
            )}
          {stage === 'confirm' && (
            <>
              <EastCostContainer>
                <p>Please select the East Coast state you wish to ship to</p>
                <ul>
                  {states.map((item) =>
                    item === 'test' ? (
                      <></>
                    ) : (
                      <li
                        onClick={() => {
                          setEmail('');
                          setActive(item);
                          setSelected(true);
                        }}>
                        {item.replace('East_', '').replaceAll('_', ' ')}
                      </li>
                    )
                  )}
                </ul>
              </EastCostContainer>
            </>
          )}

          {stage === 'error' && (
            <>
              <h3>HOLD ON.</h3>
              <p>PLEASE ENTER A VALID EMAIL ADDRESS.</p>
              <Email error={true}>
                <input
                  type='email'
                  placeholder='ENTER VALID EMAIL'
                  value={email}
                  onChange={(e) => {
                    setEmail(e?.target?.value);
                  }}
                />
              </Email>
              <Next
                onClick={re.test(email.toLowerCase()) ? submit : null}
                disabled={!re.test(email.toLowerCase())}>
                Submit
              </Next>
            </>
          )}
        </div>
      </Container>
    </Modal>
  );
};

export default CustomModal;

const Container = styled.div`
  width: calc(90vw - 40px);
  max-width: 750px;
  min-height: 420px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;

  .backIconText {
    position: absolute;
    top: 18px;
    left: 22px;
    cursor: pointer;
    :hover {
      opacity: 0.8;
    }
  }
  p {
    font-family: 'robotoregular';
    text-align: center;
    padding: 11px;
    font-size: 25px;
    max-width: 582px;
    margin: 0 auto;
    line-height: 40px;
  }
  h3 {
    font-size: 60px;
    text-align: center;
    font-family: 'hansonbold';
  }

  @media (max-width: 968px) {
    p {
      font-size: 18px;
      line-height: 25px;
    }
    h3 {
      font-size: 30px;
    }
  }
  @media (max-width: 767px) {
    min-height: auto;
    padding: 15px 0;
    .legalExport {
      max-width: 241px;
    }
  }
`;
const Email = styled.div`
  font-size: 23px;
  margin: 35px auto 0;
  max-width: 360px;
  position: relative;
  input {
    height: 44px;
    width: 100%;
    border: none;
    font-family: 'hansonbold';
    border-bottom: 2px solid #46464680;
    /* padding: 0 31px; */
    text-align: center;
    color: ${(props) => (props.error ? '#F79530' : '#4a4a4a')};
    font-size: clamp(16px, 2.5vw, 22.5px);
    :focus {
      outline: none;
    }
  }
  input[type='email']::-webkit-input-placeholder {
    color: ${(props) => (props.error ? '#F79530' : '#4a4a4a')};
    opacity: 0.5;
    font-family: 'hansonbold';
    font-size: clamp(16px, 2.5vw, 22.5px);
    margin: 0 auto;
  }
`;

const Next = styled.div`
  font-size: 20px;
  margin: 15px auto;
  width: fit-content;
  padding: 15px 44px;
  border-radius: 7.5px;
  background: #58a08f;
  opacity: ${(props) => (props?.disabled ? '0.5' : '1')};
  color: #fff;
  text-transform: uppercase;
  cursor: pointer;

  :hover {
    ${(props) => (props?.disabled ? '' : 'opacity: 0.8;')};
  }
  :active {
    ${(props) => (props?.disabled ? '' : 'opacity: 0.6;')};
  }
`;

const EastCostContainer = styled.div`
  p {
    text-align: center;
    padding: 11px;
    font-size: 25px;
    max-width: 100%;
    margin: 0 auto;
    line-height: 40px;
    font-family: 'robotobold';
  }

  ul {
    list-style: none;
    text-align: center;
    font-family: 'robotoregular';
    line-height: 33px;
    li {
      cursor: pointer;

      @media (min-width: 968px) {
        width: fit-content;
        margin: 0 auto;
      }
    }
    li:active,
    li:focus,
    li:hover {
      font-family: 'robotobold';
      color: #58a08f;
    }
  }
  @media (max-width: 767) {
    p {
      font-size: 18px;
      /* font-family: 'robotobold'; */
      max-width: 272px;
    }
  }
`;
