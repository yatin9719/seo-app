import axios from 'axios';
import { WORLD_MAP_FORMID, SUBMIT_FORM } from 'api';

export const submitEmail = async (body) => {
  let reqBody = {
    fields: [
      {
        name: 'email',
        value: body?.email,
      },
      {
        name: 'state',
        value: body?.state,
      },
      {
        name: 'country',
        value: body?.country,
      },
    ],
  };
  let { data } = await axios.post(
    `${SUBMIT_FORM}/${WORLD_MAP_FORMID}`,
    reqBody
  );
  return data;
};
